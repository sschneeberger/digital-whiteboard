<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Debug\ErrorHandler;

umask(0000);

if( !isset($_COOKIE['XDEBUG_SESSION']) )
    $loader = require_once __DIR__.'/../app/bootstrap.php.cache';
else
    $loader = require_once __DIR__.'/../app/autoload.php';
Debug::enable();

require_once __DIR__.'/../app/AppKernel.php';

$kernel = new AppKernel('dev', true);
if( !isset($_COOKIE['XDEBUG_SESSION']) )
    $kernel->loadClassCache();
ErrorHandler::register();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
