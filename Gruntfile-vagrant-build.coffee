module.exports = (grunt) ->
  remoteGruntShellTaskConfig = (remoteTasks...) ->
    args = remoteTasks.map((taskName) -> "--task='#{taskName}'").join(" ")
    options:
      stdout: true
    command: "vagrant grunt #{args}"

  grunt.initConfig
    shell:
      vagrant_all: remoteGruntShellTaskConfig('clean', 'default')
      vagrant_app_js: remoteGruntShellTaskConfig('js')
      vagrant_app_css: remoteGruntShellTaskConfig('css')
    watch:
      options:
        interval: 1000
        dateFormat: (time) ->
          grunt.log.writeln "The watch finished in " + time + "ms at" + (new Date()).toString()
          grunt.log.writeln "Waiting for more changes..."
      app_css:
        files: ['app_frontend/css/**/*.{css,less}']
        tasks: ['shell:vagrant_app_css']
      app_js:
        files: ['app_frontend/js/**/*.{js,coffee}']
        tasks: ['shell:vagrant_app_js']
      gruntfile:
        options:
          reload: true
          atBegin: true
        files: ['Gruntfile*.coffee']
        tasks: ['shell:vagrant_all']

  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-shell')

  grunt.registerTask('default', ['watch'])