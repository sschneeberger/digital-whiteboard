# Setup

This project has a [Vagrant][] setup so it should be easy once you [get Vagrant intstalled][vagrant-install]. If not already done, install [VirtualBox][]. Once both Vagrant and VirtualBox is installed and working, rock on!

 [Vagrant]: http://vagrantup.com/
 [vagrant-install]: http://docs.vagrantup.com/v2/installation/index.html
 [VirtualBox]: https://www.virtualbox.org

## Booting up the VM

    host$ cd $PROJECT_DIR
    host$ vagrant up

This will boot up the vagrant machine. The first boot will take a while for it to prepare everyting to run the project. Once `vagrant up` runs through, the VM is ready.

## Using the VM

After Vagrant is done, the VM is reachable on IP `192.168.50.100`. To SSH into it, you can use `vagrant ssh` in the project directory. To have the localized domains working, add entries for the configured names to `/etc/hosts` (or the equivalent on Windows):

    192.168.50.100 de.digital-whiteboard.dev en.digital-whiteboard.dev

Try hitting <http://en.digital-whiteboard.dev/app_dev.php/> and there should be no errors.

The project's root directory is mounted to `/vagrant` inside the VM, so all shell commands should be executed there. There is also a working `grunt` executable, so to build the assets, change to the `/vagrant` directory and run `grunt` (or `grunt watch` to also watch for JS/CSS changes).

# Integration status in production server on Hetzner http://78.47.12.187

[![Build Status](http://78.47.12.187:8080/jenkins/buildStatus/icon?job=digital-whiteboard)](http://78.47.12.187:8080/jenkins/job/digital-whiteboard/)

# Developer Machine Setup

## reTurn Server

"reTurn is a highly efficient C++ open-source STUN/TURN server and client library. It is an implementation of the latest STUN/TURN RFCs: RFC5389 (STUN), and RFC5766 (TURN)."
This used for video/audio streaming.

    # you can use "vi" or "vim"
    vim /etc/reTurnServer.config

    # your IP addresses go here:
    TurnAddress = 192.168.50.100
    AltStunAddress = 192.168.50.100

    # your domain goes here:
    AuthenticationRealm = return-digital-whiteboard

    # your desired password goes here:
    LongTermAuthPassword = your-password

ReStart reTurnServer:
    service resiprocate-turn-server restart

You can check ports and addresses reTurnServer is listenning on:
    netstat -nlp | grep reTurnServer

##  Screen sharing

Full screen sharing requires following things:

    Chrome extension something like desktopCapture extension (https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk)
    Chrome command-line i.e. CL flag: --enable-usermedia-screen-capturing for non SSL site additional flag: --allow-http-screen-capture
    Firefox 33+ or Firefox nightly with two flags via about:config page

Firefox nightly requires:

    media.getusermedia.screensharing.enabled flag to be set true
    media.getusermedia.screensharing.allowed_domains flag to be used to store ASCII domain names e.g. localhost or www.webrtc-experiment.com

Chrome command-line flag can be enabled using guidelines explains here: http://www.chromium.org/developers/how-tos/run-chromium-with-flags


##  Dependency installation

    brew install memcached node
    php composer.phar install
    npm install -g grunt-cli

## Database creation

    php app/console doctrine:database:create
    php app/console doctrine:schema:create

# Development

## Memcache start

memcached

## Database Update

    php app/console doctrine:schema:update --force

## Development Fixtures load

    php app/console doctrine:fixtures:load

## Tests

Symfony2:

    php app/console doctrine:database:create --env=test
    php app/console doctrine:schema:create --env=test
    php app/console doctrine:fixtures:load --env=test
    bin/phpunit -c app/ --coverage-html=docs/symfony2-coverage

AngularJS:

    grunt test
    cd app_frontend/build/js/app
    karma start karma.js

## Generate PhpDoc

    bin/phpdoc -d ./src -t ./docs/symfony2-api --template="responsive-twig"

## Caches clear

    php app/console cache:clear
    php app/console memcache:clear default

## Assets watch

    grunt watch

`grunt watch` watches for changes in JS/CSS files in `app_frontend/` and takes the required steps to get the assets rebuilt. After that, `app/console assetic:dump` is called automatically so `assetic:watch` is not necessary.

## File Explorer WebSocket server

    php app/console file_explorer:server

## Presentation WebSocket server

    php app/console presentation:server

## Setting up exporting presentations as video and pdf

    tar -xJf vagrant/puppet/modules/ffmpeg/files/ffmpeg-2.4.2-64bit-static.tar.xz
    ln -s vagrant/puppet/modules/ffmpeg-2.4.2-64bit-static/ffmpeg /usr/bin/ffmpeg

## Trigger exporting video and pdf:

Open /trigger-export in browser

## Setting up cronjobs

Exporting video and pdf:

    0 1   * * *   root   php app/console presentation:export

Clean up script:

    0 1   * * *   root   php app/console presentation:cleanup


