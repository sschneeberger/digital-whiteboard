module.exports = (grunt) ->
  COMPOSER_VENDOR_DIR = do ->
    execResult = require('shelljs').exec("cd /vagrant; ./composer.phar config vendor-dir", {silent: true})
    if execResult.code == 0
      execResult.output.trim()
    else
      "vendor"

  BOWER_JS_FILES = [
    'app_frontend/bower_components/jquery/dist/jquery.js'
    'app_frontend/bower_components/jquery-ui/jquery-ui.js'
    'app_frontend/bower_components/jqueryui-touch-punch/jquery.ui.touch-punch.js'
    "app_frontend/bower_components/jstree/jquery.jstree.js"
    "app_frontend/bower_components/jquery-timer/jquery-timer.js"
    "app_frontend/bower_components/JSColor/jscolor.js"
    "app_frontend/bower_components/underscore/underscore.js"
    "app_frontend/bower_components/blueimp-canvas-to-blob/js/canvas-to-blob.js"
    "app_frontend/bower_components/angular/angular.js"
    "app_frontend/bower_components/angular-mocks/angular-mocks.js"
    "app_frontend/bower_components/angular-ui-sortable/sortable.js"
    "app_frontend/bower_components/angular-animate/angular-animate.js"
    "app_frontend/bower_components/angular-sanitize/angular-sanitize.js"
    "app_frontend/bower_components/modernizr/modernizr.js"
    'app_frontend/bower_components/bootstrap/dist/js/bootstrap.js'
    'app_frontend/bower_components/fabric/dist/fabric.js'
    'app_frontend/bower_components/autobahnjs/autobahn_v08.js'
    'node_modules/rtcmulticonnection/RTCMultiConnection.js'
    'node_modules/rtcmulticonnection/getScreenId.js'
    'node_modules/rtcmulticonnection/RecordRTC.js'
    'node_modules/rtcmulticonnection/screenshot.js'
  ]

  grunt.initConfig
    config:
      copyright_banner: "For licenses of used third-party libraries, see /attributions.txt"
    shell:
      assetic_dump:
        options:
          stdout: true
        command: 'php app/console assetic:dump'
    less:
      options:
        paths: [
          "app_frontend/bower_components/"
          "app_frontend/css"
        ]
        syncImport: true
      app:
        options:
          sourceMap: true
          sourceMapFilename: "app_frontend/build/css/source-maps/main.less.map"
          sourceMapURL: "/source-maps/main.less.map"
          outputSourceFiles: true
        files:
          "app_frontend/build/css/main.css": "app_frontend/css/main.less"
      fonts:
        files:
          "app_frontend/build/css/fonts.css": "app_frontend/css/fonts.less"
    concat:
      ie:
        files:
          "app_frontend/build/js/ie.js": [
            "app_frontend/bower_components/html5shiv/dist/html5shiv.js"
            "app_frontend/bower_components/respond/dest/respond.min.js"
          ]
      libs:
        files:
          "app_frontend/build/js/libs.js": BOWER_JS_FILES
    coffee:
      app:
        options:
          sourceMap: true
          sourceMapDir: "app_frontend/build/js/source-maps/"
        files: [{
          expand: true
          flatten: false
          cwd: "app_frontend/js/"
          src: ["**/!(*karma.config|*test).coffee"]
          dest: "app_frontend/build/js/app/"
          ext: ".js"
        }]
      test:
        options:
          sourceMap: true
          sourceMapDir: "app_frontend/build/js/source-maps/"
        files: [{
          expand: true
          flatten: false
          cwd: "app_frontend/js/"
          src: ["**/*.coffee"]
          dest: "app_frontend/build/js/app/"
          ext: ".js"
        }]
    copy:
      bower_fonts:
        files: [{
          src: ['app_frontend/bower_components/bootstrap/dist/fonts/*']
          dest: 'web/fonts/'
          expand: true
          flatten: true
        }]
      app_images:
        files: [{
          cwd: 'app_frontend/images'
          src: ['**/*.{jpg,jpeg,png,gif}']
          dest: 'web/images/'
          expand: true
          flatten: false
        }]
      app_fonts:
        files: [{
          cwd: 'app_frontend/fonts'
          src: ['**/*.{eot,woff,svg,ttf}']
          dest: 'web/fonts/'
          expand: true
          flatten: false
        }]
      app_js:
        files: [{
          cwd: 'app_frontend/js/'
          src: ['**/*.js']
          dest: 'app_frontend/build/js/app/'
          expand: true
          flatten: false
        }]
      coffee_source_maps:
        options:
          process: (content, srcpath) ->
            return content unless srcpath.match(/\.map$/)
            # fix source path
            sourceMap = JSON.parse(content)
            sourceMap.sourceRoot = "src/"
            JSON.stringify(sourceMap, null, " ")
        files: [{
        # Source maps
          expand: true
          flatten: true
          cwd: 'app_frontend/build/js/source-maps/'
          src: ['**/*.map']
          dest: "web/source-maps"
        }, {
        # Actual source files
          expand: true
          flatten: true
          cwd: 'app_frontend/js/'
          src: ['**/*.coffee']
          dest: "web/source-maps/src"
        }]
      less_source_maps:
        files: [{
          expand: true
          flatten: true
          cwd: 'app_frontend/build/css/source-maps/'
          src: ['**/*.map']
          dest: "web/source-maps"
        }]
    uglify:
      app:
        options:
          banner: "/* <%= config.copyright_banner %> */"
          mangle: true
          compress:
            dead_code: false
          preserveComments: false
        files: [{
          cwd: 'app_frontend/build/js/'
          src: '*.js'
          dest: 'app_frontend/build/js/'
          expand: true
        }]
    cssmin:
      options:
        banner: "/* <%= config.copyright_banner %> */"
        keepSpecialComments: 0
      app:
        files: [{
          cwd: 'app_frontend/build/css'
          src: '*.css'
          dest: 'app_frontend/build/css'
          expand: true
        }]
    clean:
      app: ['app_frontend/build/*', 'web/source-maps']
      assets: ['web/{fonts,images}']
    watch:
      options:
        interval: 1000
        dateFormat: (time) ->
          grunt.log.writeln "The watch finished in " + time + "ms at" + (new Date()).toString()
          grunt.log.writeln "Waiting for more changes..."
      app_css:
        files: ['app_frontend/css/**/*.{css,less}']
        tasks: ['less:app', 'shell:assetic_dump']
      app_js:
        files: ['app_frontend/js/**/*.{js,coffee}']
        tasks: ['js']
      gruntfile:
        options:
          reload: true
          atBegin: true
        files: ['Gruntfile.coffee']
        tasks: ['default']

  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-less')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-contrib-coffee')
  grunt.loadNpmTasks('grunt-shell')
  grunt.loadNpmTasks('grunt-contrib-concat')
  grunt.loadNpmTasks('grunt-contrib-copy')
  grunt.loadNpmTasks('grunt-contrib-uglify')
  grunt.loadNpmTasks('grunt-contrib-cssmin')
  grunt.loadNpmTasks('grunt-contrib-clean')

  grunt.registerTask('all', 'Build all assets', [
    'less:app'
    'concat:ie', 'concat:libs', 'coffee:app', 'copy:app_js'
    'copy:app_images'
    'less:fonts', 'copy:app_fonts', 'copy:bower_fonts'
  ])

  grunt.registerTask('test', 'Build tests for test mode', [
    'coffee:test'
  ])
  grunt.registerTask('dev', 'Build assets for dev mode', [
    'clean', 'all', 'copy:less_source_maps', 'copy:coffee_source_maps', 'shell:assetic_dump'
  ])
  grunt.registerTask('prod', 'Build assets for prod mode', [
    'clean', 'all', 'uglify:app', 'cssmin:app'
  ])

  grunt.registerTask('css', 'Build CSS in dev mode', [
    'less:app', 'copy:less_source_maps', 'shell:assetic_dump'
  ])
  grunt.registerTask('js', 'Build JS (without libs) in dev mode', [
    'coffee:app', 'copy:coffee_source_maps', 'copy:app_js', 'shell:assetic_dump'
  ])
  grunt.registerTask('assets', 'Build assets', [
    'copy:app_images', 'copy:app_fonts', 'less:fonts', 'shell:assetic_dump'
  ])
  grunt.registerTask('libs', 'Build all third party stuff', [
    'concat:ie', 'concat:libs', 'copy:bower_fonts', 'shell:assetic_dump'
  ])

  grunt.registerTask('default', ['dev'])