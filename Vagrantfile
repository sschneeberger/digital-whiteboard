require File.dirname(__FILE__) + "/vagrant/vagrant_grunt_plugin.rb"

VM_MEMORY = 512 unless defined?(VM_MEMORY)
VM_CORE_COUNT = 2 unless defined?(VM_CORE_COUNT)

Vagrant.configure("2") do |config|
  config.vm.box = "puppetlabs-debian-73"
  config.vm.box_url = "http://puppet-vagrant-boxes.puppetlabs.com/debian-73-x64-virtualbox-puppet.box"

  config.vm.hostname = "digital-whiteboard.dev"
  config.vm.network :private_network, ip: "192.168.50.100"

  config.vm.synced_folder ".", "/vagrant"

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", VM_MEMORY]
    vb.customize ["modifyvm", :id, "--cpus", VM_CORE_COUNT]

    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    vb.customize ["modifyvm", :id, "--vram", 4]
    vb.customize ["setextradata", :id, "--VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root", "1"]
  end

  config.vm.provider :vmware_fusion do |vmw, override|
    override.vm.box_url = "http://puppet-vagrant-boxes.puppetlabs.com/debian-70rc1-x64-vf503.box"
    override.vm.synced_folder ".", "/vagrant", type: "nfs"

    vmw.vmx['memsize'] = VM_MEMORY
    vmw.vmx['numvcpus'] = VM_CORE_COUNT
  end

  config.vm.provision :puppet do |puppet|
    puppet.manifest_file = "digital-whiteboard_dev.pp"
    puppet.manifests_path = "vagrant/puppet/manifests"
    puppet.module_path = "vagrant/puppet/modules"
    puppet.hiera_config_path = "vagrant/puppet/hiera.yaml"

    puppet.facter = { 'fqdn' => config.vm.hostname }
  end
end