<?php

namespace HTWG\DigitalWhiteboard\PresentationBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class DashboardControllerTest
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Tests\Controller
 */
class DashboardControllerTest extends WebTestCase
{
    /**
     * @var null
     */
    private $client = null;

    /**
     *
     */
    public function setUp()
    {
        $this->client = static::createClient();
        $this->client->followRedirects(true);
        $kernel = static::createKernel();
        $kernel->boot();
        $container = $kernel->getContainer();
        $session = $container->get('session');
        $em = $container->get('doctrine')->getManager();

        $user = $em->getRepository('HTWGDigitalWhiteboardPresentationBundle:User')->find(1);
        $firewall = 'secured_area';
        $token = new UsernamePasswordToken($user, "1234", $firewall, $user->getRoles());
        $session->set('_security_'.$firewall, serialize($token));
        $session->save();

        $this->client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));
    }

    /**
     * Test if dashboard is loaded if user is logged in.
     */
    public function testIndex()
    {
        $crawler = $this->client->request('GET', '/dashboard');

        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Dashboard")')->count());
    }
}
