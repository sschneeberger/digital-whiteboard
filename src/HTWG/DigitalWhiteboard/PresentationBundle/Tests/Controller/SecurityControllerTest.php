<?php

namespace HTWG\DigitalWhiteboard\PresentationBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class SecurityControllerTest
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Tests\Controller
 */
class SecurityControllerTest extends WebTestCase
{
    /**
     * @var null
     */
    private $client = null;

    /**
     *
     */
    public function setUp()
    {
        $this->client = static::createClient();
        $this->client->followRedirects(true);
        $this->client->request('GET', '/login');
    }

    /**
     * Test if login site is loaded.
     */
    public function testLoadLogin()
    {
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertGreaterThan(0, $this->client->getCrawler()->filter('html:contains("Login")')->count());
    }

    /**
     * Test if error appears if login is wrong.
     */
    public function testLoginError()
    {
        $form = $this->client->getCrawler()->selectButton('Login')->form();

        $form['_username'] = 'Lucas';
        $form['_password'] = '1234';

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertGreaterThan(0, $this->client->getCrawler()->filter('html:contains("Bad credentials.")')->count());
    }
}
