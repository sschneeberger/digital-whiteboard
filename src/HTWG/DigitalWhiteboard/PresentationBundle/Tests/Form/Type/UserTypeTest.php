<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/Tests/Form/Type/UserTypeTest.php

namespace HTWG\DigitalWhiteboard\PresentationBundle\Tests\Form\Type;

use HTWG\DigitalWhiteboard\PresentationBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Validator\Validation;

/**
 * Class UserTypeTest
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Tests\Form\Type
 */
class UserTypeTest extends WebTestCase{
    /**
     * @var
     */
    private $type;

    /**
     * @var
     */
    private $form;

    /**
     * @var
     */
    protected $factory;

    /*protected function setUp()
    {
        $client = static::createClient();
        $client->followRedirects(true);
        $kernel = static::createKernel();
        $kernel->boot();
        $container = $kernel->getContainer();
        $em = $container->get('doctrine')->getManager();

        $role = $em->getRepository('HTWGDigitalWhiteboardPresentationBundle:Role');

        $this->factory = Forms::createFormFactoryBuilder()
            ->addExtensions($this->getExtensions())
            ->getFormFactory();

        $this->type = new UserType($role);
        $this->form = $this->factory->create($this->type);
    } */

    /*protected function getExtensions()
    {
        //TODO: add preloaded roles_collection entity
        return array(
            new ValidatorExtension(Validation::createValidator()));
    }*/

    /**
     *
     */
    public function testSubmitValidData()
    {}

    /**
     * Isolated test for user register form.
     */
    /*public function testSubmitValidData()
    {
        $formData = [
            'firstName' => 'Paul',
            'lastName' => 'Tester',
            'username' => 'ptester',
            'role' => ""
        ];

        $this->form->submit($formData);

        $this->assertFalse($this->form->isValid());
        $this->assertEquals($formData, $this->form->getData());
    }

    public function testFirstNameCantBeEmpty()
    {
        $formData = [
            'lastName' => 'ELek',
            'email' => 'test@test.com',
            'description' => 'Everything is fine',
        ];

        $this->form->submit($formData);

        $expectedErrors = [
            'firstName' => 'This value should not be blank.',
            'attachment' => 'This value should not be blank.'
        ];

        $this->assertFalse($this->form->isValid());
        $this->assertEquals($expectedErrors, $this->gatherErrors($this->form));

    }

    private function gatherErrors(Form $form)
    {
        $errors = [];
        foreach($form->all() as $id => $sub) {
            foreach ($sub->getErrors() as $err) {
                $errors[$id] = $err->getMessage();
            }
        }

        return $errors;
    }*/
}