<?php

namespace HTWG\DigitalWhiteboard\PresentationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Presentation
 *
 * @ORM\Table(name="presentation")
 * @ORM\Entity(repositoryClass="HTWG\DigitalWhiteboard\PresentationBundle\Repository\PresentationRepository")
 *
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Entity
 */
class Presentation
{
    const STATE_STARTED = 0;

    const STATE_ENDED = 1;

    const STATE_EXPORTED = 2;

    const STATE_DELETED = 3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var date
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="presentations")
     * @ORM\JoinColumn(name="id_owner", referencedColumnName="id")
     **/
    private $owner;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer", length=12)
     */
    private $duration;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer", length=1)
     */
    private $state;


    public function __construct(){
        $this->state = self::STATE_STARTED;
        $this->duration = 0;
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Presentation
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date->format('d.m.Y, H:i');
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Presentation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Presentation
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return Presentation
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set owner
     *
     * @param \HTWG\DigitalWhiteboard\PresentationBundle\Entity\User $owner
     * @return Presentation
     */
    public function setOwner(\HTWG\DigitalWhiteboard\PresentationBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \HTWG\DigitalWhiteboard\PresentationBundle\Entity\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }
}
