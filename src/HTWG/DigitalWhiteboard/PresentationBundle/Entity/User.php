<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/Entity/User.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="HTWG\DigitalWhiteboard\PresentationBundle\Repository\UserRepository")
 *
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Entity
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="user_name", type="string", length=25, unique=true)
     */
    protected $username;

    /**
     */
    protected $plainPassword;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Length(max = 4096)
     */
    protected $password;

    /**
     * @ORM\Column(name="first_name", type="string", length=25, unique=true)
     */
    protected $firstname;

    /**
     * @ORM\Column(name="last_name", type="string", length=25, unique=true)
     */
    protected $lastname;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity="Presentation", mappedBy="owner")
     **/
    private $presentations;

    /**
     */
    private $retype_password;

    /**
     */
    private $rolesCollection;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users", cascade={"all"}))
     *
     */
    private $roles;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->presentations = new ArrayCollection();
        $this->isActive = true;
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid(null, true));
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getRetypePassword()
    {
        return $this->retypePassword;
    }

    /**
     *
     */
    public function getRolesCollection()
    {
        return $this->rolesCollection;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return $this->roles->toArray();
    }

    /**
     * @param Collection $collection
     */
    public function setRolesCollection($collection)
    {
        $this->addRole($collection);
    }

    public function setRoles($roles){
        $this->roles = $roles;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add role
     *
     * @param \HTWG\DigitalWhiteboard\PresentationBundle\Entity\Role $role
     * @return User
     */
    public function addRole(\HTWG\DigitalWhiteboard\PresentationBundle\Entity\Role $role)
    {
        $this->roles[] = $role;

        return $this;
    }

    /**
     * Remove role
     *
     * @param \HTWG\DigitalWhiteboard\PresentationBundle\Entity\Role $role
     */
    public function removeRole(\HTWG\DigitalWhiteboard\PresentationBundle\Entity\Role $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * Check if Password is legal
     *
     * @return Bool
     */
    public function isPasswordLegal()
    {
        return $this->firstname != $this->password;
    }

    /**
     * Add presentations
     *
     * @param \HTWG\DigitalWhiteboard\PresentationBundle\Entity\Presentation $presentations
     * @return User
     */
    public function addPresentation(\HTWG\DigitalWhiteboard\PresentationBundle\Entity\Presentation $presentations)
    {
        $this->presentations[] = $presentations;

        return $this;
    }

    /**
     * Remove presentations
     *
     * @param \HTWG\DigitalWhiteboard\PresentationBundle\Entity\Presentation $presentations
     */
    public function removePresentation(\HTWG\DigitalWhiteboard\PresentationBundle\Entity\Presentation $presentations)
    {
        $this->presentations->removeElement($presentations);
    }

    /**
     * Get presentations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPresentations()
    {
        return $this->presentations;
    }
}
