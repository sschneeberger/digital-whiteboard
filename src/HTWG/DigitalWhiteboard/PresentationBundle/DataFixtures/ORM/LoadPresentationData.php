<?php

namespace HTWG\DigitalWhiteboard\PresentationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\ORM\EntityManager;
use HTWG\DigitalWhiteboard\PresentationBundle\Entity\Presentation;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

use HTWG\DigitalWhiteboard\PresentationBundle\Entity\User;

/**
 * Class LoadPresentationData
 * @package HTWG\DigitalWhiteboard\PresentationBundle\DataFixtures\ORM
 */
class LoadPresentationData extends AbstractFixture implements OrderedFixtureInterface {

    /**
     * @var EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $presentations = [ [ "name" => "AngularJS - Introduction",
                             "date" => new \DateTime('14-07-07 15:00:01'),
                             "owner" => "admin",
                             "duration" => 0,
                             "state" => Presentation::STATE_STARTED],
                           [ "name" => "C++ - pointer arithmetics",
                             "date" => new \DateTime('14-07-07 16:30:00'),
                             "owner" => "test",
                             "duration" => 0,
                             "state" => Presentation::STATE_STARTED],
                           [ "name" => "Algorithmen und Datenstrukturen",
                             "date" => new \DateTime('14-07-07 12:00:00'),
                             "owner" => "admin",
                             "duration" => 0,
                             "state" => Presentation::STATE_ENDED],
                           [ "name" => "Algorithmen und Datenstrukturen 2",
                             "date" => new \DateTime('14-07-06 10:00:00'),
                             "owner" => "admin",
                             "duration" => 0,
                             "state" => Presentation::STATE_ENDED],
                           [ "name" => "Bresenham algorithm Part 1",
                             "date" => new \DateTime('14-07-05 08:00:00'),
                             "owner" => "admin",
                             "duration" => 0,
                             "state" => Presentation::STATE_EXPORTED],
                           [ "name" => "Bresenham algorithm Part 2",
                             "date" => new \DateTime('14-07-04 09:45:00'),
                             "owner" => "admin",
                             "duration" => 0,
                             "state" => Presentation::STATE_EXPORTED],
                           [ "name" => "Bresenham algorithm Part 3",
                             "date" => new \DateTime('14-07-03 10:15:00'),
                             "owner" => "admin",
                             "duration" => 0,
                             "state" => Presentation::STATE_EXPORTED],
                           [ "name" => "Big Data and NOSQL",
                             "date" => new \DateTime('14-07-02 11:10:00'),
                             "owner" => "admin",
                             "duration" => 0,
                             "state" => Presentation::STATE_EXPORTED],
                           [ "name" => "Search Engine Optimation, Google Crawler and how tey work",
                             "date" => new \DateTime('14-07-01 12:30:00'),
                             "owner" => "admin",
                             "duration" => 0,
                             "state" => Presentation::STATE_EXPORTED],
                           [ "name" => "Lange Programmiernacht: PHP",
                             "date" => new \DateTime('14-06-30 12:45:00'),
                             "owner" => "admin",
                             "duration" => 0,
                             "state" => Presentation::STATE_EXPORTED],
                           [ "name" => "Lange Programmiernacht: ASP und .net",
                             "date" => new \DateTime('14-06-28 18:00:00'),
                             "owner" => "admin",
                             "duration" => 0,
                             "state" => Presentation::STATE_EXPORTED],
                           [ "name" => "Lange Programmiernacht: Ruby on Rails",
                             "date" => new \DateTime('14-06-02 20:00:00'),
                             "owner" => "admin",
                             "duration" => 0,
                             "state" => Presentation::STATE_EXPORTED],
                           [ "name" => "Scala: Case Classes",
                             "date" => new \DateTime('14-06-01 16:30:00'),
                             "owner" => "admin",
                             "duration" => 0,
                             "state" => Presentation::STATE_EXPORTED]
        ];

        foreach($presentations as $presentation) {
            $obj = $this->addPresentation($presentation);
            $user = $manager->getRepository('HTWGDigitalWhiteboardPresentationBundle:User')
                ->findOneBy(array('username' => $presentation["owner"]));
            $obj->setOwner($user);
            $manager->persist($obj);
        }

        $manager->flush();
    }

    /**
     * @param array $presentation
     * @return Presentation
     */
    private function addPresentation(array $presentation)
    {
        $obj = new Presentation();
        $obj->setName($presentation['name']);
        $obj->setDate($presentation['date']);
        $obj->setDuration($presentation['duration']);
        $obj->setState($presentation['state']);

        return $obj;
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3; // the order in which fixtures will be loaded
    }
}

?>