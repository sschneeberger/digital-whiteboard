<?php

namespace HTWG\DigitalWhiteboard\PresentationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

use HTWG\DigitalWhiteboard\PresentationBundle\Entity\Role;
use HTWG\DigitalWhiteboard\PresentationBundle\Entity\User;

/**
 * Class LoadUserData
 * @package HTWG\DigitalWhiteboard\PresentationBundle\DataFixtures\ORM
 */
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface{

    /**
     * @var EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $users = [ [ "username" => "admin",
                     "password" => password_hash('1234', PASSWORD_BCRYPT, array('cost' => 12)),
                     "first_name" => "Admin",
                     "last_name" => "Administrator",
                     "role" => "Admin" ],
                   [ "username" => "test",
                     "password" => password_hash('1234', PASSWORD_BCRYPT, array('cost' => 12)),
                     "first_name" => "Test",
                     "last_name" => "Tester",
                     "role" => "Presenter"],
                   [ "username" => "max",
                     "password" => password_hash('1234', PASSWORD_BCRYPT, array('cost' => 12)),
                     "first_name" => "Max",
                     "last_name" => "Mustermann",
                     "role" => "Viewer"],
                   [ "username" => "hans",
                     "password" => password_hash('1234', PASSWORD_BCRYPT, array('cost' => 12)),
                     "first_name" => "Hans",
                     "last_name" => "Vogel",
                     "role" => "Viewer"]
        ];

        foreach($users as $user) {
            $obj = $this->addUser($user);
            $role = $manager->getRepository('HTWGDigitalWhiteboardPresentationBundle:Role')
                            ->findOneBy(array('name' => $user["role"]));

            $obj->addRole($role);

            $manager->persist($obj);
        }

        $manager->flush();
    }

    /**
     * @param array $user
     * @return User
     */
    private function addUser(array $user)
    {
        $obj = new User();
        $obj->setUsername($user['username']);
        $obj->setFirstname($user['first_name']);
        $obj->setLastname($user['last_name']);
        $obj->setPassword($user['password']);

        return $obj;
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}

?>