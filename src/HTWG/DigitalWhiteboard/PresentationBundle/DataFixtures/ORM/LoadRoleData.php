<?php

namespace HTWG\DigitalWhiteboard\PresentationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use HTWG\DigitalWhiteboard\PresentationBundle\Entity\Role;

/**
 * Class LoadRoleData
 * @package HTWG\DigitalWhiteboard\PresentationBundle\DataFixtures\ORM
 */
class LoadRoleData extends AbstractFixture implements OrderedFixtureInterface {

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $roles = [ ["name" => "Admin",
                    "role" => "ROLE_ADMIN" ],
                   ["name" => "Presenter",
                    "role" => "ROLE_PRESENTER" ],
                   ["name" => "Viewer",
                    "role" => "ROLE_VIEWER"] ];

        foreach($roles as $role) {
            $obj = $this->addRole($role);
            $manager->persist($obj);
        }

        $manager->flush();
    }

    /**
     * @param array $role
     * @return Role
     */
    private function addRole(array $role)
    {
        $obj = new Role();
        $obj->setRole($role['role']);
        $obj->setName($role['name']);

        return $obj;
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}

?>