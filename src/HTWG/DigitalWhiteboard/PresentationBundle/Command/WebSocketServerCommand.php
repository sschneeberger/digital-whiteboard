<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/Command/WebSocketServerCommand.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Command;

use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\Wamp\WampServer;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\Factory;
use React\Socket\Server;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class WebSocketServerCommand
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Command
 */
class WebSocketServerCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('presentation:server')
            ->setDescription('Start the presentation server');
    }

    /**
     * Starts WebSocket for presentation. Implemented with RatchetPHP.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        echo "presentation websocket is listening...\n";

        $webSocket = $this->getContainer()->get('presentation.websocket_server');
        $webSocketPort = $this->getContainer()->getParameter('presentation.websocket_port');
        $webSocketAddress = $this->getContainer()->getParameter('presentation.websocket_address');

        $loop   = Factory::create();

        // Set up our WebSocket server for clients wanting real-time updates
        $webSock = new Server($loop);
        $webSock->listen($webSocketPort, $webSocketAddress); // Binding to 0.0.0.0 means remotes can connect
        $webServer = new IoServer(
            new HttpServer(
                new WsServer(
                    new WampServer(
                        $webSocket
                    )
                )
            ),
            $webSock
        );

        $loop->run();
    }
}