<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/Command/WebSocketWebRTCCommand.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Command;

use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\Wamp\WampServer;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\Factory;
use React\Socket\Server;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class WebSocketWebRTCCommand
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Command
 */
class WebSocketWebRTCCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('presentation:webrtc')
            ->setDescription('Start the webrtc server');
    }

    /**
     * Starts WebRTC WebSocket Server.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        echo "webrtc websocket is listening...\n";

        $webSocket = $this->getContainer()->get('presentation.webrtc_server');
        $webSocketPort = $this->getContainer()->getParameter('presentation.webrtc_port');
        $webSocketAddress = $this->getContainer()->getParameter('presentation.websocket_address');

        $loop   = Factory::create();

        // Set up our WebSocket server for clients wanting real-time updates
        $webSock = new Server($loop);
        $webSock->listen($webSocketPort, $webSocketAddress); // Binding to 0.0.0.0 means remotes can connect
        $webServer = new IoServer(
            new HttpServer(
                new WsServer(
                    $webSocket
                )
            ),
            $webSock
        );

        $loop->run();
    }
}