<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/Command/CleanUpCommand.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Command;

use HTWG\DigitalWhiteboard\PresentationBundle\Entity\Presentation;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CleanUpCommand
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Command
 */
class CleanUpCommand extends ContainerAwareCommand
{
    /**
     * @var null
     */
    private $redis = null;

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('presentation:cleanup')
            ->setDescription('Clean up all persisted data');
    }

    /**
     * Clean up all deleted presentations.
     * Removes content in redis and on file system.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(6000);

        $em = $this->getContainer()->get('doctrine')->getManager();
        $presentations = $em->getRepository('HTWGDigitalWhiteboardPresentationBundle:Presentation')
            ->findByState(Presentation::STATE_DELETED);

        $countPresentations = count( $presentations );
        echo "Found " . $countPresentations . " Presentations.\n";

        if ( $countPresentations )
        {
            echo "Start deleting...\n";

            $this->redis = $this->getContainer()->get('snc_redis.presentation');

            foreach( $presentations as $presentation ){
                $dir = $this->getUploadDir($presentation);
                $this->rrmdir($dir);
                $key = "user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId();
                $this->redis->DEL( $key.':messages' );
                $this->redis->DEL( $key.':slides:contents' );
                $this->redis->DEL( $key.':slides:images' );
                $this->redis->DEL( $key.':slides' );
                $this->redis->DEL( $key.':recordImages' );
                $this->redis->DEL( $key.':createdTime' );
                $this->redis->DEL( $key );
                $owner = $presentation->getOwner();
                $owner->removePresentation($presentation);
                $em->remove($presentation);
            }

            $em->flush();

            echo "Deleting done.\n";
        }
        else
        {
            echo "Nothing to do.\n";
        }

    }

    /**
     * @return string
     */
    private function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../../web/storage';
    }

    /**
     * @param $presentation
     * @return string
     */
    private function getUploadDir($presentation)
    {
        // the absolute directory path where uploaded documents should be saved
        return $this->getUploadRootDir() . '/' . $presentation->getOwner()->getUsername() . '/presentation_' .$presentation->getId() ;
    }

    /**
     * Helper function for removing directory recursively.
     *
     * @param $dir
     * @source http://www.php.net/manual/de/function.rmdir.php#108113
     */
    private function rrmdir ( $dir )
    {
        foreach ( glob($dir . '/*') as $file )
        {
            if ( is_dir( $file ) )
            {
                $this->rrmdir( $file );
            }
            else
            {
                unlink( $file );
            }
        }
        rmdir( $dir );
    }
}