<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/Command/ExportingCommand.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Command;

use HTWG\DigitalWhiteboard\PresentationBundle\Entity\Presentation;
use JMS\DiExtraBundle\Annotation\AfterSetup;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ExportingCommand
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Command
 */
class ExportingCommand extends ContainerAwareCommand
{
    /**
     * @var null
     */
    private $redis = null;

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('presentation:export')
            ->setDescription('Export video and pdf');
    }

    /**
     * Generates video and pdf of ended presentations.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(6000);

        $em = $this->getContainer()->get('doctrine')->getManager();
        $presentations = $em->getRepository('HTWGDigitalWhiteboardPresentationBundle:Presentation')
            ->findByState(Presentation::STATE_ENDED);

        $countPresentations = count( $presentations );
        echo "Found " . $countPresentations . " presentations.\n";

        if ( $countPresentations )
        {
            echo "Start exporting...\n";

            $this->redis = $this->getContainer()->get('snc_redis.presentation');

            foreach( $presentations as $presentation ){
                $this->exportVideo($presentation);
                $this->exportPDF($presentation);
                $presentation->setState(Presentation::STATE_EXPORTED);
                $em->persist($presentation);
            }

            $em->flush();

            echo "Exporting done.\n";
        }
        else
        {
            echo "Nothing to do.\n";
        }

    }

    /**
     * Creates audio and video file with ffmpeg and sox:
     * 1. Create silent audio file with the length of the duration of presentation.
     * 2. Mix recorded audio chunks to this silent audio file if audio chunks exists.
     * 3. Creates jpg images out of recorded canvas blobs stored in redis if blobs exists
     * 4. Create final video with audio file and jpg images and store video file to users storage
     * 5. Clean up.
     *
     * @param $presentation
     */
    private function exportVideo($presentation)
    {
        $recordedImages = "user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ':recordImages';
        $createdTime = "user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ':createdTime';
        $countImages = $this->redis->LLEN($recordedImages);
        $dir = $this->getUploadDir($presentation);

        if ( $countImages > 0 )
        {
            exec("sox -n -r 48000 -c 2 " . $dir . "/silence.wav trim 0.0 " . $presentation->getDuration().".0");
            exec( "ffmpeg -i ". $dir . "/silence.wav -ab 128k -y " . $dir . "/silence.mp3 > /dev/null 2>&1");
            unlink($dir . "/silence.wav");

            $this->exportAudio($dir);
            $imageDir = $this->getUploadDirImages($presentation);

            for( $i = 0; $i < $countImages; $i++ )
            {
                $imageBlob = $this->redis->LPOP($recordedImages);
                $created = $this->redis->LPOP($createdTime);
                //TODO: use webp if php 5.5
                $imageBase64 = str_replace("data:image/jpeg;base64,","",$imageBlob);
                $im = imagecreatefromstring(base64_decode($imageBase64));
                imagejpeg($im, $imageDir.'/' . $i. '.jpg', 75);
                if ( $created == 0 )
                {
                    $date = date( "Y-m-d H:i:s", 0);
                    $d = $date . ".000";
                }
                else
                {
                    if ( $created < 1000 )
                    {
                        $time = 0;
                    }
                    else
                    {
                        $time = substr($created, 0, -3);
                    }
                    $microseconds = substr($created, -3);
                    $date = date( "Y-m-d H:i:s", $time);
                    $d = $date . "." . $microseconds;
                }

                exec("touch -d \"" . $d .  "\" " . $imageDir.'/' . $i. '.jpg' );
            }

            $audio = "-i " . $dir . "/silence.mp3";
            if ( file_exists($dir . "/audio.mp3"))
            {
                $audio = "-i " . $dir . "/audio.mp3 -af 'volume=8'";
            }

            exec("ffmpeg -ts_from_file 2 -i ". $imageDir . "/%d.jpg ". $audio ." -c:v libx264 -r 30 -s 640x400 -t " . $presentation->getDuration() . " " . $dir . "/video.mp4" );

            $this->rrmdir($imageDir);
            if ( is_dir($dir.'/tmpAudioChunks') )
            {
                $this->rrmdir($dir.'/tmpAudioChunks');
            }
            unlink($dir . "/silence.mp3");
            if ( file_exists($dir . "/audio.mp3"))
            {
                unlink($dir . "/audio.mp3");
            }
        }
    }

    /**
     * @return string
     */
    private function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../../web/storage';
    }

    /**
     * @param $presentation
     * @return string
     */
    private function getUploadDir($presentation)
    {
        $dir = $this->getUploadRootDir() . '/'. $presentation->getOwner()->getUsername();

        if( !is_dir( $dir ) )
        {
            mkdir( $dir, 0755, true );
            chown( $dir, "www-data");
        }

        $dir = $this->getUploadRootDir() . '/'. $presentation->getOwner()->getUsername() . '/presentation_' . $presentation->getId();

        if( !is_dir( $dir ) )
        {
            mkdir( $dir, 0755, true );
            chown( $dir, "www-data");
        }

        return $dir;
    }

    /**
     * @param $presentation
     * @return string
     */
    private function getUploadDirImages($presentation)
    {
        $dir = $this->getUploadDir($presentation) . '/tmpImages';

        if( !is_dir( $dir ) )
        {
            mkdir( $dir, 0755, true );
        }

        return $dir;
    }

    /**
     * Helper function for removing directory recursively.
     *
     * @param $dir
     * @source http://www.php.net/manual/de/function.rmdir.php#108113
     */
    private function rrmdir ( $dir )
    {
        foreach ( glob($dir . '/*') as $file )
        {
            if ( is_dir( $file ) )
            {
                $this->rrmdir( $file );
            }
            else
            {
                unlink( $file );
            }
        }
        rmdir( $dir );
    }

    /**
     * Set recorded audio chunks to specific positions in silent audio file.
     *
     * @param $dir
     */
    private function exportAudio($dir)
    {
        $sox = 'sox -m ' . $dir . '/silence.mp3';

        if ( is_readable($dir . '/tmpAudioChunks') and count(scandir($dir . '/tmpAudioChunks')) > 2)
        {
            $files = scandir($dir . '/tmpAudioChunks');

            foreach( $files as $i => $mp3 )
            {
                if ( mime_content_type($dir . '/tmpAudioChunks/' . $mp3) === "audio/mpeg" )
                {
                    $time = substr($mp3, 0, -4);
                    $sox .= ' "|sox '.$dir .'/tmpAudioChunks/'.$mp3.' -p pad '.$time.'"';

                    if ( $i === count($files) - 1 )
                    {
                        $sox .= ' ' .$dir.'/audio.mp3';
                    }
                }
            }
        }

        exec($sox);
    }

    /**
     * Create pdf out of slide images which are stored as blobs in redis.
     *
     * @param $presentation
     */
    private function exportPDF($presentation)
    {
        $dir = $this->getUploadDir($presentation);
        $presentationService = $this->getContainer()->get('presentation.presentation');
        $images = $presentationService->getSlideImages($presentation);

        if ( count($images) )
        {
            $owner = $presentation->getOwner();
            $author = $owner->getFirstname() . ' ' . $owner->getLastname();
            $imageDir = $this->getUploadDirImages($presentation);
            $pdf = new \TCPDF('L', 'mm', 'A4', true, 'UTF-8', false);
            $pdf->SetCreator('Digital Whiteboard');
            $pdf->SetAuthor($author);
            $pdf->SetTitle($presentation->getName());
            $pdf->SetSubject('Webinar');
            $pdf->SetMargins(15, 27, 15);
            $pdf->SetHeaderMargin(5);
            $pdf->SetFooterMargin(10);
            $pdf->SetFont('helvetica', '', 14, '', true);
            $html = "<h1>".$presentation->getName()."</h1><br/><p>" . $author . ", " . date( "d.m.Y", time() )."</p>";
            $pdf->setXY(100, 200);
            $pdf->AddPage();
            $pdf->WriteHTML($html);

            foreach( $images as $i => $image )
            {
                $imageBase64 = str_replace("data:image/png;base64,","",$image);
                $im = imagecreatefromstring(base64_decode($imageBase64));
                imagepng($im, $imageDir . '/' . $i. '.png', 9);
                $pdf->Image( $imageDir . '/' . $i . '.png', 50, 50 );
            }
            $pdf->Output( $dir . '/report.pdf', 'F' );
            $this->rrmdir($imageDir);
        }
    }

}