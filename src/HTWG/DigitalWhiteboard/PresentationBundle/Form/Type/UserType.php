<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/Form/Type/UserType.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserType
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Form\Type
 */
class UserType extends AbstractType
{
    /**
     * @param $repository
     */
    public function __construct($repository){
        $this->repository = $repository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAction($options['action']);
        $builder
            ->add('username', 'text', array(
                'required' => true,
                'label' => 'form.username',
                'attr' => [
                    'placeholder' => 'form.username'
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ],
                'invalid_message' => 'form.error'
            ))
            ->add('firstname', 'text', array(
                'required' => true,
                'label' => "form.first_name",
                'attr' => [
                    'placeholder' => 'form.first_name'
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ))
            ->add('lastname', 'text', array(
                'required' => true,
                'label' => "form.last_name",
                'attr' => [
                    'placeholder' => 'form.last_name'
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ))
            ->add('password', 'repeated', array(
                'first_name' => 'plain_password',
                'second_name' => 'retype_password',
                'first_options'  => array(
                    'label' => 'form.password',
                    'attr' => [
                        'placeholder' => 'form.password'
                     ],
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank(),
                    ]
                ),
                'second_options' => array(
                    'label' => 'form.retype_password',
                    'attr' => [
                        'placeholder' => 'form.retype_password'
                    ],
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank(),
                    ]
                ),
                'type'        => 'password',
                'required' => true,
                'property_path' => 'plainPassword',
                'invalid_message' => 'form.password_not_equal',
            ))
            ->add('rolesCollection', 'entity', array(
                'class' => 'HTWGDigitalWhiteboardPresentationBundle:Role',
                'data' => $this->repository->findOneByName("Viewer"),
                'property' => 'name',
                'required' => true,
                'label' => "form.roles",
                'expanded' => true,
                'multiple' => false,
                'query_builder' => function() {
                        return $this->repository->createQueryBuilder('r')
                            ->where("(r.name = 'Presenter') OR (r.name = 'Viewer')")
                            ->orderBy('r.name', 'ASC');
                    },
            ))
            ->add('submit', 'submit', array(
                'attr' => array('class' => 'create'),
                'label' => 'form.create'
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'common',
            'data_class' => 'HTWG\DigitalWhiteboard\PresentationBundle\Entity\User'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'digital_whiteboard_user';
    }
}

?>