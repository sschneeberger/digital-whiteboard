<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/Form/Type/PresentationType.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PresentationType
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Form\Type
 */
class PresentationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAction($options['action']);
        $builder
            ->add('name', 'text', array(
                'required' => true,
                'label' => "Name",
                'attr' => [
                    'placeholder' => 'Name'
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ))
            ->add('submit', 'submit', array(
                'attr' => array('class' => 'save'),
                'label' => 'Start'
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'common',
            'data_class' => 'HTWG\DigitalWhiteboard\PresentationBundle\Entity\Presentation'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'digital_whiteboard_presentation';
    }


}
