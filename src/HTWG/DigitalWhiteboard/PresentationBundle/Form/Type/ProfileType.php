<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/Form/Type/UserType.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ProfileType
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Form\Type
 */
class ProfileType extends AbstractType
{
    /**
     * @param $repository
     */
    public function __construct($repository){
        $this->repository = $repository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAction($options['action']);
        $builder
            ->add('firstname', 'text', array(
                'required' => true,
                'label' => "form.first_name",
                'attr' => [
                    'placeholder' => 'form.first_name'
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ))
            ->add('lastname', 'text', array(
                'required' => true,
                'label' => "form.last_name",
                'attr' => [
                    'placeholder' => 'form.last_name'
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ))
            ->add('current_password', 'password', array(
                'label' => 'form.current_password',
                'attr' => [
                    'placeholder' => 'form.current_password'
                ],
                'mapped' => false,
                'constraints' => new UserPassword(),
            ))
            ->add('password', 'repeated', array(
                'first_name' => 'plain_password',
                'second_name' => 'retype_password',
                'first_options'  => array(
                    'label' => 'form.new_password',
                    'attr' => [
                        'placeholder' => 'form.retype_new_password'
                    ],
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank(),
                    ]
                ),
                'second_options' => array(
                    'label' => 'form.retype_password',
                    'attr' => [
                        'placeholder' => 'form.retype_password'
                    ],
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank(),
                    ]
                ),
                'type'        => 'password',
                'required' => true,
                'property_path' => 'plainPassword',
                'invalid_message' => 'form.password_not_equal',
            ))
            ->add('submit', 'submit', array(
                'attr' => array('class' => 'save'),
                'label' => 'form.save'
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'common',
            'data_class' => 'HTWG\DigitalWhiteboard\PresentationBundle\Entity\User'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'digital_whiteboard_edit_profile';
    }
}

?>