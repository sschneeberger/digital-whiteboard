<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/PresentationService.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Service;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Predis\Client;
use Symfony\Bridge\Doctrine\ManagerRegistry;

//TODO: atomic actions if collaborative editing of presentation
/**
 * Class PresentationService
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Service
 */
class PresentationService {

    /**
     * @var Redis
     */
    private $redis;

    /**
     * @var em
     */
    private $em;

    /**
     * @var managerRegistry
     */
    private $managerRegistry;

    /**
     * @param Client $redis
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(Client $redis, ManagerRegistry $managerRegistry)
    {
        $this->redis = $redis;
        $this->managerRegistry = $managerRegistry;
        $this->em = $managerRegistry->getManager();
    }

    /**
     * Reset doctrine service connection for long running socket script.
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    private function checkDoctrineConnection()
    {
        $connection = $this->em->getConnection();
        try
        {
            $connection->query('SELECT 1');
        }
        catch (DBALException $e)
        {
            if ($connection->getTransactionIsolation())
            {
                // failed in the middle of a transaction - this is serious!
                throw $e;
            }

            // force instantiation of a new entity manager
            $this->managerRegistry->resetManager();
            $this->em = $this->managerRegistry->getManager();
        }
    }

    /**
     * @param $userID
     * @return mixed
     */
    public function getUser($userID)
    {
        $this->checkDoctrineConnection();

        $user = $this->em
            ->getRepository("HTWGDigitalWhiteboardPresentationBundle:User")
            ->find($userID);

        return $user;
    }

    /**
     * @param $presentationID
     * @return mixed
     */
    public function getPresentation($presentationID)
    {
        $this->checkDoctrineConnection();

        $presentation = $this->em
            ->getRepository("HTWGDigitalWhiteboardPresentationBundle:Presentation")
            ->find($presentationID);

        return $presentation;
    }

    /**
     * Store chat message to redis.
     *
     * @param $presentation
     * @param $msg
     * @return mixed
     */
    public function addMessage($presentation, $msg)
    {
        return $this->redis->RPUSH("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId(). ':messages', $msg);
    }

    /**
     * @param $presentation
     * @return mixed
     */
    public function getMessages($presentation)
    {
        return $this->redis->LRANGE("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId(). ':messages', -50, -1);
    }

    /**
     * @param $presentation
     * @return mixed
     */
    public function getLastMessage($presentation)
    {
        return $this->redis->LRANGE("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId(). ':messages', -1, -1)[0];
    }

    /**
     * Get content and images of slides of a specific presentation.
     *
     * @param $presentation
     * @return array
     */
    public function getSlides($presentation)
    {
        $result = array();

        if ( $this->redis->HEXISTS("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId(). ':slides:contents', 0 ) )
        {
            $contents = $this->redis->HGETALL("user_" . $presentation->getOwner()->getId() . ":presentation_" . $presentation->getId() . ":slides:contents" );
            ksort($contents);
            $result["contents"] = $contents;
        }
        if ( $this->redis->HEXISTS("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId(). ':slides:images', 0 ) )
        {
            $result["images"] = $this->getSlideImages($presentation);
        }

        return $result;
    }

    /**
     * @param $presentation
     * @return mixed
     */
    public function getSlideImages($presentation)
    {
        $images = $this->redis->HGETALL("user_" . $presentation->getOwner()->getId() . ":presentation_" . $presentation->getId() . ":slides:images" );
        ksort($images);
        return $images;
    }

    /**
     * Get content of slides of a specific presentation for viewer.
     *
     * @param $presentation
     * @return array
     */
    public function getSlidesForViewer($presentation)
    {
        $result = array();

        if ( $this->redis->HEXISTS("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId(). ':slides:contents', 0 ) )
        {
            $contents = $this->redis->HGETALL("user_" . $presentation->getOwner()->getId() . ":presentation_" . $presentation->getId() . ":slides:contents" );
            ksort($contents);
            $result["contents"] = $contents;
        }

        return $result;
    }

    /**
     * Get record time. If record time is not set, set it to 0.
     *
     * @param $presentation
     * @return mixed
     */
    public function getRecordTime($presentation)
    {
        if ( !$this->redis->HEXISTS("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId(), "recordTime" ) )
        {
            $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId(), 'recordTime', 0);
        }

        return $this->redis->HGET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId(), "recordTime");
    }

    /**
     * @param $presentation
     * @return mixed
     */
    public function getActiveSlide($presentation)
    {
        return $this->redis->HGET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides","activeSlide");
    }

    /**
     * @param $presentation
     */
    public function updateDate($presentation)
    {
        $presentation->setDate(new \DateTime());
        $this->em->persist($presentation);
        $this->em->flush();
    }

    /**
     * @param $userID
     * @param $presentation
     * @param $slideIndex
     */
    public function setActiveSlide($userID, $presentation, $slideIndex)
    {
        if ( $presentation->getOwner()->getId() === $userID )
        {
            $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides", 'activeSlide', $slideIndex);
        }
    }

    /**
     * Store record time in redis and update presentation entity.
     *
     * @param $userID
     * @param $presentation
     * @param $recordTime
     */
    public function setRecordTime($userID, $presentation, $recordTime)
    {
        if ( $presentation->getOwner()->getId() === $userID )
        {
            $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId(), 'recordTime', $recordTime);
            $this->updateDate($presentation);
            $presentation->setDuration($recordTime);
            $this->em->persist($presentation);
            $this->em->flush();
        }
    }

    /**
     * Store every slide content and image of a specific presentation in redis.
     *
     * @param $userID
     * @param $presentation
     * @param $slides
     */
    public function setSlides($userID, $presentation, $slides)
    {
        if ( $presentation->getOwner()->getId() === $userID )
        {
            $this->redis->MULTI();
            $this->redis->DEL("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:contents");
            $this->redis->DEL("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:images");
            foreach ( $slides as $id => $slide )
            {
                $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:contents", $id, $slide['content']);
                $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:images", $id, $slide['base64']);
            }
            $this->redis->EXEC();
            $this->updateDate($presentation);
        }
    }

    /**
     * Store an initial slide if presenter opens the presentationt the first time.
     * This is needed for the viewer. The viewer then gets a correct empty slide.
     *
     * @param $userID
     * @param $presentation
     * @param $content
     * @param $image
     */
    public function setInitialSlide($userID, $presentation, $content, $image)
    {
        if ( $presentation->getOwner()->getId() === $userID )
        {
            if ( !$this->redis->HEXISTS("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId(). ':slides:contents', 0 ) and
                 !$this->redis->HEXISTS("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId(). ':slides:images', 0 ) )
            {
                $this->redis->MULTI();
                $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:contents", 0, $content);
                $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:images", 0, $image);
                $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides", 'activeSlide', -1);
                $this->redis->EXEC();
                $this->updateDate($presentation);
            }
        }
    }

    /**
     * Store a single slide to a specific presentation.
     * This is called on every action the presentator does.
     *
     * @param $userID
     * @param $presentation
     * @param $slideIndex
     * @param $content
     * @param $image
     */
    public function setSlide($userID, $presentation, $slideIndex, $content, $image)
    {
        if ( $presentation->getOwner()->getId() === $userID )
        {
            $this->redis->MULTI();
            $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:contents", $slideIndex, $content);
            $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:images", $slideIndex, $image);
            $this->redis->EXEC();
            $this->updateDate($presentation);
        }
    }

    /**
     * Store a new slide to a specific presentation.
     * The method checks the position of the new slide and store it at the correct position in redis.
     *
     * @param $userID
     * @param $presentation
     * @param $slideIndex
     * @param $content
     * @param $image
     */
    public function addSlide($userID, $presentation, $slideIndex, $content, $image)
    {
        if ( $presentation->getOwner()->getId() === $userID )
        {
            $contents = $this->redis->HGETALL("user_" . $userID . ":presentation_" . $presentation->getId() . ":slides:contents" );
            $images = $this->redis->HGETALL("user_" . $userID . ":presentation_" . $presentation->getId() . ":slides:images" );
            ksort($contents);
            ksort($images);
            $this->redis->MULTI();
            foreach ( $contents as $key => $slideContent )
            {
                if ( $key >= $slideIndex )
                {
                    $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:contents", $key+1, $slideContent);
                    $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:images", $key+1, $images[$key]);
                }
            }
            $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:contents", $slideIndex, $content);
            $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:images", $slideIndex, $image);
            $this->redis->EXEC();
            $this->updateDate($presentation);
        }
    }

    /**
     * Store slides correctly in redis if presentator moves a slide to a new position.
     *
     * @param $userID
     * @param $presentation
     * @param $from
     * @param $to
     */
    public function moveSlide($userID, $presentation, $from, $to)
    {
        if ( $presentation->getOwner()->getId() === $userID )
        {
            $slideContent = $this->redis->HGET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:contents", $from);
            $slideImage = $this->redis->HGET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:images", $from);
            $this->removeSlide($userID, $presentation, $from);
            $this->addSlide($userID, $presentation, $to, $slideContent, $slideImage);
            $this->updateDate($presentation);
        }
    }

    /**
     * Remove a slide in a specific presentation.
     * This reorders the keys in redis.
     *
     * @param $userID
     * @param $presentation
     * @param $slideIndex
     */
    public function removeSlide($userID, $presentation, $slideIndex)
    {
        if ( $presentation->getOwner()->getId() === $userID )
        {
            $contents = $this->redis->HGETALL("user_" . $userID . ":presentation_" . $presentation->getId() . ":slides:contents" );
            $images = $this->redis->HGETALL("user_" . $userID . ":presentation_" . $presentation->getId() . ":slides:images" );
            ksort($contents);
            ksort($images);

            foreach ( $contents as $key => $slideContent )
            {
                if ( $key > $slideIndex )
                {
                    $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:contents", $key-1, $slideContent);
                    $this->redis->HSET("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:images", $key-1, $images[$key]);
                }
            }

            $this->redis->HDEL("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:contents", count($contents)-1);
            $this->redis->HDEL("user_" . $presentation->getOwner()->getId().":presentation_" . $presentation->getId() . ":slides:images", count($contents)-1);
            $this->updateDate($presentation);
        }
    }

    /**
     * @param $userID
     * @param $presentation
     * @param $image
     */
    public function setRecordImage($userID, $presentation, $image, $created)
    {
        $this->redis->RPUSH("user_" . $userID.":presentation_" . $presentation->getId(). ':recordImages', $image);
        $this->redis->RPUSH("user_" . $userID.":presentation_" . $presentation->getId(). ':createdTime', $created);
    }

    /**
     * @param $userID
     * @param $presentation
     * @param $recordTime
     * @param $audio
     */
    public function setRecordAudio($userID, $presentation, $recordTime, $audio)
    {
        $this->redis->HSET("user_" . $userID.":presentation_" . $presentation->getId(). ':recordAudios', $recordTime, $audio);
    }
}
