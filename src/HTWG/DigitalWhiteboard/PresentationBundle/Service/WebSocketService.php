<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/WebSocketService.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Service;

use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

/**
 * Implemented with RatchetPHP and publish/subscribe pattern.
 * Activationtracking is enabled with Monolog.
 *
 * Class WebSocketService
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Service
 */
class WebSocketService implements WampServerInterface {

    /**
     * @var \Symfony\Component\HttpKernel\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var PresentationService
     */
    protected $presentationService;
    /**
     * All connected users.
     *
     * @var array
     */
    protected $users;
    /**
     * All users connected to a presentation.
     *
     * @var array
     */
    protected $viewers;
    /**
     * @var array
     */
    protected $topics;

    /**
     * A lookup of all the topics clients have subscribed to
     */
    protected $subscribedTopics = array();

    /**
     * @param LoggerInterface $logger
     * @param PresentationService $presentationService
     */
    public function __construct(LoggerInterface $logger, PresentationService $presentationService) {
        $this->logger = $logger;
        $this->presentationService = $presentationService;
        $this->users = array();
        $this->viewers = array();
        $this->topics = array();
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn) {
        $status = "[WHITEBOARD] client has connection ID '".$conn->resourceId."'";
        $this->logger->info($status);
        self::log($status);
    }

    /**
     * Subscribes user to a topic (presentation).
     * Broadcasts a message to every connected to user (needed for connected user list).
     *
     * @param ConnectionInterface $conn
     * @param \Ratchet\Wamp\Topic|string $topic
     */
    public function onSubscribe(ConnectionInterface $conn, $topic) {
        if ( $topic->getId() != "anonymous - 0" )
        {
            $status = "[WHITEBOARD] client " . $conn->resourceId . " subscribes to '".$topic->getId()."'";
            $this->logger->info($status);
            self::log($status);
            $this->viewers[$conn->resourceId] = $topic;
            if ( !array_key_exists( $topic->getId(), $this->topics ) )
            {
                $this->topics[$topic->getId()] = 0;
            }
            $this->topics[$topic->getId()]++;
            $topic->broadcast(array("connID" => $conn->resourceId, "name" => "dw.presentation.subscribe"));
        }
    }

    /**
     * Onsubscribes user from presentation.
     * Broadcasts a message to every connected user (needed for connected user list).
     *
     * @param ConnectionInterface $conn
     * @param \Ratchet\Wamp\Topic|string $topic
     */
    public function onUnSubscribe(ConnectionInterface $conn, $topic) {
        $this->topics[$topic->getId()]--;
        $status = "[WHITEBOARD] client unsubscribes connection ID '".$conn->resourceId."'";
        $this->logger->info($status);
        self::log($status);
        $user = $this->users[$conn->resourceId]['userID'];
        $message = 'stateLeaving rh6Ee0tTh5 ' . date("H:i") . ": " . $this->users[$conn->resourceId]['firstName'] . " " . $this->users[$conn->resourceId]['lastName'];
        $this->presentationService->addMessage($this->users[$conn->resourceId]['presentation'], $message);
        $topic->broadcast(array("name" => "dw.presentation.update.messagelist", "message" => $message));
        $topic->broadcast(array("name" => 'dw.presentation.unsubscribe',
            "usersCount" => $this->topics[$topic->getId()],
            "userID" => $this->users[$conn->resourceId]['userID']));

        if ( $this->topics[$topic->getId()] == 0 )
        {
            unset( $this->topics[$topic->getId()] );
        }
    }

    /**
     * If connected user closes connection (browser close, etc.), then onsubscribes him correctly from presentation.
     * If closing user is presentator stop screencasting on viewer side.
     *
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn) {

        $topic = false;

        if ( array_key_exists( $conn->resourceId, $this->viewers ) )
        {
            $topic = $this->viewers[$conn->resourceId];
            $this->onUnSubscribe($conn, $topic);
            unset( $this->viewers[$conn->resourceId] );
        }

        if ( array_key_exists( $conn->resourceId, $this->users ) )
        {
            if ( $this->users[$conn->resourceId]["presenter"] )
            {
                $this->presentationService->setActiveSlide($this->users[$conn->resourceId]["userID"], $this->users[$conn->resourceId]["presentation"], -1);

                if ( $topic )
                {
                    $topic->broadcast(array("name" => 'dw.presentation.stop.screencast'));
                }
            }

            unset( $this->users[$conn->resourceId] );
        }

        $status = "[WHITEBOARD] client lost connection ID '".$conn->resourceId."'";
        $this->logger->info($status);
        self::log($status);
    }

    /**
     * On publish methods on specific topic (presentation).
     *
     * @param ConnectionInterface $conn
     * @param \Ratchet\Wamp\Topic|string $topic
     * @param string $event
     * @param array $exclude
     * @param array $eligible
     */
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude = array(), array $eligible = array())
    {
        $params = $event['args'];
        switch ($event['name'])
        {
            case 'dw.presentation.start.screencast':
                $status = "[WHITEBOARD] presentator " . $conn->resourceId ." starts screencast in presentation with ID " . $params[1];
                $this->logger->info($status);
                self::log($status);
                $this->presentationService->setActiveSlide($params[0], $this->users[$conn->resourceId]['presentation'], $params[2]);
                $slides = $this->presentationService->getSlidesForViewer($this->users[$conn->resourceId]['presentation']);
                if ( !empty($slides) )
                {
                    $content = json_encode($slides);
                    $topic->broadcast(array("name" => $event["name"], "content" => $content, "activeSlide" => $params[2]), $exclude);
                }
                break;
            case 'dw.presentation.stop.screencast':
                $status = "[WHITEBOARD] presentator " . $conn->resourceId ." stops screencast in presentation with ID " . $params[1];
                $this->logger->info($status);
                self::log($status);
                $this->presentationService->setActiveSlide($params[0], $this->users[$conn->resourceId]['presentation'], -1);
                $topic->broadcast(array("name" => $event["name"]), $exclude);
                break;
            case 'dw.presentation.set.activeslide':
                $status = "[WHITEBOARD] presentator " . $conn->resourceId ." sets active slide " . $params[2] . " in presentation with ID " . $params[1];
                $this->logger->info($status);
                self::log($status);
                $this->presentationService->setActiveSlide($params[0], $this->users[$conn->resourceId]['presentation'], $params[2]);
                $topic->broadcast(array("name" => $event["name"], "activeSlide" => $params[2]), $exclude);
                break;
            case 'dw.presentation.send.command':
                $status = "[WHITEBOARD] presentator " . $conn->resourceId ." sends command in presentation with ID " . $params[0];
                $this->logger->info($status);
                self::log($status);
                $topic->broadcast(array("name" => $event["name"], "command" => $params[1]), $exclude);
                break;
            case 'dw.presentation.update.userlist':
                $topic->broadcast(array("name" => $event["name"],
                    "userID" => $this->users[$conn->resourceId]['userID'],
                    "presenter" => $this->users[$conn->resourceId]['presenter'],
                    "firstName" => $this->users[$conn->resourceId]['firstName'],
                    "lastName" => $this->users[$conn->resourceId]['lastName'],
                    "usersCount" => $this->topics[$topic->getId()]), $exclude);
                break;
            case 'dw.presentation.send.message':
                $status = "[WHITEBOARD] user " . $conn->resourceId ." sends text message in presentation with ID " . $params[0];
                $this->logger->info($status);
                self::log($status);
                $message = 'text rh6Ee0tTh5 ' . date("H:i") . ": " . $this->users[$conn->resourceId]['firstName'] . " " . $this->users[$conn->resourceId]['lastName'] . " - " . $params[0];
                $this->presentationService->addMessage($this->users[$conn->resourceId]['presentation'], $message);
                $topic->broadcast(array("name" => "dw.presentation.update.messagelist", "message" => $message), $exclude);
                break;
            case 'dw.presentation.update.messagelist':
                $message = $this->presentationService->getLastMessage($this->users[$conn->resourceId]['presentation']);
                $topic->broadcast(array("name" => $event["name"], "message" => $message), $exclude);
                break;
        }
    }

    /**
     * OnCall methods are used as remote procedure calls without broadcasting messages.
     *
     * @param ConnectionInterface $conn
     * @param string $id
     * @param \Ratchet\Wamp\Topic|string $topic
     * @param array $params
     */
    public function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
        switch ($topic->getId())
        {
            case 'dw.presentation.register.user':
                $status = "[WHITEBOARD] client " . $conn->resourceId ." is registered as " . $params[0] . " with ID " . $params[1];
                $this->logger->info($status);
                self::log($status);
                $user = $this->presentationService->getUser($params[1]);
                $presentation = $this->presentationService->getPresentation($params[2]);
                $this->users[$conn->resourceId] = array(
                    "userID" => $params[1],
                    "firstName" => $user->getFirstname(),
                    "lastName" => $user->getLastname(),
                    "presentation" => $presentation,
                    "presenter" => false);
                $msg = 'stateJoining rh6Ee0tTh5 ' . date("H:i") . ": " . $this->users[$conn->resourceId]["firstName"] . " " . $this->users[$conn->resourceId]["lastName"];
                $this->presentationService->addMessage($presentation, $msg);
                $activeSlide = $this->presentationService->getActiveSlide($presentation);
                if ( $activeSlide > -1 )
                {
                    $content = $this->presentationService->getSlidesForViewer($presentation);
                    if ( !empty($content) )
                    {
                        $json = json_encode($content);
                        $conn->callResult($id, array("content" => $json, "activeSlide" => $activeSlide));
                    }
                }
                else
                {
                    $conn->callResult($id, array("activeSlide" => $activeSlide));
                }
                break;
            case 'dw.presentation.register.presentator':
                $status = "[WHITEBOARD] presentator " . $conn->resourceId ." is registered as " . $params[0] . " with ID " . $params[1];
                $this->logger->info($status);
                self::log($status);
                $user = $this->presentationService->getUser($params[1]);
                $presentation = $this->presentationService->getPresentation($params[2]);
                $this->users[$conn->resourceId] = array(
                    "userID" => $params[1],
                    "firstName" => $user->getFirstname(),
                    "lastName" => $user->getLastname(),
                    "presentation" => $presentation,
                    "presenter" => true);
                $this->presentationService->setInitialSlide($params[1], $presentation, $params[3], $params[4]);
                $msg = 'stateJoining rh6Ee0tTh5 ' . date("H:i") . ": " . $this->users[$conn->resourceId]["firstName"] . " " . $this->users[$conn->resourceId]["lastName"];
                $this->presentationService->addMessage($presentation, $msg);
                $content = $this->presentationService->getSlides($presentation);
                if ( !empty($content) )
                {
                    $json = json_encode($content);
                    $recordTime = $this->presentationService->getRecordTime($presentation);
                    $conn->callResult($id, array("content" => $json, "recordTime" => $recordTime));
                }
                break;
            case 'dw.presentation.reregister.presentator':
                $status = "[WHITEBOARD] presentator " . $conn->resourceId ." is registered as " . $params[0] . " with ID " . $params[1];
                $this->logger->info($status);
                self::log($status);
                $user = $this->presentationService->getUser($params[1]);
                $presentation = $this->presentationService->getPresentation($params[2]);
                $this->users[$conn->resourceId] = array(
                    "userID" => $params[1],
                    "firstName" => $user->getFirstname(),
                    "lastName" => $user->getLastname(),
                    "presentation" => $presentation,
                    "presenter" => true);
                $msg = 'stateJoining rh6Ee0tTh5 ' . date("H:i") . ": " . $this->users[$conn->resourceId]["firstName"] . " " . $this->users[$conn->resourceId]["lastName"];
                $this->presentationService->addMessage($presentation, $msg);
                $this->presentationService->setSlides($params[1], $presentation, $params[3]);
                $content = $this->presentationService->getSlides($presentation);
                if ( !empty($content) )
                {
                    $json = json_encode($content);
                    $recordTime = $this->presentationService->getRecordTime($presentation);
                    $conn->callResult($id, array("content" => $json, "recordTime" => $recordTime));
                }
                break;
            case 'dw.presentation.save.slide':
                $status = "[WHITEBOARD] presentator " . $conn->resourceId ." saves slide " . $params[2] . " in presentation with ID " . $params[1];
                $this->logger->info($status);
                self::log($status);
                $this->presentationService->setSlide($params[0], $this->users[$conn->resourceId]['presentation'], $params[2], $params[3], $params[4]);
                $conn->callResult($id, array("result" => true));
                break;
            case 'dw.presentation.add.slide':
                $status = "[WHITEBOARD] presentator " . $conn->resourceId ." adds new slide " . $params[2] . " in presentation with ID " . $params[1];
                $this->logger->info($status);
                self::log($status);
                $this->presentationService->addSlide($params[0], $this->users[$conn->resourceId]['presentation'], $params[2], $params[3], $params[4]);
                $conn->callResult($id, array("result" => true));
                break;
            case 'dw.presentation.remove.slide':
                $status = "[WHITEBOARD] presentator " . $conn->resourceId ." removes slide " . $params[2] . " in presentation with ID " . $params[1];
                $this->logger->info($status);
                self::log($status);
                $this->presentationService->removeSlide($params[0], $this->users[$conn->resourceId]['presentation'], $params[2]);
                $conn->callResult($id, array("result" => true));
                break;
            case 'dw.presentation.move.slide':
                $status = "[WHITEBOARD] presentator " . $conn->resourceId ." moves slide " . $params[2] . " in presentation with ID " . $params[1];
                $this->logger->info($status);
                self::log($status);
                $this->presentationService->moveSlide($params[0], $this->users[$conn->resourceId]['presentation'], $params[2], $params[3]);
                $conn->callResult($id, array("result" => true));
                break;
            case 'dw.presentation.get.userlist':
                $users = array();
                foreach( $this->users as $connUser )
                {
                    if ( $connUser['presentation']->getId() == $params[0] )
                    {
                        $users[] = array(
                            "userID" => $connUser['userID'],
                            "presenter" => $connUser['presenter'],
                            "firstName" => $connUser['firstName'],
                            "lastName" => $connUser['lastName']);
                    }
                }
                $conn->callResult($id, array("usersCount" => count($users), "users" => $users));
                break;
            case 'dw.presentation.get.messagelist':
                $messages = $this->presentationService->getMessages($this->users[$conn->resourceId]['presentation']);
                $conn->callResult($id, array("messages" => $messages));
                break;
            case 'dw.presentation.save.recordtime':
                $this->presentationService->setRecordTime($params[0], $this->users[$conn->resourceId]['presentation'], $params[2]);
                $conn->callResult($id, array("result" => true));
                break;
            case 'dw.presentation.record.image':
                $this->presentationService->setRecordImage($params[0], $this->users[$conn->resourceId]['presentation'], $params[2], $params[3]);
                $conn->callResult($id, array("result" => true));
                break;
        }
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e) {
        self::log("An error has occurred: {$e->getMessage()}\n");

        $conn->close();
    }

    /**
     * @param $text
     */
    public static function log($text)
    {
        echo date('H:i:s').": ".$text."\n";
    }
}