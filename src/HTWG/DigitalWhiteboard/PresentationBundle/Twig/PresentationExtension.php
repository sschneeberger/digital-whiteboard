<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/Twig/PresentationExtension.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Twig;

use Doctrine\ORM\EntityManager;

/**
 * Class PresentationExtension
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Twig
 */
class PresentationExtension extends \Twig_Extension
{
    /**
     * @var em
     */
    private $em;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }


    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('video_exists', array($this, 'videoExists')),
            new \Twig_SimpleFilter('pdf_exists', array($this, 'pdfExists')),
        );
    }

    /**
     * Get presentation from ORM
     *
     * @param $presentationID
     * @return null
     */
    private function getPresentation($presentationID)
    {
        $result = null;
        if (!$presentationID or $presentationID < 1)
        {
            return $result;
        }

        $presentation = $this->em
            ->getRepository("HTWGDigitalWhiteboardPresentationBundle:Presentation")
            ->find($presentationID);

        if (!$presentation)
        {
            return $result;
        }

        return $presentation;
    }

    /**
     * Check if generated video for presentation exists.
     *
     * @param $presentationID
     * @return bool
     */
    public function videoExists($presentationID)
    {
        $result = false;
        $presentation = $this->getPresentation($presentationID);

        if (!$presentation)
        {
            return $result;
        }

        $owner = $presentation->getOwner()->getUsername();

        if ( file_exists(__DIR__.'/../../../../../web/storage/'.$owner.'/presentation_'.$presentationID.'/video.mp4'))
        {
            $result = true;
        }

        return $result;
    }

    /**
     * Check if generated report for presentation exists.
     *
     * @param $presentationID
     * @return bool
     */
    public function pdfExists($presentationID)
    {
        $result = false;
        $presentation = $this->getPresentation($presentationID);

        if (!$presentation)
        {
            return $result;
        }

        $owner = $presentation->getOwner()->getUsername();

        if ( file_exists(__DIR__.'/../../../../../web/storage/'.$owner.'/presentation_'.$presentationID.'/report.pdf'))
        {
            $result = true;
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'presentation_extension';
    }
}