<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/DashboardController.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Controller;

use HTWG\DigitalWhiteboard\PresentationBundle\Entity\Presentation;
use HTWG\DigitalWhiteboard\PresentationBundle\Form\Type\PresentationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class DashboardController
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Controller
 */
class DashboardController extends Controller
{
    /**
     *
     */
    const LIMIT = 10;

    /**
     * @Route("/dashboard")
     */
    public function indexAction(Request $request, $page = 1)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $presentation = new Presentation();
        $form = $this->createForm(new PresentationType(), $presentation, array(
            'action' => $this->generateUrl('digital_whiteboard_dashboard'),
            'method' => 'POST')
        );
        $form->handleRequest($request);

        if ($form->isValid()) {

            $user->addPresentation($presentation);
            $presentation->setOwner($user);
            $presentation->setDate(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($presentation);
            $em->flush();
            $logger = $this->get('logger');
            $logger->info("user " . $user->getUsername() . "with ID " . $user->getId() . ' creates presentation ' . $presentation->getName() . ' with ID ' . $presentation->getId());

            return $this->redirect(
                $this->generateUrl('digital_whiteboard_connect_presentation', array('id' => $presentation->getId()))
            );
        }

        $offset = ( $page - 1 ) * self::LIMIT;
        $em = $this->getDoctrine()->getManager();

        if ( $this->get('security.context')->isGranted('ROLE_ADMIN') or $this->get('security.context')->isGranted('ROLE_PRESENTER') ) {
            $presentations = $em
                ->getRepository("HTWGDigitalWhiteboardPresentationBundle:Presentation")
                ->findPresentationsByOwner($offset, self::LIMIT, $user->getId());

            $countPresentations = $em
                ->getRepository("HTWGDigitalWhiteboardPresentationBundle:Presentation")
                ->countPresentationsByOwner($user->getId());

            $totalPages = ceil($countPresentations/self::LIMIT);

            return $this->render(
                'HTWGDigitalWhiteboardPresentationBundle:Dashboard:dashboard_presenter.html.twig',
                array(
                    'form' => $form->createView(),
                    'formFull' => $form,
                    'currentPage' => $page,
                    'totalPages' => $totalPages,
                    'presentations' => $presentations
                )
            );
        }else{

            $presentations = $em
                ->getRepository("HTWGDigitalWhiteboardPresentationBundle:Presentation")
                ->findPresentations($offset, self::LIMIT);

            $countPresentations = $em
                ->getRepository("HTWGDigitalWhiteboardPresentationBundle:Presentation")
                ->countPresentations();

            $totalPages = ceil($countPresentations/self::LIMIT);

            return $this->render(
                'HTWGDigitalWhiteboardPresentationBundle:Dashboard:dashboard_viewer.html.twig',
                array(
                    'form' => $form->createView(),
                    'formFull' => $form,
                    'presentations' => $presentations,
                    'currentPage' => $page,
                    'totalPages' => $totalPages,
                )
            );
        }
    }

    /**
     * @Route("/dashboard")
     * @Template("HTWGDigitalWhiteboardPresentationBundle:Dashboard:dashboard_presenter.html.twig")
     */
    public function deletePresentationAction(Request $request, $id = 0)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        $presentation = $em
            ->getRepository("HTWGDigitalWhiteboardPresentationBundle:Presentation")
            ->find($id);

        if (!$presentation){
            throw $this->createNotFoundException(
                'No presentation found for id '.$id
            );
        }

        if ($presentation->getOwner() != $user){
            throw new AccessDeniedException('This user is not the owner of the presentation.');
        }

        $logger = $this->get('logger');
        $logger->info("user " . $user->getUsername() . "with ID " . $user->getId() . ' deletes presentation ' . $presentation->getName() . ' with ID ' . $presentation->getId());

        $presentation->setState(Presentation::STATE_DELETED);
        $em->persist($presentation);
        $em->flush();

        return $this->redirect(
            $this->generateUrl('digital_whiteboard_dashboard')
        );
    }

    /**
     * @Route("/connect/{id}")
     * @Method("GET")
     * @Template("HTWGDigitalWhiteboardPresentationBundle:Whiteboard:whiteboard.html.twig")
     */
    public function connectPresentationAction(Request $request, $id = 0)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        $presentation = $em
            ->getRepository("HTWGDigitalWhiteboardPresentationBundle:Presentation")
            ->find($id);

        if (!$presentation){
            throw $this->createNotFoundException(
                'No presentation found for id '.$id
            );
        }

        if ($presentation->getState() !== Presentation::STATE_STARTED){
            throw $this->createNotFoundException(
                'Presentation '.$presentation->getName().' has already ended.'
            );
        }

        if ( $this->get('security.context')->isGranted('ROLE_ADMIN') or $this->get('security.context')->isGranted('ROLE_PRESENTER') ) {
            $fileExplorer = $this->get('file_explorer');
            $fileExplorer->setRepository($this->container->getParameter('file_explorer.repository'));
            $fileExplorer->setUserName($user->getUsername());
            $files = $fileExplorer->getPathList();
            $logger = $this->get('logger');
            $logger->info("user " . $user->getUsername() . " with ID " . $user->getId() . ' connects to presentation ' . $presentation->getName() . ' with ID ' . $presentation->getId());
            return $this->render(
                'HTWGDigitalWhiteboardPresentationBundle:Whiteboard:whiteboard_presenter.html.twig',
                array(
                    'presentation' => $presentation,
                    'files' => $files
                )
            );
        }
        else{
            return $this->render(
                'HTWGDigitalWhiteboardPresentationBundle:Whiteboard:whiteboard_viewer.html.twig',
                array(
                    'presentation' => $presentation
                )
            );
        }
    }

    /**
     * @Route("/close/{id}")
     * @Method("POST")
     */
    public function closePresentationAction(Request $request, $id = 0)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        $presentation = $em
            ->getRepository("HTWGDigitalWhiteboardPresentationBundle:Presentation")
            ->find($id);

        if (!$presentation){
            throw $this->createNotFoundException(
                'No presentation found for id '.$id
            );
        }

        if ($presentation->getState() !== Presentation::STATE_STARTED){
            throw $this->createNotFoundException(
                'Presentation ' . $presentation->getName() . ' has already ended.'
            );
        }

        if ($presentation->getOwner() == $user){
            $presentation->setState(Presentation::STATE_ENDED);
            $presentation->setDate(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($presentation);
            $em->flush();
            $logger = $this->get('logger');
            $logger->info("presenter " . $user->getUsername() . " with ID: " . $user->getId() . ' closes presentation ' . $presentation->getName() . ' with ID: ' . $presentation->getId());
        }
        else{
            $logger = $this->get('logger');
            $logger->info("viewer " . $user->getUsername() . " with ID: " . $user->getId() . ' closes presentation ' . $presentation->getName() . ' with ID: ' . $presentation->getId());
        }

        return $this->redirect(
            $this->generateUrl('digital_whiteboard_dashboard')
        );
    }

    /**
     * @Route("/video")
     * @Template("HTWGDigitalWhiteboardPresentationBundle:Elements:video.html.twig")
     */
    public function videoPresentationAction(Request $request, $id = 0)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        $presentation = $em
            ->getRepository("HTWGDigitalWhiteboardPresentationBundle:Presentation")
            ->find($id);

        if (!$presentation){
            throw $this->createNotFoundException(
                'No presentation found for id '.$id
            );
        }

        return $this->render(
            'HTWGDigitalWhiteboardPresentationBundle:Elements:video.html.twig',
            array(
                'presentation' => $presentation
            )
        );
    }

    /**
     * @Route("/pdf")
     */
    public function pdfPresentationAction(Request $request, $id = 0)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        $presentation = $em
            ->getRepository("HTWGDigitalWhiteboardPresentationBundle:Presentation")
            ->find($id);

        if (!$presentation){
            throw $this->createNotFoundException(
                'No presentation found for id '.$id
            );
        }

        $file = __DIR__.'/../../../../../web/storage/'.$presentation->getOwner()->getUsername().'/presentation_'.$presentation->getId().'/report.pdf';

        if ( !file_exists($file) )
        {
            throw $this->createNotFoundException(
                'No pdf found for id '.$id
            );
        }

        $response = new Response();
        $response->headers->set('Content-Description', 'File Transfer');
        $response->headers->set('Content-Type', 'application/octet-stream');
        $response->headers->set('Content-Disposition', 'attachment; filename=' . urlencode(basename($file)) .';');
        $response->headers->set('Expires', 0);
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Content-Length', filesize($file));
        $response->sendHeaders();
        $response->setContent(readfile($file));
        return $response;
    }

    /**
     * @Route("/pdf")
     */
    public function triggerExportAction(Request $request)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        if ( $this->get('security.context')->isGranted('ROLE_ADMIN') or $this->get('security.context')->isGranted('ROLE_PRESENTER') ) {

            $outputFile = __DIR__.'/../../../../../web/storage/trigger_export_out.txt';
            $PIDFile = __DIR__.'/../../../../../web/storage/trigger_export_pid.txt';
            $cmd = "php " . __DIR__.'/../../../../../app/console presentation:export --env='.$this->container->get( 'kernel' )->getEnvironment();

            $isRunning = false;

            if ( file_exists($PIDFile) )
            {
                $file = fopen($PIDFile, "r") or die("Unable to open file!");
                $pid = fread($file,filesize($PIDFile));
                fclose($file);

                try
                {
                    $result = shell_exec(sprintf("ps %d", $pid));
                    if( count(preg_split("/\n/", $result)) > 2)
                    {
                        $isRunning = true;
                    }
                }catch(Exception $e){}
            }

            $state = 0;
            if ( $isRunning )
            {
                $state = 1;
            }
            else
            {
                if ( file_exists($PIDFile) )
                {
                    unlink($PIDFile);
                }
                exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputFile, $PIDFile));
            }

            $response = new Response();
            $response->headers->set('Content-Type', 'text/plain');
            $response->sendHeaders();

            return $this->render(
                'HTWGDigitalWhiteboardPresentationBundle:Elements:trigger_export.html.twig',
                array(
                    "state" => $state
                ),
                $response
            );
        }
        else{
            throw new AccessDeniedException('This user does not have access to this section.');
        }
    }
}
