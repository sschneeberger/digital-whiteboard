<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/WhiteboardController.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Controller;

use HTWG\DigitalWhiteboard\PresentationBundle\Entity\Presentation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class WhiteboardController
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Controller
 */
class WhiteboardController extends Controller
{
    /**
     * @Route("/whiteboard")
     * @Template("HTWGDigitalWhiteboardPresentationBundle:Whiteboard:whiteboard_presenter.html.twig")
     */
    public function indexAction(Request $request)
    {
    }

    /**
     * Stores audio blobs (wave to mp3) to user storage.
     *
     * @param Request $request
     * @return Response
     * @throws \Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function recordAction(Request $request)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface)
        {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        if ( $this->get('security.context')->isGranted('ROLE_ADMIN') or $this->get('security.context')->isGranted('ROLE_PRESENTER') )
        {
            if ( isset( $_FILES['audioBlob'] ) )
            {
                $audioBlob = $_FILES['audioBlob'];
                $recordTime = $request->get('recordTime');
                $presentationID = $request->get('presentationID');
                $em = $this->getDoctrine()->getManager();
                $presentation = $em
                    ->getRepository("HTWGDigitalWhiteboardPresentationBundle:Presentation")
                    ->find($presentationID);

                if (!$presentation){
                    throw $this->createNotFoundException(
                        'No presentation found for id '.$presentationID
                    );
                }

                if ( $recordTime < 0 )
                {
                    $recordTime = 0;
                }

                $dir = $this->getUploadDirAudioChunks($presentation);
                $mp3 = $dir. '/'. $recordTime . ".mp3";
                exec( "ffmpeg -i ". $audioBlob['tmp_name'] . " -ab 96k -y " . $mp3 . " > /dev/null 2>&1");
            }
        }
        else
        {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return new Response();
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function disconnectAction(Request $request){

        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        if ( $this->get('security.context')->isGranted('ROLE_ADMIN') or $this->get('security.context')->isGranted('ROLE_PRESENTER') ) {

            $id = $request->get('presentationID');
            $em = $this->getDoctrine()->getManager();
            $presentation = $em
                ->getRepository("HTWGDigitalWhiteboardPresentationBundle:Presentation")
                ->find($id);

            if (!$presentation){
                throw $this->createNotFoundException(
                    'No presentation found for id '.$id
                );
            }

            $presentation->setState(Presentation::STATE_ENDED);
            $em->persist($presentation);
            $em->flush();

        } else{
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return new Response();
    }

    /**
     * @return string
     */
    private function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../../web/storage';
    }

    /**
     * @param $presentation
     * @return string
     */
    private function getUploadDir($presentation)
    {
        $dir = $this->getUploadRootDir() . '/'. $presentation->getOwner()->getUsername() . '/presentation_' . $presentation->getId();

        if( !is_dir( $dir ) )
        {
            mkdir( $dir, 0755, true );
        }

        return $dir;
    }

    /**
     * @param $presentation
     * @return string
     */
    private function getUploadDirAudioChunks($presentation)
    {
        $dir = $this->getUploadDir($presentation) . '/tmpAudioChunks';

        if( !is_dir( $dir ) )
        {
            mkdir( $dir, 0755, true );
        }

        return $dir;
    }
}