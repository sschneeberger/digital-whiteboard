<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/SecurityController.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Controller;

use HTWG\DigitalWhiteboard\PresentationBundle\Form\Type\ProfileType;
use HTWG\DigitalWhiteboard\PresentationBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;
use HTWG\DigitalWhiteboard\PresentationBundle\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class SecurityController
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Controller
 */
class SecurityController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContextInterface::AUTHENTICATION_ERROR);
        }
        else {
            $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
            $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        }

        return $this->render(
            'HTWGDigitalWhiteboardPresentationBundle:Security:login.html.twig',
            array(
                'last_username' => $session->get(SecurityContextInterface::LAST_USERNAME),
                'error'         => $error,
                'success'         => false,
                )
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository("HTWGDigitalWhiteboardPresentationBundle:Role");

        $user = new User();
        $form = $this->createForm(new UserType($repository), $user, array(
            'action' => $this->generateUrl('digital_whiteboard_register_user'),
            'method' => 'POST')
        );
        $form->handleRequest($request);

        if ($form->isValid()) {
            $factory = $this->container->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($form->get('password')->getData(), $user->getSalt());
            $user->setPlainPassword("");
            $user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->render(
                'HTWGDigitalWhiteboardPresentationBundle:Security:register.html.twig',
                array(
                    'form' => $form->createView(),
                    'formFull' => $form,
                    'success' => 'user_created_successfully'
                )
            );
        }

        return $this->render(
            'HTWGDigitalWhiteboardPresentationBundle:Security:register.html.twig',
            array(
                'form' => $form->createView(),
                'formFull' => $form,
                'success' => false
            )
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException
     */
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $repository = $this->getDoctrine()->getRepository("HTWGDigitalWhiteboardPresentationBundle:Role");

        $form = $this->createForm(new ProfileType($repository), $user, array(
                'action' => $this->generateUrl('digital_whiteboard_edit_user'),
                'method' => 'POST')
        );
        $form->handleRequest($request);

        if ($form->isValid()) {

            $factory = $this->container->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($form->get('password')->getData(), $user->getSalt());
            $user->setPlainPassword("");
            $user->setPassword($password);

            $presentations = $em->getRepository('HTWGDigitalWhiteboardPresentationBundle:Presentation')
                ->findByOwner($user->getId());

            foreach ( $presentations as $presentation ){
                $user->addPresentation($presentation);
            }

            $em->persist($user);
            $em->flush();

            return $this->render(
                'HTWGDigitalWhiteboardPresentationBundle:Security:edit.html.twig',
                array(
                    'form' => $form->createView(),
                    'formFull' => $form,
                    'success' => 'edit_profile_success'
                )
            );
        }

        $em->refresh($user);

        return $this->render(
            'HTWGDigitalWhiteboardPresentationBundle:Security:edit.html.twig',
            array(
                'form' => $form->createView(),
                'formFull' => $form,
                'success' => false
            )
        );
    }
}
?>