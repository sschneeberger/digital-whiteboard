<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/Repository/RoleRepository.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class RoleRepository
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Repository
 */
class RoleRepository extends EntityRepository
{

}