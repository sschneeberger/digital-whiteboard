<?php
// src/HTWG/DigitalWhiteboard/PresentationBundle/Repository/PresentationRepository.php
namespace HTWG\DigitalWhiteboard\PresentationBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\ResultSetMapping;
use HTWG\DigitalWhiteboard\PresentationBundle\Entity\Presentation;

/**
 * Class PresentationRepository
 * @package HTWG\DigitalWhiteboard\PresentationBundle\Repository
 */
class PresentationRepository extends EntityRepository
{

    /**
     * counts all finished presentations.
     *
     * @return integer
     */
    public function countPresentations(){
        $q = $this
            ->createQueryBuilder('p')
            ->select('count(p)')
            ->getQuery();

        $result = $q->getResult();

        return $result[0][1];
    }

    /**
     * Finds all finished presentations.
     *
     * @return array
     */
    public function findPresentations($offset = 0, $total = 10000){
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('HTWGDigitalWhiteboardPresentationBundle:Presentation', 'p');
        $rsm->addFieldResult('p', 'id', 'id');
        $rsm->addFieldResult('p', 'name', 'name');
        $rsm->addFieldResult('p', 'duration', 'duration');
        $rsm->addFieldResult('p', 'date', 'date');
        $rsm->addFieldResult('p', 'state', 'state');
        $rsm->addJoinedEntityResult('\HTWG\DigitalWhiteboard\PresentationBundle\Entity\User','u','p','owner');
        $rsm->addFieldResult('u', 'userId', 'id');
        $rsm->addFieldResult('u', 'user_name', 'username');
        $rsm->addFieldResult('u', 'first_name', 'firstname');
        $rsm->addFieldResult('u', 'last_name', 'lastname');

        // build rsm here
        $sql = "SELECT p.id, p.name, p.state, p.duration, p.date, u.id AS userId, u.user_name, u.first_name, u.last_name FROM presentation AS p INNER JOIN "
            ."(SELECT p.state, MAX(p.date) AS max_p_date "
            ."FROM presentation AS p "
            ."WHERE p.state <> ".Presentation::STATE_DELETED." "
            ."GROUP BY p.state) max_dates "
            ."ON p.state = max_dates.state "
            ."INNER JOIN users AS u ON p.id_owner = u.id "
            ."ORDER BY p.state ASC, max_dates.max_p_date DESC, p.date DESC "
            ."LIMIT " . $total
            ." OFFSET " . $offset;
        $q = $this->_em->createNativeQuery($sql, $rsm);

        $presentations = $q->getResult();

        return $presentations;
    }

    /**
     * counts all presentations by owner.
     *
     * @return integer
     */
    public function countPresentationsByOwner($ownerID){
        $q = $this
            ->createQueryBuilder('p')
            ->select('count(p)')
            ->andWhere('p.owner = :owner')
            ->setParameter('owner', $ownerID)
            ->getQuery();

        $result = $q->getResult();

        return $result[0][1];
    }

    /**
     * Finds all presentations by owner.
     *
     * @return array
     */
    public function findPresentationsByOwner($offset = 0, $total = 10000, $ownerID){
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('HTWGDigitalWhiteboardPresentationBundle:Presentation', 'p');
        $rsm->addFieldResult('p', 'id', 'id');
        $rsm->addFieldResult('p', 'name', 'name');
        $rsm->addFieldResult('p', 'duration', 'duration');
        $rsm->addFieldResult('p', 'date', 'date');
        $rsm->addFieldResult('p', 'state', 'state');
        $rsm->addJoinedEntityResult('\HTWG\DigitalWhiteboard\PresentationBundle\Entity\User','u','p','owner');
        $rsm->addFieldResult('u', 'userId', 'id');
        $rsm->addFieldResult('u', 'user_name', 'username');
        $rsm->addFieldResult('u', 'first_name', 'firstname');
        $rsm->addFieldResult('u', 'last_name', 'lastname');

        // build rsm here
        $sql = "SELECT p.id, p.name, p.state, p.duration, p.date, u.id AS userId, u.user_name, u.first_name, u.last_name FROM presentation AS p INNER JOIN "
                  ."(SELECT p.state, MAX(p.date) AS max_p_date "
                     ."FROM presentation AS p "
                    ."GROUP BY p.state) max_dates "
                  ."ON p.state = max_dates.state "
            ."INNER JOIN users AS u ON p.id_owner = u.id "
            ."WHERE p.id_owner = '".$ownerID."' "
            ."AND p.state != ".Presentation::STATE_DELETED." "
            ."ORDER BY p.state ASC, max_dates.max_p_date DESC, p.date DESC "
            ."LIMIT " . $total
            ." OFFSET " . $offset;
        $q = $this->_em->createNativeQuery($sql, $rsm);

        $presentations = $q->getResult();

        return $presentations;
    }
}