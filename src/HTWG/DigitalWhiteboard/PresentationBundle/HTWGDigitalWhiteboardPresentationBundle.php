<?php

namespace HTWG\DigitalWhiteboard\PresentationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class HTWGDigitalWhiteboardPresentationBundle
 * @package HTWG\DigitalWhiteboard\PresentationBundle
 */
class HTWGDigitalWhiteboardPresentationBundle extends Bundle
{
    /**
     * @return string
     */
    public function getParent()
    {
        return 'HTWGDigitalWhiteboardImageRepositoryBundle';
    }
}
