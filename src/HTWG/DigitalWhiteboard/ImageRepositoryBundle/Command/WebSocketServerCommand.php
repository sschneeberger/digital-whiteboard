<?php
// src/HTWG/DigitalWhiteboard/ImageRepositoryBundle/Command/WebSocketServerCommand.php
namespace HTWG\DigitalWhiteboard\ImageRepositoryBundle\Command;

use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\Wamp\WampServer;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\Factory;
use React\Socket\Server;
use React\ZMQ\Context;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class WebSocketServerCommand
 * @package HTWG\DigitalWhiteboard\ImageRepositoryBundle\Command
 */
class WebSocketServerCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('file_explorer:server')
            ->setDescription('Start the file explorer server');
    }

    /**
     * Starts WebSocket for image repository. Implemented with RatchetPHP.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        echo "file_server websocket is listening...";

        $webSocket = $this->getContainer()->get('websocket_server');
        $webSocketPort = $this->getContainer()->getParameter('file_explorer.websocket_port');
        $webSocketAddress = $this->getContainer()->getParameter('file_explorer.websocket_address');
        $TCPPort = $this->getContainer()->getParameter('file_explorer.tcp_port');

        $loop   = Factory::create();

        // Listen for the web server to make a ZeroMQ push after an ajax request
        $context = new Context($loop);
        $pull = $context->getSocket(\ZMQ::SOCKET_PULL);
        $pull->bind('tcp://127.0.0.1:'.$TCPPort); // Binding to 127.0.0.1 means the only client that can connect is itself
        $pull->on('message', array($webSocket, "onMessageFromFileExplorerService"));

        // Set up our WebSocket server for clients wanting real-time updates
        $webSock = new Server($loop);
        $webSock->listen($webSocketPort, $webSocketAddress); // Binding to 0.0.0.0 means remotes can connect
        $webServer = new IoServer(
            new HttpServer(
                new WsServer(
                        $webSocket
                )
            ),
            $webSock
        );

        $loop->run();
    }
}