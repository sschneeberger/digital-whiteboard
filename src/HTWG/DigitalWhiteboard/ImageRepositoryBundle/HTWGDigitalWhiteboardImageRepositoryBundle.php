<?php

namespace HTWG\DigitalWhiteboard\ImageRepositoryBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class HTWGDigitalWhiteboardImageRepositoryBundle
 * @package HTWG\DigitalWhiteboard\ImageRepositoryBundle
 */
class HTWGDigitalWhiteboardImageRepositoryBundle extends Bundle
{
}
