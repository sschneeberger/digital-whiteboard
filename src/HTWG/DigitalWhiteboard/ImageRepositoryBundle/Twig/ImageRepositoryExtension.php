<?php
// src/HTWG/DigitalWhiteboard/ImageRepositoryBundle/ImageRepositoryExtension.php
namespace HTWG\DigitalWhiteboard\ImageRepositoryBundle\Twig;

/**
 * Class ImageRepositoryExtension
 * @package HTWG\DigitalWhiteboard\ImageRepositoryBundle\Twig
 */
class ImageRepositoryExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('file_title', array($this, 'fileTitleFilter')),
        );
    }

    /**
     * @param $title
     * @return string
     */
    public function fileTitleFilter($title)
    {
        return substr($title, strrpos($title, '/')+1);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'image_repository_extension';
    }
}