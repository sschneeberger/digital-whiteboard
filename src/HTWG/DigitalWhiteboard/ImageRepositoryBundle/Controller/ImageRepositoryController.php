<?php
// src/HTWG/DigitalWhiteboard/ImageRepositoryBundle/Controller/ImageRepositoryController.php
namespace HTWG\DigitalWhiteboard\ImageRepositoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ImageRepositoryController
 * @package HTWG\DigitalWhiteboard\ImageRepositoryBundle\Controller
 */
class ImageRepositoryController extends Controller
{
    /**
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function indexAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $files = $this->getFiles($user);

        return $this->render(
            'HTWGDigitalWhiteboardImageRepositoryBundle:ImageRepository:index.html.twig',
            array(
                'files' => $files,
                'success' => false,
                'error' => false
            )
        );
    }

    /**
     * XHR action, forwards command to file explorer service
     *
     * @param Request $request
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function sendAction(Request $request){
        set_time_limit(360);

        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        if ( $this->get('security.context')->isGranted('ROLE_ADMIN') or $this->get('security.context')->isGranted('ROLE_PRESENTER') ) {

            $fileExplorer = $this->get('file_explorer');
            $this->setServiceParams($fileExplorer,$user);
            $fileExplorer->setCallback( $request->get('callbackValue') );
            $action = $request->get('action');
            $fileExplorer->$action();
        }else{
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return new Response();
    }

    /**
     * @param $fileExplorer
     * @param $user
     */
    private function setServiceParams($fileExplorer, $user){
        $fileExplorer->setRepository($this->container->getParameter('file_explorer.repository'));
        $fileExplorer->setWebsocketAddress($this->container->getParameter('file_explorer.websocket_address'));
        $fileExplorer->setWebsocketPort($this->container->getParameter('file_explorer.websocket_port'));
        $fileExplorer->setTCPPort($this->container->getParameter('file_explorer.tcp_port'));
        $fileExplorer->setUserName($user->getUsername());
    }

    /**
     * Get files of your image repository.
     *
     * @param $user
     * @return array
     */
    private function getFiles($user){
        $fileExplorer = $this->get('file_explorer');
        $this->setServiceParams($fileExplorer,$user);
        return $fileExplorer->getPathList();
    }
}
