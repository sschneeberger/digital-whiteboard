<?php
// src/HTWG/DigitalWhiteboard/ImageRepositoryBundle/FileExplorerService.php
namespace HTWG\DigitalWhiteboard\ImageRepositoryBundle\Service;

use Proxies\__CG__\HTWG\DigitalWhiteboard\PresentationBundle\Entity\User;

/**
 * Class FileExplorerService
 * @package HTWG\DigitalWhiteboard\ImageRepositoryBundle\Service
 */
class FileExplorerService
{
    /**
     * @var
     */
    public $username;
    /**
     * @var
     */
    public $repository;
    /**
     * @var
     */
    public $websocketAddress;
    /**
     * @var
     */
    public $websocketPort;
    /**
     * @var
     */
    public $TCPPort;

    /**
     * $callbackValue holds the callback.
     *
     * @var string
     */
    private $callbackValue = "";

    /**
     * @param $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @param $repository
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $websocketAddress
     */
    public function setWebsocketAddress($websocketAddress)
    {
        $this->websocketAddress = $websocketAddress;
    }

    /**
     * @param $websocketPort
     */
    public function setWebsocketPort($websocketPort)
    {
        $this->websocketPort = $websocketPort;
    }

    /**
     * @param $TCPPort
     */
    public function setTCPPort($TCPPort)
    {
        $this->TCPPort = $TCPPort;
    }

    /**
     * @param $callback
     */
    public function setCallback($callback)
    {
        $this->callbackValue = $callback;
    }

    /**
     * @return string
     */
    public function getAbsoluteStoragePath()
    {
        return $this->getUploadRootDir();
    }

    /**
     * @return string
     */
    public function getRelativeStoragePath()
    {
        return $this->getUploadDir();
    }

    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../../web/storage';
    }

    /**
     * @return string
     */
    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return $this->username.'/image-repository';
    }

    /**
     * Change working directory to storage of user.
     *
     * @return bool
     */
    public function changeToUserStorage()
    {
        if ( chdir( $this->getAbsoluteStoragePath() ) )
        {
            if ( !is_dir( $this->username ) )
            {
                mkdir( $this->username, 0755, true );
            }

            if ( chdir( $this->username ) )
            {
                if ( !is_dir( $this->repository ) )
                {
                    return mkdir( $this->repository, 0755, true );
                }
                else
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Get tree of images directory.
     *
     * @return array
     */
    public function getPathList()
    {
        $currentDir = getcwd();
        $result = array();

        if ( $this->changeToUserStorage() )
        {
            $result = $this->buildRecursiveFilePathList( "image_repository" );
        }
        chdir($currentDir);
        return $result;
    }

    /**
     * Create html snippet.
     *
     * @param $files
     * @return string
     */
    public function createDirListAsHTMLSnippet( $files )
    {
        $html = "";
        foreach( $files as $key => $file )
        {
            if ( is_array( $file ) )
            {
                $html .=  "<li rel='folder' data-rel='".$key."' title='".substr($key, strrpos($key, '/')+1)."'>";
                $html .= "<a href='#'>".substr($key, strrpos($key, '/')+1)."</a>";
                $html .= "<ul>";
                $html .= $this->createDirListAsHTMLSnippet( $file );
                $html .= "</ul>";
                $html .= "</li>";
            }
            else
            {
                $html .= "<li rel='file' data-rel='".$key."' title='".$file."'>";
                $html .= "<a href='#'>" . $file ."</a>";
                $html .= "</li>";
            }
        }
        return $html;
    }

    /**
     * Get html snippet
     */
    public function getHTMLSnippet()
    {
        $currentDir = getcwd();
        $result = "error";
        $userID = 0;

        if ( isset( $_POST['userID'] ) and !empty( $_POST['userID'] ) )
        {
            $userID = $_POST['userID'];
            $files = $this->getPathList();
            $result = "<ul><li id='root' rel='root' data-rel='image_repository' title='Root'><a href='#'>Root</a><ul>";
            $result .= $this->createDirListAsHTMLSnippet($files);
            $result .= "</ul></li></ul>";
        }

        $this->sendMessage( "uploadSuccessHandler", $result, $userID );
        chdir( $currentDir );
    }


    /**
     * Traverse directory and build a tree.
     *
     * @param $dir
     * @return array
     */
    private function buildRecursiveFilePathList( $dir )
    {
        $files = array();

        if ( $handle = opendir( $dir ) )
        {
            while ( false !== ( $file = readdir( $handle ) ) )
            {
                if ( $file != "." && $file != ".." )
                {
                    if ( is_dir( $dir.'/'.$file ) )
                    {
                        $dir2 = $dir.'/'.$file;
                        $files[$dir2] = self::buildRecursiveFilePathList($dir2);
                    }
                    else
                    {
                        $files[$dir.'/'.$file] = $file;
                    }
                }
            }
            closedir($handle);
        }

        return $files;
    }

    /**
     * Helper function for removing directory recursively.
     *
     * @param $dir
     * @source http://www.php.net/manual/de/function.rmdir.php#108113
     */
    public static function rrmdir ( $dir )
    {
        foreach ( glob($dir . '/*') as $file )
        {
            if ( is_dir( $file ) )
            {
                self::rrmdir( $file );
            }
            else
            {
                unlink( $file );
            }
        }
        rmdir( $dir );
    }

    /**
     * Send message to webSocket.
     *
     * @param $func - callbackFunction
     * @param $result
     * @param $userID
     */
    private function sendMessage( $func, $result, $userID )
    {
        $msg = array( "func" => $func, "value" => $result, "userID" => $userID );

        // This is our new stuff
        $context = new \ZMQContext();
        $socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'file-explorer');
        $socket->connect("tcp://127.0.0.1:".$this->TCPPort);

        $socket->send(json_encode($msg));
    }

    /**
     * Upload an image.
     */
    public function uploadImage()
    {
        $currentDir = getcwd();
        $result = false;
        $userID = 0;
        if ( $this->changeToUserStorage() )
        {
            $allowedFileTypes = array( "image/png", "image/jpg", "image/jpeg", "image/gif", "image/bmp" );

            if ( is_array( $_FILES['file'] ) and in_array( $_FILES['file']['type'], $allowedFileTypes ) and
                isset( $_POST['path'] ) and !empty( $_POST['path'] ) and
                isset( $_POST['userID'] ) and !empty( $_POST['userID'] ) and
                isset( $_POST['base_dir'] ) and !empty( $_POST['base_dir'] ) )
            {
                $userID = $_POST['userID'];
                $imagePath = './' . $_POST['base_dir'] . '/' . dirname( $_POST['path'] );

                if ( !is_dir( $imagePath ) )
                {
                    mkdir( $imagePath, 0755, true );
                }

                if ( is_dir( $imagePath ) and move_uploaded_file( $_FILES['file']['tmp_name'], $imagePath.'/'.basename($_POST['path']) ) )
                {
                    $result = true;
                }
            }
        }

        $this->sendMessage( "uploadHandler", $result, $userID );
        chdir( $currentDir );
    }

    /**
     * Removes a file.
     */
    public function removeFile()
    {
        $currentDir = getcwd();
        $userID = 0;
        $result = "error";
        if ( $this->changeToUserStorage() )
        {
            if ( isset( $_POST['file'] ) and !empty( $_POST['file'] ) and
                isset( $_POST['userID'] ) and !empty( $_POST['userID'] ) )
            {
                $userID = $_POST['userID'];
                if ( is_dir( $_POST['file'] ) )
                {
                    $this->rrmdir( $_POST['file'] );
                }
                else
                {
                    unlink( $_POST['file'] );
                }

                if ( !file_exists( $_POST['file'] ) )
                {
                    $result = $this->callbackValue;
                }
            }
        }

        $this->sendMessage( "resultHandler", $result, $userID );
        chdir( $currentDir );
    }

    /**
     * Alias method of php's rename file.
     */
    public function moveFile()
    {
        $this->renameFile();
    }

    /**
     * Override method of php's rename file.
     */
    public function renameFile()
    {
        $currentDir = getcwd();
        $result = "error";
        $userID = 0;
        if ( $this->changeToUserStorage() )
        {
            if ( isset( $_POST['file'] ) and !empty( $_POST['file'] ) and
                isset( $_POST['userID'] ) and !empty( $_POST['userID'] ) and
                isset( $_POST['oldFile'] ) and !empty( $_POST['oldFile'] ) )
            {
                $userID = $_POST['userID'];
                // avoid "../"
                if ( !strncmp( $_POST['file'], ".", strlen(".")) == 0 )
                {
                    if ( rename( $_POST['oldFile'], $_POST['file'] ) )
                    {
                        $result = $this->callbackValue;
                    }
                }
            }
        }
        $this->sendMessage( "resultHandler", $result, $userID );
        chdir( $currentDir );
    }

    /**
     * Create dir
     */
    public function createFolder()
    {
        $currentDir = getcwd();
        $result = "error";
        $userID = 0;
        if ( $this->changeToUserStorage() )
        {
            if ( isset( $_POST['file'] ) and !empty( $_POST['file'] ) and
                isset( $_POST['userID'] ) and !empty( $_POST['userID'] ) )
            {
                $userID = $_POST['userID'];
                // avoid "../"
                if ( !strncmp( $_POST['file'], ".", strlen(".")) == 0 )
                {

                    if ( !is_dir( $_POST['file'] ) )
                    {
                        mkdir( $_POST['file'], 0755, true );
                        $result = $this->callbackValue;
                    }
                }
            }
        }
        $this->sendMessage( "resultHandler", $result, $userID );
        chdir( $currentDir );
    }
}