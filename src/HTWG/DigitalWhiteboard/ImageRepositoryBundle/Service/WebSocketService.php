<?php
// src/HTWG/DigitalWhiteboard/ImageRepositoryBundle/WebSocketService.php
namespace HTWG\DigitalWhiteboard\ImageRepositoryBundle\Service;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

/**
 * Class WebSocketService
 * @package HTWG\DigitalWhiteboard\ImageRepositoryBundle\Service
 */
class WebSocketService implements MessageComponentInterface
{
    /**
     * @var \SplObjectStorage
     */
    protected $clients;

    /**
     *
     */
    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "[FILE_EXPLORER] {$conn->resourceId} connected \n";

        $result['func'] = "setUserID";
        $result['value'] = $conn->resourceId;

        $input = json_encode( $result );
        $msg = $input;

        $conn->send( $msg );
    }

    /**
     * ImageTreeHandler webSocket onMessage listener.
     *
     * @param ConnectionInterface $from
     * @param $msg
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
    }

    /**
     * @param $msg
     */
    public function onMessageFromFileExplorerService($msg){
        // Echo
        $data = json_decode( $msg, true );

        $result['func'] = $data['func'];
        $result['value'] = $data['value'];

        $userID = 0;

        switch ( $data['func'] )
        {
            case 'uploadSuccessHandler':
            case 'resultHandler':
            case 'uploadHandler':
                $userID = $data['userID'];
                break;
            default:break;
        }

        $input = json_encode( $result );
        $msg = $input;

        //TODO: build this with PUB/SUB Pattern of RatchetPHP and AutobahnJS
        //send data to the right "website" user
        if ( in_array( $data['func'], array( "uploadHandler", "uploadSuccessHandler", "resultHandler" ) ) )
        {
            foreach ( $this->clients as $client )
            {
                if ( $client->resourceId == $userID )
                {
                    $client->send($msg);
                }
            }
        }
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}