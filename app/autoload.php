<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
if (file_exists(__DIR__.'/../vendor/autoload.php')) {
    $loader = require __DIR__.'/../vendor/autoload.php';
} else {
    $loader = require "/home/vagrant/.composer_packages/autoload.php";
}

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

return $loader;
