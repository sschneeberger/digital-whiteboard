require 'shellwords'

class GruntPlugin < Vagrant.plugin("2")
  name "Grunt Plugin"

  class NoTasksError < Vagrant::Errors::VagrantError
    error_message('At least one task is required. See `vagrant grunt --help` for help.')
  end

  class CommunicationError < Vagrant::Errors::VagrantError
    error_message('Failed to communicate with VM')
  end

  class Config < Vagrant.plugin(2, :config)
    attr_accessor :grunt_bin, :cwd

    DEFAULT_GRUNT_BIN = "grunt"
    DEFAULT_CWD = "/vagrant"

    def initialize
      @grunt_bin = DEFAULT_GRUNT_BIN
      @cwd = DEFAULT_CWD
    end

    def finalize!
      @grunt_bin = DEFAULT_GRUNT_BIN if @grunt_bin == UNSET_VALUE
      @cwd = DEFAULT_GRUNT_BIN if @cwd == UNSET_VALUE
    end
  end

  class Command < Vagrant.plugin(2, :command)
    def execute
      options = {
        tasks: []
      }
      opts = OptionParser.new do |o|
        o.banner = "Usage: vagrant grunt [vm-name...] [options] \nIf no VM names are given, it runs on all VMs"
        o.separator "\n"

        o.on(:REQUIRED, "-t", "--task", "=GRUNT_TASK", "Grunt task to run. Can be given multiple times. Must be given at least once.") do |task|
          options[:tasks].push task
        end
      end

      vms = parse_options(opts)

      raise NoTasksError if options[:tasks].empty?


      with_target_vms(vms) do |machine|
        communicator = machine.communicate
        raise CommunicationError unless communicator.wait_for_ready(120)

        config = machine.config.grunt
        command = "cd #{config.cwd.shellescape}; " + ([config.grunt_bin] + options[:tasks]).map(&:shellescape).join(" ")

        output = {
          stderr: "",
          stdout: ""
        }
        communicator.execute(command) do |type, data|
          case type
            when :stdout then machine.ui.info(data)
            else machine.ui.warn(data)
          end
        end
      end

      @env.ui.success("Grunt tasks #{options[:tasks].inspect} finished at #{Time.now.to_s}")

      0
    end
  end

  config :grunt do
    Config
  end

  command :grunt do
    Command
  end
end