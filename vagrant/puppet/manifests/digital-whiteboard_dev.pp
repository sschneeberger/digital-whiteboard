### Basic setup
$additional_packages = hiera('additional_packages')
package { $additional_packages:
  ensure => installed,
}

class { 'locales':
  default_value => "en_US.UTF-8",
  available => [
    "en_US.UTF-8 UTF-8",
    "de_CH.UTF-8 UTF-8",
  ]
}

stage{ 'prepare':
    before => Stage['main'],
}

class preparations{
 # parameters for apt class is in hiera!
  include apt, apt::backports

# Ensure all APT index is current an all packages are up to date
  exec { "apt_upgrade":
    command => "${apt::params::provider} upgrade -y",
    # This can take a while…
    timeout => 1800,
    require => [
      Exec["apt_update"],
      Apt::Source['backports'],
    ]
  }

# Ensure augeas works
  package { "libaugeas-ruby":
    ensure => installed,
    require => Exec["apt_upgrade"],
  }
}

class{ 'preparations':
    stage => 'prepare',
}

# Sane terminal title
file { "/etc/profile.d/01_terminal-title.sh":
  ensure => file,
  content => "# set terminal title with hostname and 'v:' prefix\ntty -s && echo -ne \"\\033]0;v:\${HOSTNAME}\\007\"",
}

# Ensure vagrant user has umask of 0000 so we don't have to
# muck around with permissions
file { "/etc/profile.d/01_vagrant-umask.sh":
  ensure => file,
  content => 'if [ "$USER" == "vagrant" ]; then umask 0000; fi';
}

# Set timezone
package { "tzdata":
  ensure => installed
}
file { "/etc/localtime":
  require => Package["tzdata"],
  ensure => link,
  target => "/usr/share/zoneinfo/Europe/Zurich",
}
file { "/etc/timezone":
  require => Package["tzdata"],
  content => "Europe/Zurich",
}

### /Basic setup

### PHP stuff
package { ["php5", "php5-cli"]:
  ensure => installed,
}

# Symlink app specific PHP config
file {"/etc/php5/conf.d/90-xdebug.ini":
  ensure => link,
  target => "/vagrant/vagrant/config/php/xdebug.ini",
  require => Package["php5"],
  notify => Service['apache2']
}

file {"/etc/php5/conf.d/91-whiteboard.ini":
  ensure => link,
  target => "/vagrant/vagrant/config/php/whiteboard.ini",
  require => Package["php5"],
  notify => Service['apache2']
}

$php_values = hiera('php')
# Install all packages defined in hiera under php.packages
php_extension { $php_values['extensions']:
  ensure => installed,
}

define php_extension ($ensure) {
  package { "php5-${title}":
    ensure => $ensure,
  }
}
### /PHP stuff

### Pear packages
define pear-config ($value) {

  exec { "pear-config-set-${name}":
    command => "/usr/bin/pear config-set ${name} ${value}",
    unless => "/usr/bin/pear config-get ${name} | grep ${value}",
    require => Package['php-pear'],
  }

}

include pearpackages

class pearpackages {

	pear-config { auto_discover:
        value => 1
    }

    file {
        "/var/tmp/zeromq.deb":
        source => "puppet:///modules/zeromq/zeromq_4.0.4_amd64.deb",
        ensure => file
    }

    package { "puppet-zeromq":
        provider => dpkg,
        ensure   => present,
        source   => "/var/tmp/zeromq.deb",
        require => File['/var/tmp/zeromq.deb']
    }

    exec { "pecl-zmq":
        command => "printf \"\\n\" | pecl -d preferred_state=beta install zmq-beta && pecl info zmq-beta",
        path => '/usr/bin:/usr/sbin:/bin:/sbin',
        unless => "pecl install zmq-beta | egrep fail",
        require => [Package['puppet-zeromq'], Exec['pear-config-set-auto_discover']]
    }

    file {'/etc/php5/conf.d/zmq.ini':
        ensure => present,
        owner => root,
        group => root,
        mode => 444,
        path => '/etc/php5/conf.d/zmq.ini',
        content => 'extension=zmq.so',
        require => Exec['pecl-zmq'],
        notify => Service['apache2']
    }

}
### /Pear packages

### MySQL stuff
$mysql_values = hiera('mysql')
class { 'mysql::server':
  root_password => $mysql_values['root_password'],
  override_options => { 'mysqld' => {
    'bind-address' => '0.0.0.0'
  } },
  users => { "root@%" => {
    ensure => present,
    password_hash => mysql_password($mysql_values['root_password']),
  } },
  databases => { "digital_whiteboard" => {
    ensure => present,
    charset => 'utf8',
  } },
  grants => { "root@%/*.*" => {
    ensure => 'present',
    options => ['GRANT'],
    privileges => ['ALL'],
    table => '*.*',
    user => 'root@%',
  } },
}
class { 'mysql::client': }
### /MySQL stuff

### Redis stuff
package { ["redis-server/wheezy-backports", "redis-tools/wheezy-backports"]:
    ensure => installed,
}
### /Redis stuff


### Apache stuff
class {'apache':
  default_vhost => false,
  mpm_module => 'prefork',
}
$apache_values = hiera('apache')
class {'apache::mod::php': }

# we define the document root as file resource so
# apache::vhost won't attempt to chown it. There is
# no point in doing that since it's inside the shared
# folder. Plus, if shared folder is shared via NFS,
# apache::vhost even fails
file {'/vagrant/web':
  ensure => directory
}

apache::vhost {'digital-whiteboard_http':
  docroot => '/vagrant/web',
  ip => '*',
  port => '80',
  servername => $apache_values['servername'],
  serveraliases => $apache_values['serveraliases'],
  override => 'All',
  custom_fragment => "EnableSendFile off\nSetEnv SYMFONY__DEV__USETMPDIRS true"
}
### /Apache stuff

### Node.js stuff
package {'nodejs':
  ensure => installed
}

# Make nodejs executable available as `node`
file {'/usr/local/bin/node':
  ensure => link,
  target => '/usr/bin/nodejs',
}
# Put locally installed NPM packages' bin into $PATH

# Windows doesn't support symlinks so this does not work
# on windows host machines
file {'/etc/profile.d/vagrant-npm-packages.sh':
  ensure => absent,
  content => 'PATH="/vagrant/node_modules/.bin:$PATH"',
}

file { "/home/vagrant/bin":
  ensure => directory,
  owner => "vagrant",
  group => "vagrant",
}

### /Node.js stuff

### Memcached
package { "memcached":
  ensure => installed,
}
### /Memcached


### Symfony stuff
$composer_home = "/home/vagrant/.composer"
# Update /etc/environment to set SYMFONY__DEV__USETMPDIRS to true
# to signal the Symfony app to write its cache and log files
# to /tmp and not hitting the shared folder too much
augeas { 'etc-environment-symfony-env':
    context => '/files/etc/environment',
    changes => [
        'set SYMFONY__DEV__USETMPDIRS true',
        "set COMPOSER_HOME ${composer_home}",
    ],
    notify => Service['httpd'],
}

# Symlink parameters.yml to the predefined ones for vagrant
file { "parameters-yml":
  name => "/vagrant/app/config/parameters.yml",
  ensure => file,
  replace => false,
  source => "/vagrant/vagrant/config/symfony/parameters.yml",
}

package { "git":
  ensure => installed,
}

file { $composer_home:
  ensure => directory,
  owner => "vagrant",
  group => "vagrant",
}

file { "composer-config":
  ensure => file,
  name => "${composer_home}/config.json",
  source => "/vagrant/vagrant/config/composer/config.json",
  owner => "vagrant",
  group => "vagrant",
}

exec { "composer-install":
  require => [
    File["composer-config"],
    File["parameters-yml"],
    Package['php5-cli'],
    Package['git'],
# Needs correct locales setup, otherwise checking for ICU will fail
    Class['locales'],
# Everything after this needs MySQL, so we might as well
    Class['mysql::server'],
  ],
  environment => "COMPOSER_HOME=${composer_home}",
  command => "/vagrant/composer.phar install --no-interaction",
  cwd => "/vagrant",
  # logoutput => true,
  # This can take a while…
  timeout => 1800,
}

#exec_symfony_command {'doctrine:schema:update':
#  args => "--force",
#}

#exec_symfony_command {'doctrine:fixtures:load':
#  args => "--no-interaction",
#  require => Exec_symfony_command['doctrine:schema:update'],
#}

define exec_symfony_command ($args = '') {
  exec { "app-console-${title}":
    command => "/vagrant/app/console ${title} ${args}",
    cwd => "/vagrant",
    require => [
      Exec["composer-install"],
      File["parameters-yml"],
      Package['php5-cli'],
    ],
  }
}
### /Symfony stuff

### Fix permissions
stage {'finalize': }
Stage['main'] -> Stage['finalize']

class finalization {
  file { "/tmp/digital-whiteboard":
    ensure => directory,
    mode => "a+rwX",
    owner => "vagrant",
    group => "vagrant",
  }

  exec_fix_permissions {'/tmp/digital-whiteboard':
    require => File["/tmp/digital-whiteboard"],
    mode => "a+rwX",
    owner => "vagrant",
    group => "vagrant",
  }

  exec_fix_permissions {"/home/vagrant/.composer_packages":
    mode => "a+rwX",
    owner => "vagrant",
    group => "vagrant",
  }


  exec_fix_permissions {"/home/vagrant/.composer/cache":
    mode => "a+rwX",
    owner => "vagrant",
    group => "vagrant",
  }

# Puppet's file resource with `recursive => true` log changes to
# every file and spams the logs. We do it ourselves here.
  define exec_fix_permissions($owner, $group, $mode) {
    exec { "chown-${title}":
      command => "/bin/chown -R ${owner}:${group} ${title}",
    }
    exec { "chmod-${title}":
      command => "/bin/chmod -R ${mode} ${title}",
    }
  }
}
class {'finalization':
  stage => finalize,
}
### /Fix permissions