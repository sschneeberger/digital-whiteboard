###
This script contains the whiteboard class

@author sischnee <sischnee@gmail.com>
@since 2014/08/04
###
window.Whiteboard = ->

  ###
  @var
  @access private
  @type object
  ###
  that = this

  ###
  Init method sets up the whiteboard

  @access public
  @return void
  ###
  @init = ->
    if $( "#whiteboard" ).length

      this.resizeCanvas()

      $( window ).resize( () ->
        that.resizeCanvas()
      )

      $("#tool-pen-color-field").css('color', '#000000')

      $("#tool-pen-color-value").change( () ->
        $("#tool-pen-color-field").css('color', '#'+$(this).val())
      )

      $("#tool-line-color-field").css('color', '#000000')

      $("#tool-line-color-value").change( () ->
        $("#tool-line-color-field").css('color', '#'+$(this).val())
      )

      $("#tool-forms-border-color-field").css('color', '#000000')

      $("#tool-forms-border-color-value").change( () ->
        $("#tool-forms-border-color-field").css('color', '#'+$(this).val())
      )

      $("#tool-forms-fill-color-field").css('color', '#FFFFFF')

      $("#tool-forms-fill-color-value").change( () ->
        $("#tool-forms-fill-color-field").css('color', '#'+$(this).val())
      )

      $("#tool-text-color-field").css('color', '#000000')

      $("#tool-text-color-value").change( () ->
        $("#tool-text-color-field").css('color', '#'+$(this).val())
      )

      $(document).unbind("keydown").bind "keydown", (event) ->
        doPrevent = false
        if event.keyCode is 8
          d = event.srcElement or event.target
          if (d.tagName.toUpperCase() is "INPUT" and (d.type.toUpperCase() is "TEXT" or d.type.toUpperCase() is "PASSWORD" or d.type.toUpperCase() is "FILE" or d.type.toUpperCase() is "EMAIL" or d.type.toUpperCase() is "SEARCH" or d.type.toUpperCase() is "DATE")) or d.tagName.toUpperCase() is "TEXTAREA" or d.tagName.toUpperCase() is "CANVAS"
            doPrevent = d.readOnly or d.disabled
          else
            doPrevent = true
        event.preventDefault()  if doPrevent
        return


  @resizeCanvas = ->
    canvasWidth = parseInt($('#whiteboard').css('width'), 10)
    canvasHeight = parseInt(canvasWidth * 10/16, 10)
    $('#whiteboard').css('height', canvasHeight + "px")
    $('#sidebar').css('height', canvasHeight + "px")
    $('#communication').css('height', canvasHeight + "px")

  @init()
  return