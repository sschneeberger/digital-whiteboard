'use strict'

angular.module('digitalWhiteboardApp.command_services', []).factory("CommandService", ['WhiteboardService', 'SlideService', 'SlidesService', 'SocketService', (WhiteboardService, SlideService, SlidesService, SocketService) ->
  return {
    notifyObjectSelection: (scope) ->
      selectedObject = scope.canvas.getActiveObject()
      selectedGroup = scope.canvas.getActiveGroup()
      if selectedObject?
        selected = selectedObject
      if selectedGroup?
        selected = selectedGroup
      if selected? and scope.canvas.options.isEditingMode
        scope.canvas.trigger("object:selected", {target: selected})

    createCommand: (scope, object, commandType, properties) ->
      command =
        command: commandType
        type: object.type
        properties: properties
      @resetStateStackBecauseOfNewActionIfNecessary(scope)
      if scope.commands.length is scope.stackIndexLimit
        scope.commands.shift()
        scope.commands[scope.stackIndex-1] = JSON.stringify(command)
      else
        scope.commands[scope.stackIndex] = JSON.stringify(command)
      if scope.stackIndex < scope.stackIndexLimit
        scope.stackIndex++

      if scope.sess? and scope.tools.screencast
        if command.properties.origin?
          delete command.properties.origin
        if command.properties.groupObjectProperties?
          for props in command.properties.groupObjectProperties
            delete props.origin
        if command.command is "group-remove"
          delete command.properties.groupObjectProperties
        if command.command is "remove-slide"
          delete command.properties.content
        SocketService.sendCommand(scope, JSON.stringify(command))

      if object.type isnt "slide"
        SlideService.saveSlide(scope, scope.slides.slideIndex)
      console.log(scope.commands)
      scope.$apply() if not scope.$$phase

    doUndo: (scope, command) ->
      command = JSON.parse(command)
      switch command.command
        when "add-slide"
          if scope.sess? and scope.tools.screencast
            socketCommand = $.extend(true,{},command)
            socketCommand.command = "remove-slide"
            delete socketCommand.properties.content
            SocketService.sendCommand(scope, JSON.stringify(socketCommand))
          SlideService.removeSlide(scope, command.properties.slideIndex)
        when "remove-slide"
          if scope.sess? and scope.tools.screencast
            socketCommand = $.extend(true,{},command)
            socketCommand.command = "re-add-slide"
            SocketService.sendCommand(scope, JSON.stringify(socketCommand))
          SlideService.reAddSlide(scope, command.properties.slideIndex, command.properties.content)
        when "move-slide"
          if scope.sess? and scope.tools.screencast
            socketCommand = $.extend(true,{},command)
            tmpTo = socketCommand.properties.to
            socketCommand.properties.to = socketCommand.properties.from
            socketCommand.properties.from = tmpTo
            SocketService.sendCommand(scope, JSON.stringify(socketCommand))
          SlideService.moveSlide(scope, command.properties.to, command.properties.from)
          scope.slides.slideIndex = command.properties.from
          SlidesService.showSlide(scope, command.properties.from)
        else
          if scope.slides.slideIndex is command.properties.slideIndex
            @undoTools(scope, command)
            SlideService.saveSlide(scope, command.properties.slideIndex)
          else
            scope.slides.slideIndex = command.properties.slideIndex
            SlidesService.setBackAndNext(scope)
            scope.canvas.clear()
            that = this
            scope.canvas.loadFromJSON(JSON.parse(scope.slides.content[command.properties.slideIndex].json), () ->
              SlidesService.renderSlide(scope)
              that.undoTools(scope,command)
              SlideService.saveSlide(scope, scope.slides.slideIndex)
              scope.$apply()
            )
      return

    doRedo: (scope, command) ->
      command = JSON.parse(command)
      switch command.command
        when "add-slide"
          if scope.sess? and scope.tools.screencast
            SocketService.sendCommand(scope, JSON.stringify(command))
          SlideService.addSlide(scope, command.properties.slideIndex)
        when "remove-slide"
          if scope.sess? and scope.tools.screencast
            SocketService.sendCommand(scope, JSON.stringify(command))
          SlideService.removeSlide(scope, command.properties.slideIndex)
        when "move-slide"
          if scope.sess? and scope.tools.screencast
            SocketService.sendCommand(scope, JSON.stringify(command))
          SlideService.moveSlide(scope, command.properties.from, command.properties.to)
          scope.slides.slideIndex = command.properties.to
          SlidesService.showSlide(scope, command.properties.to)
        else
          if scope.slides.slideIndex is command.properties.slideIndex
            @redoTools(scope, command)
            SlideService.saveSlide(scope, command.properties.slideIndex)
          else
            scope.slides.slideIndex = command.properties.slideIndex
            SlidesService.setBackAndNext(scope)
            scope.canvas.clear()
            that = this
            scope.canvas.loadFromJSON(JSON.parse(scope.slides.content[command.properties.slideIndex].json), () ->
              SlidesService.renderSlide(scope)
              that.redoTools(scope,command)
              SlideService.saveSlide(scope, scope.slides.slideIndex)
              scope.$apply()
            )
      return

    undoTools: (scope, command) ->
      scope.commandAction = false
      properties = command.properties

      if properties.uid?
        object = WhiteboardService.getObjectFromUID(scope, properties.uid)

      switch command.command
        when "add"
          scope.canvas.discardActiveGroup() if scope.canvas.getActiveGroup()?.getObjects().indexOf(object) > -1
          WhiteboardService.deleteObject(scope, object) if not _.isEmpty(object)
          if scope.sess? and scope.tools.screencast
            socketCommand = $.extend(true,{},command)
            socketCommand.command = "remove"
            SocketService.sendCommand(scope, JSON.stringify(socketCommand))
        when "modify"
          if not _.isEmpty(object)
            scope.canvas.discardActiveGroup() if scope.canvas.getActiveGroup()?.getObjects().indexOf(object) > -1
            if object.type is "circle"
              object.setRadius(properties.origin.width/2)
            if object.type is "ellipse"
              object.set("rx", properties.origin.width/2)
              object.set("ry", properties.origin.height/2)
            object.set(properties.origin)
            object.setCoords()
          if scope.sess? and scope.tools.screencast
            socketCommand = $.extend(true,{},command)
            tmpOrigin = $.extend(true,{},socketCommand.properties.origin)
            delete socketCommand.properties.origin
            socketCommand.properties.new = tmpOrigin
            SocketService.sendCommand(scope, JSON.stringify(socketCommand))
        when "group-modify"
          @groupModify(scope, properties.groupObjectUIDs, properties, false, false)
          if scope.sess? and scope.tools.screencast
            socketCommand = $.extend(true,{},command)
            tmpOrigin = $.extend(true,{},socketCommand.properties.origin)
            delete socketCommand.properties.origin
            socketCommand.properties.new = tmpOrigin
            SocketService.sendCommand(scope, JSON.stringify(socketCommand))
        when "group-wb-modify"
          @groupModify(scope, properties.groupObjectUIDs, properties, false, true)
          if scope.sess? and scope.tools.screencast
            socketCommand = $.extend(true,{},command)
            tmpOrigin = $.extend(true,{},socketCommand.properties.origin)
            delete socketCommand.properties.origin
            socketCommand.properties.new = tmpOrigin
            if socketCommand.properties.groupObjectProperties?
              for props in socketCommand.properties.groupObjectProperties
                tmpOriginOrigin = $.extend(true,{},props.origin)
                delete props.origin
                props.new = tmpOriginOrigin
            SocketService.sendCommand(scope, JSON.stringify(socketCommand))
        when "group-remove"
          if scope.sess? and scope.tools.screencast
            socketCommand = $.extend(true,{},command)
            socketCommand.command = "group-add"
            SocketService.sendCommand(scope, JSON.stringify(socketCommand))
          @groupAdd(scope, command)
        when "layer-down"
          if not _.isEmpty(object)
            if scope.sess? and scope.tools.screencast
              socketCommand = $.extend(true,{},command)
              socketCommand.command = "layer-up"
              SocketService.sendCommand(scope, JSON.stringify(socketCommand))
            scope.canvas.discardActiveGroup() if scope.canvas.getActiveGroup()?.getObjects().indexOf(object) > -1
            object.bringForward()
        when "layer-up"
          if not _.isEmpty(object)
            if scope.sess? and scope.tools.screencast
              socketCommand = $.extend(true,{},command)
              socketCommand.command = "layer-up"
              SocketService.sendCommand(scope, JSON.stringify(socketCommand))
            scope.canvas.discardActiveGroup() if scope.canvas.getActiveGroup()?.getObjects().indexOf(object) > -1
            object.sendBackwards()
        when "remove"
          if scope.sess? and scope.tools.screencast
            socketCommand = $.extend(true,{},command)
            socketCommand.command = "add"
            SocketService.sendCommand(scope, JSON.stringify(socketCommand))
          @add(scope, command)

      @notifyObjectSelection(scope)

    redoTools: (scope, command) ->
      scope.commandAction = false
      properties = command.properties

      if properties.uid?
        object = WhiteboardService.getObjectFromUID(scope, properties.uid)

      switch command.command
        when "add"
          @add(scope, command)
        when "modify"
          if not _.isEmpty(object)
            scope.canvas.discardActiveGroup() if scope.canvas.getActiveGroup()?.getObjects().indexOf(object) > -1
            if object.type is "circle"
              object.setRadius(properties.new.width/2)
            if object.type is "ellipse"
              object.set("rx", properties.new.width/2)
              object.set("ry", properties.new.height/2)
            object.set(properties.new)
            object.setCoords()
        when "group-modify"
          @groupModify(scope, properties.groupObjectUIDs, properties, true)
        when "group-wb-modify"
          @groupModify(scope, properties.groupObjectUIDs, properties, true, true)
        when "group-remove"
          @groupRemove(scope, properties.groupObjectUIDs)
        when "layer-down"
          if not _.isEmpty(object)
            scope.canvas.discardActiveGroup() if scope.canvas.getActiveGroup()?.getObjects().indexOf(object) > -1
            object.sendBackwards()
        when "layer-up"
          if not _.isEmpty(object)
            scope.canvas.discardActiveGroup() if scope.canvas.getActiveGroup()?.getObjects().indexOf(object) > -1
            object.bringForward()
        when "remove"
          if not _.isEmpty(object)
            WhiteboardService.deleteObject(scope, object)

      scope.canvas.renderAll()
      @notifyObjectSelection(scope)
      if scope.sess? and scope.tools.screencast
        if command.properties.origin?
          delete command.properties.origin
        if command.properties.groupObjectProperties?
          for props in command.properties.groupObjectProperties
            delete props.origin
        if command.command is "group-remove"
          delete command.properties.groupObjectProperties
        SocketService.sendCommand(scope, JSON.stringify(command))

    add: (scope, command) ->
      object = {}
      properties = command.properties
      switch command.type
        when "path"
          object = new fabric.Path(properties.path)
        when "line"
          object = new fabric.Line()
        when "circle"
          object = new fabric.Circle()
        when "ellipse"
          object = new fabric.Ellipse()
        when "rect"
          object = new fabric.Rect()
          object.setHeight(properties.height)
          object.setWidth(properties.width)
        when "triangle"
          object = new fabric.Triangle()
        when "image"
          fabric.Image.fromURL(properties.src, (oImg) ->
            oImg.set(properties)
            oImg.setCoords()
            scope.canvas.add(oImg).renderAll()
            SlideService.saveSlide(scope, scope.slides.slideIndex)
            scope.$apply()
          )
        when "i-text"
          object = new fabric.IText(properties.text)
          object.set("originStyles", properties.originStyles)
          object = WhiteboardService.createTextEventListener(scope, object)
      if not _.isEmpty(object)
        object.set(properties)
        #hack because line cannot handle coordinates...
        object.setLeft(properties.left)
        object.setTop(properties.top)
        object.setCoords()
        scope.canvas.add(object).renderAll()
        console.log(object)

    groupAdd: (scope, command) ->
      for objectToAdd in command.properties.groupObjectProperties
        @add(scope, objectToAdd)

    groupModify: (scope, groupObjectUIDs, properties, redo, modify) ->
      objectsToGroup = []
      for objectUID in groupObjectUIDs
        object = WhiteboardService.getObjectFromUID(scope, objectUID)
        objectsToGroup.push(object) if not _.isEmpty(object)

      objectsInOriginSelectedGroup = []
      if scope.canvas.getActiveGroup()?
        for activeObject in scope.canvas.getActiveGroup().getObjects()
          objectsInOriginSelectedGroup.push(activeObject.get("uid")) if groupObjectUIDs.indexOf(activeObject.get("uid")) > -1

      if objectsInOriginSelectedGroup.length is groupObjectUIDs.length or objectsInOriginSelectedGroup.length > 0
        activeGroup = scope.canvas.getActiveGroup()
        scope.canvas.discardActiveGroup()
        scope.canvas.remove(activeGroup)

      if scope.canvas.getActiveObject()? and groupObjectUIDs.indexOf(scope.canvas.getActiveObject().get("uid")) > -1
        scope.canvas.discardActiveObject()

      if modify
        for objectToModify,idx in objectsToGroup
          if redo
            objectToModify.set(properties.groupObjectProperties[idx].new)
          else
            objectToModify.set(properties.groupObjectProperties[idx].origin)
          objectToModify.setCoords()

      if redo
        top = properties.new.top
        left = properties.new.left
        angle = properties.new.angle
      else
        top = properties.origin.top
        left = properties.origin.left
        angle = -properties.new.angle

      groupTemp = new fabric.Group(objectsToGroup,{
        originX: properties.originX
        originY: properties.originY
        top: top
        left: left
        angle: angle
      })

      groupTemp.setCoords()
      groupTemp._restoreObjectsState()
      scope.canvas.remove(groupTemp)
      group = new fabric.Group(objectsToGroup,{
        originX: properties.originX
        originY: properties.originY
      })

      if objectsInOriginSelectedGroup.length is groupObjectUIDs.length
        for groupObject in group.getObjects()
          groupObject.set('active', true)
        scope.canvas._activeObject = null
        scope.canvas.setActiveGroup(group.setCoords()).renderAll()
      else
        group.setCoords()
        group._restoreObjectsState()
        scope.canvas.remove(group)

    groupRemove: (scope, groupObjectUIDs) ->
      objectsToRemove = []
      for objectUID in groupObjectUIDs
        object = WhiteboardService.getObjectFromUID(scope, objectUID)
        objectsToRemove.push(object) if not _.isEmpty(object)

      objectsInOriginSelectedGroup = []
      if scope.canvas.getActiveGroup()?
        for activeObject in scope.canvas.getActiveGroup().getObjects()
          objectsInOriginSelectedGroup.push(activeObject.get("uid")) if groupObjectUIDs.indexOf(activeObject.get("uid")) > -1

      if objectsInOriginSelectedGroup.length is groupObjectUIDs.length or objectsInOriginSelectedGroup.length > 0
        activeGroup = scope.canvas.getActiveGroup()
        scope.canvas.discardActiveGroup()
        scope.canvas.remove(activeGroup)

      if scope.canvas.getActiveObject()? and groupObjectUIDs.indexOf(scope.canvas.getActiveObject().get("uid")) > -1
        scope.canvas.discardActiveObject()

      for objectToRemove in objectsToRemove
        WhiteboardService.deleteObject(scope, objectToRemove)

    resetStateStackBecauseOfNewActionIfNecessary: (scope) ->
      if scope.undoMode
        scope.undoMode = false
        scope.commands.splice(scope.stackIndex)
  }
])