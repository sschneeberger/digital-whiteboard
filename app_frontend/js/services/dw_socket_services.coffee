'use strict'

angular.module('digitalWhiteboardApp.socket_services', []).factory("SocketService", ["SlidesService", "ViewerService", "StatusService", "$interval", (SlidesService, ViewerService, StatusService, $interval) ->

  originScreenshot = ""

  return {
    establishConnection: (scope) ->
      that = this
      wsuri = window.AppConfig.webSocketUrl + ":" + window.AppConfig.webSocketPresentationPort
      scope.sess = null

      conn = new ab.connect(
        wsuri,

        (session) ->
          console.log("presentation:connected")
          scope.tools.screencastAvailable = true
          scope.tools.recordAvailable = true
          StatusService.enableStatus(scope, "connected", true)
          scope.sess = session
          scope.connID = 0
          scope.sess.subscribe(scope.topic, (topic, data) ->
            if scope.connID is 0
              scope.connID = data.connID
              console.log('presentation:subscribed to "' + topic + '"')
              if window.AppGlobals.isPresentator
                scope.connectionEstablished++
                if scope.connectionEstablished is 1
                  that.registerPresenter(scope)
                else
                  that.reRegisterPresenter(scope)
              else
                that.registerUser(scope)
              scope.$apply()
            else
              that.onEvent(data)
          )
          scope.$apply()
        ,

        (code, reason, detail) ->
          scope.connID = 0
          StatusService.enableStatus(scope, "noConnection", false)
          scope.tools.screencastAvailable = false
          scope.tools.screencast = false
          scope.tools.record = true
          scope.tools.recordAvailable = false
          scope.tools.recordStop = false
          scope.timer = null
          scope.timerStart = undefined
          scope.sess = null
          originScreenshot = ""
          scope.$apply()
          console.warn('presentation:disconnect, ', reason)
        ,

        {
          maxRetries: 1
          retryDelay: 4000
          skipSubprotocolCheck: true
        }
      )

    close: (scope) ->
      scope.sess.close() if scope.sess?
      return

    onEvent: (data) ->
      switch data.name
        when "dw.presentation.start.screencast"
          scope.slides.slideIndex = data.activeSlide
          ViewerService.setSlides(scope, data.content)
          break
        when "dw.presentation.stop.screencast"
          scope.canvas.clear()
          scope.canvas.renderAll()
          break
        when "dw.presentation.set.activeslide"
          ViewerService.showSlide(scope, data.activeSlide)
          break
        when "dw.presentation.send.command"
          ViewerService.doCommand(scope, data.command)
          break
        when "dw.presentation.update.userlist"
          ViewerService.addUserToList(scope, data)
          break
        when "dw.presentation.send.message", "dw.presentation.update.messagelist"
          ViewerService.addMessageToList(scope, data.message)
          break
        when "dw.presentation.unsubscribe"
          ViewerService.removeUserFromList(scope, data)
          break

    sendCommand: (scope, command) ->
      event =
        name: "dw.presentation.send.command"
        args: [
          window.AppGlobals.presentationID,
          command
        ]
      scope.sess.publish(scope.topic, event, true)

    registerPresenter: (scope) ->
      that = this
      content = scope.slides.content[0].json
      data = scope.canvas.getElement().toDataURL('image/png', 0.3);
      base64 = data.replace("data:image/png;base64,","");
      scope.sess.call("dw.presentation.register.presentator",
        window.AppGlobals.username,
        window.AppGlobals.userID,
        window.AppGlobals.presentationID,
        content,
        base64).then((res) ->
          SlidesService.setSlides(scope, res.content)
          scope.recordTime = parseInt(res.recordTime,10)
          scope.formatRecordTime()
          that.getUserList(scope)
          that.getMessageList(scope)
          scope.$apply()
        )

    reRegisterPresenter: (scope) ->
      that = this
      completePresentation = SlidesService.getSlides(scope)
      scope.sess.call("dw.presentation.reregister.presentator",
        window.AppGlobals.username,
        window.AppGlobals.userID,
        window.AppGlobals.presentationID,
        completePresentation).then((res) ->
          SlidesService.setSlides(scope, res.content)
          scope.recordTime = parseInt(res.recordTime,10)
          scope.formatRecordTime()
          that.getUserList(scope)
          that.getMessageList(scope)
          scope.$apply()
        )

    registerUser: (scope) ->
      that = this
      scope.sess.call("dw.presentation.register.user",
        window.AppGlobals.username,
        window.AppGlobals.userID,
        window.AppGlobals.presentationID).then((res) ->
          if res.activeSlide > -1
            scope.slides.slideIndex = res.activeSlide
            ViewerService.setSlides(scope, res.content)
          that.getUserList(scope)
          that.getMessageList(scope)
          scope.$apply()
        )

    addSlide: (scope, slideIndex) ->
      if scope.sess?
        StatusService.enableStatus(scope, "saving", true)
        data = scope.canvas.getElement().toDataURL('image/png', 0.3);
        base64 = data.replace("data:image/png;base64,","");
        scope.sess.call("dw.presentation.add.slide",
          window.AppGlobals.userID,
          window.AppGlobals.presentationID,
          slideIndex,
          scope.slides.content[slideIndex].json,
          base64).then((res) ->
            console.log("presentation:saved")
            StatusService.enableStatus(scope, "saved", true)
          )
      else
        @establishConnection(scope)


    moveSlide: (scope, from, to) ->
      if scope.sess?
        StatusService.enableStatus(scope, "saving", true)
        scope.sess.call("dw.presentation.move.slide",
          window.AppGlobals.userID,
          window.AppGlobals.presentationID,
          from,
          to).then((res) ->
            console.log("presentation:saved")
            StatusService.enableStatus(scope, "saved", true)
          )
      else
        @establishConnection(scope)

    removeSlide: (scope, slideIndex) ->
      if scope.sess?
        StatusService.enableStatus(scope, "saving", true)
        scope.sess.call("dw.presentation.remove.slide",
          window.AppGlobals.userID,
          window.AppGlobals.presentationID,
          slideIndex).then((res) ->
            console.log("presentation:saved")
            StatusService.enableStatus(scope, "saved", true)
          )
      else
        @establishConnection(scope)

    saveSlide: (scope, slideIndex) ->
      if scope.sess?
        StatusService.enableStatus(scope, "saving", true)
        base64 = SlidesService.getBase64ImageOfActiveSlide(scope)
        scope.sess.call("dw.presentation.save.slide",
          window.AppGlobals.userID,
          window.AppGlobals.presentationID,
          slideIndex,
          scope.slides.content[slideIndex].json,
          base64).then((res) ->
            console.log("presentation:saved")
            StatusService.enableStatus(scope, "saved", true)
          )
      else
        @establishConnection(scope)

    setActiveSlide: (scope) ->
      event =
        name: "dw.presentation.set.activeslide"
        args: [
          window.AppGlobals.userID,
          window.AppGlobals.presentationID,
          scope.slides.slideIndex
        ]
      scope.sess.publish(scope.topic, event, true)

    startScreencast: (scope) ->
      if scope.sess?
        event =
          name: "dw.presentation.start.screencast"
          args: [
            window.AppGlobals.userID,
            window.AppGlobals.presentationID,
            scope.slides.slideIndex
          ]
        scope.sess.publish(scope.topic, event, true)
      else
        @establishConnection(scope)

    stopScreencast: (scope) ->
      if scope.sess?
        event =
          name: "dw.presentation.stop.screencast"
          args: [
            window.AppGlobals.userID,
            window.AppGlobals.presentationID,
            -1
          ]
        scope.sess.publish(scope.topic, event, true)
      else
        @establishConnection(scope)

    getUserList: (scope) ->
      that = this
      scope.sess.call("dw.presentation.get.userlist",
        window.AppGlobals.presentationID).then((res) ->
          that.updateUserList(scope, window.AppGlobals.userID)
          ViewerService.setUserList(scope, res.usersCount, res.users)
        )

    updateUserList: (scope, userID) ->
      if scope.sess?
        event =
          name: "dw.presentation.update.userlist"
          args: [
            userID
          ]
        scope.sess.publish(scope.topic, event, true)
      else
        @establishConnection(scope)

    getMessageList: (scope) ->
      that = this
      scope.sess.call("dw.presentation.get.messagelist",
        window.AppGlobals.presentationID).then((res) ->
          that.updateMessageList(scope, window.AppGlobals.userID)
          ViewerService.setMessageList(scope, res.messages)
        )

    updateMessageList: (scope, userID) ->
      if scope.sess?
        event =
          name: "dw.presentation.update.messagelist"
          args: []
        scope.sess.publish(scope.topic, event, true)
      else
        @establishConnection(scope)

    sendMessage: (scope, msg) ->
      if scope.sess?
        event =
          name: "dw.presentation.send.message"
          args: [
            msg
          ]
        scope.sess.publish(scope.topic, event)
      else
        @establishConnection(scope)

    sendRecordTime: (scope) ->
      if scope.sess?
        scope.sess.call("dw.presentation.save.recordtime",
          window.AppGlobals.userID,
          window.AppGlobals.presentationID,
          scope.recordTime)
      else
        @establishConnection(scope)

    startRecording: (scope) ->
      @captureCanvas(scope, document.getElementById('whiteboard'))

    captureCanvas: (scope, element) ->
      if scope.timer? and scope.sess?
        that = this
        $("#webcam2").css("display", "block")
        html2canvas element,
          background: "#fff"
          onrendered: (canvas) ->
            #TODO set to "image/webp" if available on php 5.5
            screenshot = canvas.toDataURL("image/jpeg", 0.1)
            $("#webcam2").css("display", "none")
            if originScreenshot != screenshot
              originScreenshot = screenshot
              date = new Date()
              created = date.getTime()
              time = created - scope.timerStart
              time = 0 if time < 0
              scope.sess.call("dw.presentation.record.image",
                window.AppGlobals.userID,
                window.AppGlobals.presentationID,
                screenshot,
                time).then((res) ->
                  # prevent fast sending
                  date = new Date()
                  stored = date.getTime()
                  storingDuration = stored - created
                  if storingDuration < 300
                    setTimeout( () ->
                      that.captureCanvas(scope, element)
                    , 300 - storingDuration)
                  else
                    that.captureCanvas(scope, element)
                )
            else
              setTimeout( () ->
                that.captureCanvas(scope, element)
              , 500)
      return

  }
])