'use strict'

angular.module('digitalWhiteboardApp.status_services', []).factory("StatusService", () ->
  return {

    disableAllStatus: (scope) ->
      scope.status.connected = false
      scope.status.noConnection = false
      scope.status.saved = false
      scope.status.saving = false

    disableStatus: (scope, status) ->
      switch status
        when "connected"
          if scope.status.connected
            scope.status.connected = false
        when "noConnection"
          if scope.status.noConnection
            scope.status.noConnection = false
        when "saved"
          if scope.status.saved
            scope.status.saved = false
        when "saving"
          if scope.status.saving
            scope.status.saving = false
      scope.$apply() if not scope.$$phase

    enableStatus: (scope, status, enableAnimation) ->
      that = this
      @disableAllStatus(scope)
      switch status
        when "connected"
          scope.status.connected = true
        when "noConnection"
          scope.status.noConnection = true
        when "saved"
          scope.status.saved = true
        when "saving"
          scope.status.saving = true
      if not scope.status.noConnection
        setTimeout( () ->
          that.disableStatus(scope, status)
        , 3000 )
      scope.$apply() if not scope.$$phase
  }
)