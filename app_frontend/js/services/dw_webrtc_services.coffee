'use non-strict'

angular.module('digitalWhiteboardApp.webrtc_services', []).factory("WebRTCService", ["ToolbarService", "WhiteboardService", "$interval", (ToolbarService, WhiteboardService, $interval) ->

  SIGNALING_SERVER = window.AppConfig.webSocketUrl + ':' + window.AppConfig.webSocketWebRTCPort

  connectionVideo = null
  connectionDesktop = null
  websocket = null
  videoStreamID = null
  desktopStreamID = null
  videoSessionDescription = null
  desktopSessionDescription = null
  sessions = { }
  onMessageCallbacks = {}
  currentUserUUID = Math.round(Math.random() * 60535) + 5000
  recorder = null
  recordedTime = 0
  timerInterval = null
  iceServers = []

  iceServers.push({
    url: 'stun:'+window.AppConfig.iceServer
    credential: window.AppConfig.resiprocatePassword
    username: window.AppConfig.resiprocateUsername
  });

  iceServers.push({
    url: 'turn:'+window.AppConfig.iceServer
    credential: window.AppConfig.resiprocatePassword
    username: window.AppConfig.resiprocateUsername
  });

  screenConstraints = {
    optional: [
      bandwidth: 256
    ]
  }

  videoConstraints = {
    mandatory: {
      maxWidth: 320
      maxHeight: 240
      minWidth: 320
      minHeight: 240
    }
    optional: [
      bandwidth: 256
    ]
  }

  audioConstraints = {
    mandatory: {
      googEchoCancellation: true
      googAutoGainControl: true
      googNoiseSuppression: true
      googHighpassFilter: true
      googTypingNoiseDetection: true
    }
    optional: [
      {
        bandwidth: 256
      }
    ]
  }

  return {
    addSharedProperties: (connection) ->
      connection.iceServers = iceServers
      connection.getExternalIceServers = false
      connection.isAcceptNewSession = true
      connection.waitUntilRemoteStreamStartsFlowing = true
      connection.trickleIce = true
      connection.direction = "one-to-many"
      connection.log = window.AppConfig.enableWebRTCLogs
      connection.transmitRoomOnce = true
      connection.autoCloseEntireSession = true
      connection.resources = {
        muted: ''
      }

      if window.AppConfig.enableWebRTCLogs
        connection.resources.RecordRTC = '/js/main_part_1.js'
        connection.resources.html2canvas = '/js/main_part_1.js'
      else
        connection.resources.RecordRTC = '/js/main.js'
        connection.resources.html2canvas = '/js/main.js'

      connection.sdpConstraints.mandatory = {
        OfferToReceiveAudio: true
        OfferToReceiveVideo: true
      }

      connection.openSignalingChannel = (config) ->
        channel = config.channel or @channel
        onMessageCallbacks[channel] = config.onmessage
        setTimeout config.onopen, 1000  if config.onopen

        send: (message) ->
          websocket.send JSON.stringify(
            sender: currentUserUUID
            channel: channel
            message: message
          )
          return

        channel: channel

    createVideoConnection: (scope, presenter) ->
      connection = new RTCMultiConnection('room-presentation-' + window.AppGlobals.presentationID+'-video')
      connection.channel = 'room-presentation-' + window.AppGlobals.presentationID+'-video'

      @addSharedProperties(connection)

      connection.mediaConstraints = {
        video: videoConstraints
        audio: audioConstraints
      };

      connection.session = {
        video: true
        audio: true
        oneway: true
        broadcast: true
      }

      if presenter
        @addHandlersForInitiator(scope, connection)
        @addHandlersForInitiatorVideo(scope, connection)
        setTimeout( () ->
          connection.DetectRTC.load(() ->
            if connection.caniuse.getUserMedia and connection.DetectRTC.hasWebcam
              scope.tools.webcamAvailable = true
            scope.$apply()
          )
        , 2000)
      else
        @addHandlersForParticipant(scope, connection)
        @addHandlersForParticipantVideo(scope, connection)
        setTimeout( () ->
          connection.sendCustomMessage({
            action: "get.session.description"
            sender: connection.userid
          })
        , 1000)

      return connection


    createDesktopConnection: (scope, presenter) ->
      connection = new RTCMultiConnection('room-presentation-' + window.AppGlobals.presentationID+'-desktop')
      connection.channel = 'room-presentation-' + window.AppGlobals.presentationID+'-desktop'

      @addSharedProperties(connection)

      connection.mediaConstraints = {
        mandatory:{
          chromeMediaSource: "screen"
        }
        video: screenConstraints
      };

      connection.session = {
        screen: true
        oneway: true
        broadcast: true
      }

      if presenter
        @addHandlersForInitiator(scope, connection)
        @addHandlersForInitiatorDesktop(scope, connection)
        setTimeout( () ->
          connection.DetectRTC.load(() ->
            connection.DetectRTC.screen.chromeMediaSource = 'screen'
            extensionid = 'ajhifddimkapgcifgcodmmfdlknahffk'
            connection.DetectRTC.screen.getChromeExtensionStatus(extensionid, (status) ->
              if status == 'installed-enabled'
                scope.tools.desktopAvailable = true
                scope.$apply()
            )
          )
        , 2000)
      else
        @addHandlersForParticipant(scope, connection)
        @addHandlersForParticipantDesktop(scope, connection)
        setTimeout( () ->
          connection.sendCustomMessage({
            action: "get.session.description"
            sender: connection.userid
          })
        , 1000)

      return connection

    establishConnection: (scope, presenter) ->
      that = this

      websocket = new WebSocket(SIGNALING_SERVER)
      websocket.onmessage = (e) ->
        data = JSON.parse(e.data)
        data = jQuery.parseJSON(data)
        return  if data.sender is currentUserUUID
        onMessageCallbacks[data.channel] data.message if onMessageCallbacks[data.channel]
        return

      websocket.onclose = (e) ->
        scope.tools.desktopAvailable = false
        scope.tools.desktop = false
        scope.tools.webcamAvailable = false
        scope.tools.webcam = false
        scope.$apply()

      websocket.push = websocket.send
      websocket.send = (data) ->
        data.sender = currentUserUUID
        websocket.push JSON.stringify(data)
        return

      connectionVideo = @createVideoConnection(scope, presenter)
      connectionDesktop = @createDesktopConnection(scope, presenter)

      websocket.onopen = (e) ->
        connectionVideo.connect()
        connectionDesktop.connect()
        return

      if presenter
        canvas = document.getElementById('whiteboard');
        recorder = RecordRTC(canvas, {
          type: 'canvas'
        })

    close: () ->
      connectionVideo.close() if connectionVideo?
      connectionDesktop.close() if connectionDesktop?
      return

    addHandlersForInitiator: (scope, connection) ->
      connection.onstream = (e) ->
        if e.isVideo
          e.mediaElement.width = 320
          e.mediaElement.muted = true
          e.mediaElement.controls = false
          videoStreamID = e.streamid
          $("#webcam").append(e.mediaElement)
          clonedWebcam = $(e.mediaElement).clone()
          clonedWebcam.attr("data-html2canvas-force", "true")
          clonedWebcam.attr("id", $(e.mediaElement).attr("id") + "a")
          clonedWebcam[0].width = 240
          clonedWebcam[0].height = 180
          clonedWebcam[0].play()
          clonedWebcam[0].muted = true
          $("#webcam-over-whiteboard").append(clonedWebcam)

          scope.tools.webcam = true
        if e.isScreen
          scope.tmpImageRepository = scope.imageRepository
          scope.slides.tmpSelected = scope.slides.selected
          scope.tools.desktop = true
          scope.imageRepository = false
          scope.slides.selected = false
          $('.tool').addClass("unselectable-desktop")
          $('.sidebar-tool').addClass("unselectable-desktop")
          $('#tool-desktop').removeClass("unselectable-desktop")
          $('#tool-desktop-stop').removeClass("unselectable-desktop")
          $('#tool-webcam').removeClass("unselectable-desktop")
          $('#tool-webcam-stop').removeClass("unselectable-desktop")
          $('#tool-screencast').removeClass("unselectable-desktop")
          $('#tool-screencast-stop').removeClass("unselectable-desktop")
          $('#tool-record').removeClass("unselectable-desktop")
          $('#tool-record-stop').removeClass("unselectable-desktop")
          ToolbarService.deselectTools(scope)
          WhiteboardService.deactivateObjects(scope)
          scope.canvas.defaultCursor = 'default'
          e.mediaElement.controls = false
          e.mediaElement.width = parseInt($('#whiteboard').css('width'), 10)
          e.mediaElement.height = parseInt($('#whiteboard').css('height'), 10)
          desktopStreamID = e.streamid
          $(".canvas-container").hide()
          $("#status").hide()
          $(e.mediaElement).insertAfter($(".canvas-container"))
          scope.tools.desktop = true
          connection.sendCustomMessage({
            action: "join.new.session"
            param: desktopSessionDescription
          })
        scope.$apply()

      connection.onstreamended = (e) ->
        if e.isVideo
          if e.mediaElement.parentNode
            $("#webcam-over-whiteboard").empty()
            e.mediaElement.parentNode.removeChild(e.mediaElement)
          scope.tools.webcam = false
          scope.$apply()
          videoStreamID = null
        if e.isScreen
          if e.mediaElement.parentNode
            e.mediaElement.parentNode.removeChild(e.mediaElement)
            $(".canvas-container").show()
            $("#status").show()
          if scope.tools.desktop
            $("#tool-desktop-stop").click()
          desktopStreamID = null

      return

    addHandlersForInitiatorVideo: (scope, connection) ->
      connection.onCustomMessage = (e) ->
        switch e.action
          when "get.session.description"
            if videoSessionDescription? and videoStreamID?
              connection.sendCustomMessage({
                toSender: e.sender
                action: "load.session.description"
                param: videoSessionDescription
              })
      return

    addHandlersForInitiatorDesktop: (scope, connection) ->
      connection.onCustomMessage = (e) ->
        switch e.action
          when "get.session.description"
            if desktopSessionDescription? and desktopStreamID?
              connection.sendCustomMessage({
                toSender: e.sender
                action: "load.session.description"
                param: desktopSessionDescription
              })
      return

    addHandlersForParticipant: (scope, connection) ->

      that = this

      connection.onstream = (e) ->
        if e.isVideo
          e.mediaElement.width = 320
          e.mediaElement.height = 240
          e.mediaElement.autoplay = on
          videoStreamID = e.streamid
          $("#webcam").append(e.mediaElement)
          scope.tools.webcamStarted = false
          scope.$apply()
          $("#webcam video")[0].play()
        if e.isScreen
          e.mediaElement.width = parseInt($('#whiteboard').css('width'), 10)
          e.mediaElement.height = parseInt($('#whiteboard').css('height'), 10)
          e.mediaElement.autoplay = on
          e.mediaElement.controls = false
          desktopStreamID = e.streamid
          $("#whiteboard canvas").hide()
          $("#status").hide()
          $("#whiteboard").append(e.mediaElement)
          $("#whiteboard video")[0].play()

      connection.onstreamended = (e) ->
        if e.isVideo
          if e.mediaElement.parentNode
            e.mediaElement.parentNode.removeChild(e.mediaElement)
          scope.tools.webcamStarted = false
          scope.tools.webcam = false
          scope.$apply()
          videoStreamID = null
          connection.leave()
        if e.isScreen
          if e.mediaElement.parentNode
            e.mediaElement.parentNode.removeChild(e.mediaElement)
          $("#whiteboard canvas").show()
          $("#status").show()
          desktopStreamID = null
          connection.leave()


        return

    addHandlersForParticipantVideo: (scope, connection) ->

      that = this

      connection.onNewSession = (session) ->
        that.joinVideoStream(scope, session, connection)

      connection.onCustomMessage = (e) ->
        switch e.action
          when "stop.stream"
            scope.tools.webcamStarted = false
            scope.tools.webcam = false
            scope.$apply()
          when "start.stream"
            scope.tools.webcam = true
            scope.tools.webcamStarted = true
            scope.$apply()
          when "load.session.description"
            if e.toSender is connection.userid
              that.joinVideoStream(scope, e.param, connection)
          when "set.session.description"
            that.joinVideoStream(scope, e.param, connection)

      return

    addHandlersForParticipantDesktop: (scope, connection) ->

      that = this

      connection.onNewSession = (session) ->
        that.joinDesktopStream(session, connection)

      connection.onCustomMessage = (e) ->
        switch e.action
          when "load.session.description"
            if e.toSender is connection.userid
              that.joinDesktopStream(e.param, connection)
          when "join.new.session"
            if desktopStreamID?
              connection.leave()
              desktopStreamID = null
            that.joinDesktopStream(e.param, connection)

      return

    startWebcam: (scope) ->
      if websocket? and connectionVideo?
        that = this
        date = new Date()
        videoSessionDescription = connectionVideo.open({
          sessionid: 'channel-presentation-'+window.AppGlobals.presentationID+'-'+date.getTime()
          onMediaCaptured: () ->
            if scope.timer
              that.startRecording(scope)
            connectionVideo.sendCustomMessage({
              action: "start.stream"
            })
            connectionVideo.sendCustomMessage({
              action: "set.session.description"
              param: videoSessionDescription
            })
        })
      else
        @establishConnection(scope, true)

      return

    stopWebcam: (scope) ->
      if websocket? and connectionVideo?
        if scope.timer
          @stopRecording(scope)
        connectionVideo.sendCustomMessage({
          action: "stop.stream"
        })
        connectionVideo.streams[videoStreamID].stop()
        connectionVideo.leave()
        scope.tools.webcam = false
        scope.$apply()
      else
        @establishConnection(scope, true)

      return

    startDesktop: (scope) ->
      if websocket? and connectionDesktop?
        date = new Date()
        desktopSessionDescription = connectionDesktop.open('channel-presentation-'+window.AppGlobals.presentationID+'-'+date.getTime())
      else
        @establishConnection(scope, true)

      return

    stopDesktop: (scope) ->
      if websocket? and connectionDesktop?
        connectionDesktop.streams[desktopStreamID].stop()
        connectionDesktop.leave()
        scope.tools.desktop = false
        scope.$apply()
      else
        @establishConnection(scope, true)

      return

    joinVideoStream: (scope, session, connection) ->
      console.log("new session")
      console.log(session)

      if sessions[session.sessionid]
        return
      sessions[session.sessionid] = session

      scope.tools.webcam = true
      scope.tools.webcamStarted = true
      scope.$apply()

      joinRoomButton = $("#join")
      joinRoomButton.attr('data-session-id', session.sessionid)

      joinRoomButton.click( () ->
        sessionID = this.getAttribute('data-session-id')
        session = sessions[sessionID]

        if !session
          throw 'No such session exists.'

        connection.join(session)
      )

      return

    joinDesktopStream: (session, connection) ->
      if sessions[session.sessionid]
        return
      sessions[session.sessionid] = session

      connection.join(session)
      return

    startRecording: (scope) ->
      if videoStreamID?
        that = this
        console.log("start:recording")
        connectionVideo.streams[videoStreamID].startRecording('audio')
        timerInterval = $interval(() ->
          recordedTime++
        , 1000)
        setTimeout( () ->
          if scope.timer?
            that.stopRecording(scope)
        , 10000)

    stopRecording: (scope) ->
      if videoStreamID?
        console.log("stop:recording")
        if scope.sess?
          that = this
          xhr = (url, data) ->
            request = new XMLHttpRequest()
            request.open "POST", url, true
            request.send data
          connectionVideo.streams[videoStreamID].stopRecording((blob) ->
            $interval.cancel(timerInterval)
            timerInterval = null
            formData = new FormData()
            formData.append "audioBlob", blob.audio
            formData.append "recordTime", ( scope.recordTime - recordedTime )
            formData.append "presentationID", window.AppGlobals.presentationID
            formData.append "userID", window.AppGlobals.userID
            recordedTime = 0
            xhr "record", formData
            if scope.timer?
              that.startRecording(scope)
          , 'audio' )
  }
])