'use strict'

angular.module('digitalWhiteboardApp.toolbar_services', []).factory("ToolbarService", ['WhiteboardService', 'CommandService', 'SlideService', (WhiteboardService, CommandService, SlideService) ->
  return {
    selectActionTools: (scope) ->
      scope.tools.delete = true
      @selectLayerTools(scope)
    deselectActionTools: (scope) ->
      scope.tools.delete = false
      @deselectLayerTools(scope)
    deselectLayerTools: (scope) ->
      scope.tools.layerDown = false
      scope.tools.layerUp = false
    deselectTools: (scope) ->
      scope.tools.cursor = false
      scope.canvas.discardActiveGroup()
      scope.canvas.discardActiveObject()
      scope.canvas.renderAll()
      @deselectEditingTools(scope)
    deselectEditingTools: (scope) ->
      scope.commandAction = false
      scope.tools.pen.selected = false
      scope.tools.line.selected = false
      scope.tools.forms.selected = false
      scope.tools.text.selected = false
      scope.tools.text.isCreating = false
      scope.tools.text.blockCreating = false
      for obj, idx in scope.canvas.getObjects()
        if obj? and obj.type is "i-text"
          obj.exitEditing() if obj.isEditing
      scope.tools.group.isEditingMode = false
      scope.tools.group.selected = false
      scope.tools.group.line.selected = false
      scope.tools.group.pen.selected = false
      scope.tools.group.forms.selected = false
      scope.tools.group.text.selected = false
      @deselectActionTools(scope)
    selectLayerTools: (scope) ->
      @selectLayerDown(scope)
      @selectLayerUp(scope)
    selectLayerDown: (scope) ->
      object = scope.canvas.getActiveObject()
      idx = scope.canvas._objects.indexOf(object);
      if idx > 0
        scope.tools.layerDown = true
      else
        scope.tools.layerDown = false
    selectLayerUp: (scope) ->
      object = scope.canvas.getActiveObject()
      idx = scope.canvas._objects.indexOf(object);
      if idx isnt scope.canvas._objects.length - 1
        scope.tools.layerUp = true
      else
        scope.tools.layerUp = false
    deselectPenDefaults: (scope) ->
      scope.tools.pen.default1.selected = false
      scope.tools.pen.default2.selected = false
      scope.tools.pen.default3.selected = false
      scope.tools.pen.default4.selected = false
      scope.tools.pen.default5.selected = false
    deselectLineDefaults: (scope) ->
      scope.tools.line.default1.selected = false
      scope.tools.line.default2.selected = false
      scope.tools.line.default3.selected = false
      scope.tools.line.default4.selected = false
    deselectFormsDefaults: (scope) ->
      scope.tools.forms.circle.selected = false
      scope.tools.forms.ellipse.selected = false
      scope.tools.forms.triangle.selected = false
      scope.tools.forms.square.selected = false
      scope.tools.forms.rectangle.selected = false
    deselectTextDefaults: (scope) ->
      scope.tools.text.big.selected = false
      scope.tools.text.small.selected = false
    deselectSidebar: (scope) ->
      if not scope.sidebarFixed
        scope.imageRepository = false
        scope.slides.selected = false
    redo: (scope) ->
      @deselectActionTools(scope) if not scope.canvas.options.isEditingMode
      if scope.stackIndex >= scope.commands.length
        scope.stackIndex = scope.commands.length
      CommandService.doRedo(scope, scope.commands[scope.stackIndex-1])
      if not (scope.canvas.getActiveObject()? or scope.canvas.getActiveGroup()?) and scope.canvas.options.isEditingMode
        @deselectEditingTools(scope)
      WhiteboardService.activateObjects(scope) if scope.tools.cursor
    undo: (scope) ->
      @deselectActionTools(scope) if not scope.canvas.options.isEditingMode
      if scope.stackIndex < 0
        scope.stackIndex = 0
      CommandService.doUndo(scope, scope.commands[scope.stackIndex])
      if not (scope.canvas.getActiveObject()? or scope.canvas.getActiveGroup()?) and scope.canvas.options.isEditingMode
        @deselectEditingTools(scope)
      WhiteboardService.activateObjects(scope) if scope.tools.cursor
    openPenEditingMode: (object, scope) ->
      scope.tools.pen.selected = true
      scope.tools.pen.size = object.get('strokeWidth')
      scope.tools.pen.color = object.get('stroke').replace(/^#?/, "");
      document.getElementById("tool-pen-color-field").color.fromString(scope.tools.pen.color)
    openLineEditingMode: (object, scope) ->
      scope.tools.line.selected = true
      scope.tools.line.size = object.get('strokeWidth')
      scope.tools.line.color = object.get('stroke').replace(/^#?/, "")
      scope.tools.line.style = @convertStrokeDashArray(object.get('strokeDashArray'))
      document.getElementById("tool-line-color-field").color.fromString(scope.tools.line.color)
    openFormsEditingMode: (object, scope) ->
      scope.tools.forms.selected = true
      scope.tools.forms.borderStyle = @convertStrokeDashArray(object.get('strokeDashArray'))
      scope.tools.forms.borderSize = object.get('strokeWidth')
      scope.tools.forms.borderColor = object.get('stroke').replace(/^#?/, "")
      scope.tools.forms.color = object.get('fill').replace(/^#?/, "")
      document.getElementById("tool-forms-border-color-field").color.fromString(scope.tools.forms.borderColor)
      document.getElementById("tool-forms-fill-color-field").color.fromString(scope.tools.forms.color)
    openTextEditingMode: (object, scope) ->
      scope.tools.text.fontFamily = @convertFontFamilyNameToKey(object.get('fontFamily'))
      scope.tools.text.fontSize = object.get('fontSize')
      if object.get('fontStyle') is 'italic'
        scope.tools.text.italic = true
      else
        scope.tools.text.italic = false
      if object.get('textDecoration') is 'underline'
        scope.tools.text.underline = true
      else
        scope.tools.text.underline = false
      if object.get('fontWeight') is 'bold'
        scope.tools.text.bold = true
      else
        scope.tools.text.bold = false
      scope.tools.text.leftAlign = false
      scope.tools.text.centerAlign = false
      scope.tools.text.rightAlign = false
      scope.tools.text.leftAlign = true if object.get('textAlign') is 'left'
      scope.tools.text.centerAlign = true if object.get('textAlign') is 'center'
      scope.tools.text.rightAlign = true if object.get('textAlign') is 'right'
      scope.tools.text.color = object.get('fill').replace(/^#?/, "")
      document.getElementById("tool-text-color-field").color.fromString(scope.tools.text.color)

      if scope.tools.text.selected is false
        if @checkEverySelectionForAttribute('fontWeight', 'bold', object)
          scope.tools.text.bold = true
        else if @checkEverySelectionForAttribute('fontWeight', 'normal', object)
          scope.tools.text.bold = false
        else
          if object.get('fontWeight') is 'bold'
            scope.tools.text.bold = true
          else
            scope.tools.text.bold = false
        if @checkEverySelectionForAttribute('fontStyle', 'italic', object)
          scope.tools.text.italic = true
        else if @checkEverySelectionForAttribute('fontStyle', 'normal', object)
          scope.tools.text.italic = false
        else
          if object.get('fontStyle') is 'italic'
            scope.tools.text.italic = true
          else
            scope.tools.text.italic = false
        if @checkEverySelectionForAttribute('textDecoration', 'underline', object)
          scope.tools.text.underline = true
        else if @checkEverySelectionForAttribute('textDecoration', 'normal', object)
          scope.tools.text.underline = false
        else
          if object.get('textDecoration') is 'underline'
            scope.tools.text.underline = true
          else
            scope.tools.text.underline = false
        styles = object.getSelectionStyles(0,1)
        if @checkEverySelectionForAttribute('fontSize', styles[0].fontSize, object)
          scope.tools.text.fontSize = styles[0].fontSize
        else
          scope.tools.text.fontSize = object.get('fontSize')
        if @checkEverySelectionForAttribute('fill', styles[0].fill, object)
          scope.tools.text.color = styles[0].fill.replace(/^#?/, "")
          document.getElementById("tool-text-color-field").color.fromString(scope.tools.text.color)
          document.getElementById("tool-group-text-color-field").color.fromString(scope.tools.text.color)
        else
          scope.tools.text.color = object.get('fill').replace(/^#?/, "")
          document.getElementById("tool-text-color-field").color.fromString(scope.tools.text.color)
          document.getElementById("tool-group-text-color-field").color.fromString(scope.tools.text.color)
        if @checkEverySelectionForAttribute('fontFamily', styles[0].fontFamily, object)
          scope.tools.text.fontFamily = @convertFontFamilyNameToKey(styles[0].fontFamily)
        else
          scope.tools.text.fontFamily = @convertFontFamilyNameToKey(object.get('fontFamily'))

      scope.tools.text.selected = true

      if object.setSelectionStyles and object.isEditing
        styles = object.getSelectionStyles()
        if styles.hasOwnProperty('fontFamily')
          scope.tools.text.fontFamily = @convertFontFamilyNameToKey(styles.fontFamily)
        if styles.hasOwnProperty('fontSize')
          scope.tools.text.fontSize = styles.fontSize
        if styles.hasOwnProperty('fontStyle')
          if styles.fontStyle is 'italic'
            scope.tools.text.italic = true
          else
            scope.tools.text.italic = false
        if styles.hasOwnProperty('fontWeight')
          if styles.fontWeight is 'bold'
            scope.tools.text.bold = true
          else
            scope.tools.text.bold = false
        if styles.hasOwnProperty('textDecoration')
          if styles.textDecoration is 'underline'
            scope.tools.text.underline = true
          else
            scope.tools.text.underline = false
        if styles.hasOwnProperty('fill')
          scope.tools.text.color = styles.fill.replace(/^#?/, "")
          document.getElementById("tool-text-color-field").color.fromString(scope.tools.text.color)
        if styles.hasOwnProperty('textAlign')
          scope.tools.text.leftAlign = false
          scope.tools.text.centerAlign = false
          scope.tools.text.rightAlign = false
          if styles.textAlign is 'left'
            scope.tools.text.leftAlign = true
          if styles.textAlign is 'center'
            scope.tools.text.centerAlign = true
          if styles.textAlign is 'right'
            scope.tools.text.rightAlign = true
    openImageEditingMode: (image, scope) ->
      scope.tools.cursor = true
    openGroupEditingMode: (group, scope) ->
      @deselectEditingTools(scope)
      scope.tools.delete = true
      scope.commandAction = true
      group.setControlsVisibility({
        bl: false
        br: false
        mb: false
        mr: false
        ml: false
        mt: false
        tl: false
        tr: false
        mtr: true
      })
      group.set({
        lockUniScaling: true
        lockScalingX: true
        lockScalingY: true
        padding: scope.canvas.options.padding
        transparentCorners: scope.canvas.options.transparentCorners
        borderColor: scope.canvas.options.borderColor
        cornerColor: scope.canvas.options.cornerColor
      })
      scope.groupPosition = {
        top: group.get('top')
        left: group.get('left')
      }
      scope.tools.group.selected = true
      for object, idx in group.getObjects()
        switch object.type
          when "path"
            scope.tools.group.pen.selected = true
            if scope.tools.group.line.selected is false and scope.tools.group.forms.selected is false
              scope.tools.group.line.size = object.get('strokeWidth')
              scope.tools.group.line.color = object.get('stroke').replace(/^#?/, "")
              document.getElementById("tool-group-line-color-field").color.fromString(scope.tools.group.line.color)
          when "line"
            scope.tools.group.pen.selected = true
            scope.tools.group.line.selected = true
            if scope.tools.group.forms.selected is false
              scope.tools.group.line.size = object.get('strokeWidth')
              scope.tools.group.line.color = object.get('stroke').replace(/^#?/, "")
              scope.tools.group.line.style = @convertStrokeDashArray(object.get('strokeDashArray'))
              document.getElementById("tool-group-line-color-field").color.fromString(scope.tools.group.line.color)
          when "rect", "triangle", "circle", "ellipse"
            scope.tools.group.pen.selected = true
            scope.tools.group.line.selected = true
            scope.tools.group.forms.selected = true
            scope.tools.group.line.size = object.get('strokeWidth')
            scope.tools.group.line.color = object.get('stroke').replace(/^#?/, "")
            scope.tools.group.line.style = @convertStrokeDashArray(object.get('strokeDashArray'))
            scope.tools.group.forms.color = object.get('fill').replace(/^#?/, "")
            document.getElementById("tool-group-line-color-field").color.fromString(scope.tools.group.line.color)
            document.getElementById("tool-group-forms-fill-color-field").color.fromString(scope.tools.group.forms.color)
          when "i-text"
            scope.tools.group.text.selected = true
            @openTextEditingMode(object, scope)
            scope.tools.text.selected = false
      return
    checkEverySelectionForAttribute: (type, value, object) ->
      result = false
      styleCharCompare = false
      styles = object.get('styles')
      styleCharCompare = true if _.size(styles) > 0
      for keyI, line of styles
        for keyJ, styleObj of line
          if styleObj.hasOwnProperty(type)
            if styleObj[type] isnt value
              styleCharCompare = false
              break
          else
            styleCharCompare = false
            break
      result = true if styleCharCompare is true
      result
    convertStrokeDashArray: (dashArray) ->
      strokeStyle = "dashed" if _.isEqual(dashArray, [5,2])
      strokeStyle = "dotted" if _.isEqual(dashArray, [1,2])
      strokeStyle = "stroke" if dashArray is null
      strokeStyle
    convertFontFamilyNameToKey: (fontFamily) ->
      key = "arial"
      switch fontFamily
        when "Arial"
          key = "arial"
        when "Verdana"
          key = "verdana"
        when "Comic Sans MS"
          key = "comicsans"
        else
          key = "times"
      key
  }
])