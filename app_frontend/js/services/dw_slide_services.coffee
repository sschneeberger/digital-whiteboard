'use strict'

angular.module('digitalWhiteboardApp.slide_services', []).factory("SlideService", ["SocketService", "SlidesService", (SocketService, SlidesService) ->
  return {
    addSlide: (scope, slideIndex) ->
      scope.canvas.clear()
      scope.slidesOpen()
      content = @getSlideContent(scope)
      scope.tools.back = false
      scope.tools.next = false
      scope.slides.slideIndex = slideIndex
      scope.tools.removeSlide = true
      scope.slides.content.splice(scope.slides.slideIndex, 0, content)
      SlidesService.setBackAndNext(scope)
      SocketService.addSlide(scope, slideIndex)

    reAddSlide: (scope, slideIndex, content) ->
      scope.canvas.clear()
      scope.slidesOpen()
      that = this
      scope.canvas.loadFromJSON(content, () ->
        SlidesService.renderSlide(scope)
        content = that.getSlideContent(scope)
        scope.tools.back = false
        scope.tools.next = false
        scope.slides.slideIndex = slideIndex
        scope.tools.removeSlide = true
        scope.slides.content.splice(scope.slides.slideIndex, 0, content)
        SlidesService.setBackAndNext(scope)
        scope.$apply()
        SocketService.addSlide(scope, slideIndex)
      )

    moveSlide: (scope, from, to) ->
      tmpItem = scope.slides.content[from]
      scope.slides.content.splice(from, 1)
      scope.slides.content.splice(to, 0, tmpItem)
      SocketService.moveSlide(scope, from, to)

    removeSlide: (scope, slideIndex) ->
      scope.canvas.clear()
      scope.slidesOpen()
      scope.slides.content.splice(slideIndex, 1)
      scope.tools.back = false
      scope.tools.next = false
      if scope.slides.content.length is 1
        scope.tools.removeSlide = false
      if scope.slides.slideIndex is 0 and slideIndex is 0
        SlidesService.showSlide(scope, scope.slides.slideIndex)
      else if scope.slides.slideIndex isnt (slideIndex - 1)
        scope.slides.slideIndex = slideIndex - 1
        if scope.slides.slideIndex < 0
          scope.slides.slideIndex = 0
        SlidesService.showSlide(scope, scope.slides.slideIndex)
      else
        SlidesService.setBackAndNext(scope)
      SocketService.removeSlide(scope, slideIndex)

    saveInitialSlide: (scope, slideIndex) ->
      scope.slides.content[slideIndex] = @getSlideContent(scope)

    saveSlide: (scope, slideIndex) ->
      scope.slides.content[slideIndex] = @getSlideContent(scope)
      SocketService.saveSlide(scope, slideIndex)

    getSlideContent: (scope) ->
      content =
        json: JSON.stringify(scope.canvas.toDatalessJSON(scope.canvas.options.propertiesToInclude))

      groupObjects = []
      object = null
      originX = ""
      originY = ""

      if scope.canvas.getActiveObject()?
        object = scope.canvas.getActiveObject()
      if scope.canvas.getActiveGroup()?
        originX = scope.canvas.getActiveGroup().get("originX")
        originY = scope.canvas.getActiveGroup().get("originY")
        for object in scope.canvas.getActiveGroup().getObjects()
          groupObjects.push(object)
      scope.canvas.deactivateAll().renderAll()

      scope.canvas.getElement().toBlob( (blob) ->
        newImg = document.createElement("img")
        url = window.webkitURL.createObjectURL(blob)
        newImg.onload = () ->
          window.webkitURL.revokeObjectURL(url)
        newImg.src = url
        newImg.width = 160
        newImg.height = 100
        content.img = $(newImg).prop('outerHTML')
      , 'image/jpeg')

      if object?
        scope.canvas.setActiveObject(object)
        scope.canvas.renderAll()
      if groupObjects.length > 0
        group = new fabric.Group(groupObjects,{
          originX: originX
          originY: originY
        })
        for groupObject in groupObjects
          groupObject.set('active', true)
        scope.canvas._activeObject = null
        scope.canvas.setActiveGroup(group.setCoords()).renderAll()
      content
  }
])