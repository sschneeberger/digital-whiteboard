'use strict'

angular.module('digitalWhiteboardApp.properties_services', []).factory("PropertiesService", () ->
  return {
    getSharedProperties: (scope, object) ->
      properties =
        width: object.get("width")
        height: object.get("height")
        left: object.get("left")
        top: object.get("top")
        uid: object.get("uid")
        angle: object.get("angle")
        padding: object.get("padding")
        transparentCorners: object.get("transparentCorners")
        borderColor: object.get("borderColor")
        cornerColor: object.get("cornerColor")
        selectable: object.get("selectable")
        originX: object.get("originX")
        originY: object.get("originY")
        scaleX: object.get("scaleX")
        scaleY: object.get("scaleY")
        slideIndex: scope.slides.slideIndex
      properties

    getPropertiesForTextAdded: (object, properties) ->
      properties.text = object.get("text")
      properties.fontSize = object.get("fontSize")
      properties.fill = object.get("fill")
      properties.fontFamily = object.get("fontFamily")
      properties.textDecoration = object.get("textDecoration")
      properties.fontStyle = object.get("fontStyle")
      properties.fontWeight = object.get("fontWeight")
      properties.textAlign = object.get("textAlign")
      properties.styles = object.get("styles")
      properties.originStyles = object.get("originStyles")
      properties

    getPropertiesForTextRemoved: (origin, properties) ->
      properties.text = origin.originalState.text
      properties.fontSize = origin.originalState.fontSize
      properties.fill = origin.originalState.fill
      properties.fontFamily = origin.originalState.fontFamily
      properties.textDecoration = origin.originalState.textDecoration
      properties.fontStyle = origin.originalState.fontStyle
      properties.fontWeight = origin.originalState.fontWeight
      properties.textAlign = origin.originalState.textAlign
      properties.styles = origin.originStyles
      properties.originStyles = origin.originStyles
      properties

    getPropertiesForObjectAdded: (object, properties) ->
      switch object.type
        when "path"
          properties.path = object.get("path")
          properties.stroke = object.get("stroke")
          properties.strokeWidth = object.get("strokeWidth")
          properties.fill = null
          properties.strokeLineCap = "round"
          properties.strokeLineJoin = "round"
        when "line"
          properties.x1 = object.get("x1")
          properties.y1 = object.get("y1")
          properties.x2 = object.get("x2")
          properties.y2 = object.get("y2")
        when "circle"
          properties.radius = object.get("radius")
          properties.lockUniScaling = object.get("lockUniScaling")
        when "ellipse"
          properties.rx = object.get("rx")
          properties.ry = object.get("ry")
        when "rect"
          properties.lockUniScaling = object.get("lockUniScaling")
          properties.x = object.get("x")
          properties.y = object.get("y")
        when "image"
          properties.src = object.getSrc()
      switch object.type
        when "line", "circle", "rect", "triangle", "ellipse"
          properties.stroke = object.get("stroke")
          properties.strokeWidth = object.get("strokeWidth")
          properties.strokeDashArray = object.get("strokeDashArray")
      switch object.type
        when "circle", "rect", "triangle", "ellipse"
          properties.fill = object.get("fill")
      properties

    getPropertiesForObjectModified: (scope, object) ->
      properties = {}
      properties.new =
        angle: object.get("angle")
        top: object.get("top")
        left: object.get("left")
        height: object.get("height")
        width: object.get("width")
        scaleX: object.get("scaleX")
        scaleY: object.get("scaleY")
        originX: object.get("originX")
        originY: object.get("originY")
      properties.origin =
        angle: object.originalState.angle
        top: object.originalState.top
        left: object.originalState.left
        height: object.originalState.height
        width: object.originalState.width
        scaleX: object.originalState.scaleX
        scaleY: object.originalState.scaleY
        originX: object.originalState.originX
        originY: object.originalState.originY
      properties.uid = object.get("uid")
      properties.slideIndex = scope.slides.slideIndex
      properties

    getPropertiesForWhiteboardObjectModified: (scope, object, origin, fromGroup) ->
      properties = {}
      properties.new =
        height: object.get("height")
        width: object.get("width")
        originX: object.get("originX")
        originY: object.get("originY")
        stroke: object.get("stroke")
        strokeWidth: object.get("strokeWidth")
        strokeDashArray: object.get("strokeDashArray")
        fill: object.get("fill")
      properties.origin =
        height: origin.originalState.height
        width: origin.originalState.width
        originX: origin.originalState.originX
        originY: origin.originalState.originY
        stroke: origin.originalState.stroke
        strokeWidth: origin.originalState.strokeWidth
        strokeDashArray: origin.originalState.strokeDashArray
        fill: origin.originalState.fill
      if fromGroup
        properties.new.top = object.originalTop
        properties.new.left = object.originalLeft
        properties.origin.top = origin.originalTop
        properties.origin.left = origin.originalLeft
      else
        properties.new.top = object.get("top")
        properties.new.left = object.get("left")
        properties.origin.top = origin.originalState.top
        properties.origin.left = origin.originalState.left
      properties.uid = object.get("uid")
      properties.slideIndex = scope.slides.slideIndex
      properties

    getPropertiesForTextModified: (scope, object, origin, fromGroup) ->
      properties = {}
      properties.new =
        height: object.get("height")
        width: object.get("width")
        text: object.get("text")
        fontSize: object.get("fontSize")
        fill: object.get("fill")
        fontFamily: object.get("fontFamily")
        textDecoration: object.get("textDecoration")
        fontStyle: object.get("fontStyle")
        fontWeight: object.get("fontWeight")
        textAlign: object.get("textAlign")
        styles: object.get("styles")
        originStyles: object.get("originStyles")
      properties.origin =
        height: origin.originalState.height
        width: origin.originalState.width
        text: origin.originalState.text
        fontSize: origin.originalState.fontSize
        fill: origin.originalState.fill
        fontFamily: origin.originalState.fontFamily
        textDecoration: origin.originalState.textDecoration
        fontStyle: origin.originalState.fontStyle
        fontWeight: origin.originalState.fontWeight
        textAlign: origin.originalState.textAlign
        styles: origin.originStyles
      if fromGroup
        properties.new.top = object.originalTop
        properties.new.left = object.originalLeft
        properties.origin.top = origin.originalTop
        properties.origin.left = origin.originalLeft
      else
        properties.new.top = object.get("top")
        properties.new.left = object.get("left")
        properties.origin.top = origin.originalState.top
        properties.origin.left = origin.originalState.left
      properties.uid = object.get("uid")
      properties.slideIndex = scope.slides.slideIndex
      properties
  }
)