'use strict'

angular.module('digitalWhiteboardApp.whiteboard_services', []).factory("WhiteboardService", () ->
  return {
    activateObjects: (scope) ->
      @deactivateDrawingMode(scope)
      scope.canvas.selection = true
      scope.canvas.defaultCursor = 'default'
      scope.canvas.options.isEditingMode = true
      objects = scope.canvas.getObjects();
      for i of objects
        objects[i].selectable = true
    deactivateObjects: (scope) ->
      @deactivateDrawingMode(scope)
      scope.canvas.selection = false
      scope.canvas.defaultCursor = 'crosshair'
      scope.canvas.options.isEditingMode = false
      scope.canvas.deactivateAll()
      objects = scope.canvas.getObjects()
      for i of objects
        objects[i].selectable = false
      scope.canvas.renderAll()
    activateDrawingMode: (scope) ->
      scope.canvas.defaultCursor = 'crosshair'
      scope.canvas.isDrawingMode = true
      scope.canvas.freeDrawingBrush = new fabric.PencilBrush(scope.canvas)
      scope.canvas.freeDrawingBrush.color = "#" + scope.tools.pen.color
      scope.canvas.freeDrawingBrush.width = scope.tools.pen.size
    deactivateDrawingMode: (scope) ->
      scope.canvas.defaultCursor = 'default'
      scope.canvas.isDrawingMode = false
      scope.canvas.freeDrawingBrush = null
    ###resizeAllObjects: (scope, originWidth) ->
      canvasWidth = parseInt($('#whiteboard').css('width'), 10)
      objects = scope.canvas.getObjects()
      scale = canvasWidth / originWidth
      for i of objects
        scaleX = objects[i].scaleX
        scaleY = objects[i].scaleY
        left = objects[i].left
        top = objects[i].top
        objects[i].scaleX = scaleX * scale
        objects[i].scaleY = scaleY * scale
        objects[i].left = left * scale
        objects[i].top = top * scale
        objects[i].setCoords()
        objects[i].resizeToScale()
      scope.canvas.renderAll()###
    createLine: (scope, options) ->
      object = new fabric.Line([scope.canvas.getPointer(options.e).x,
                                scope.canvas.getPointer(options.e).y,
                                scope.canvas.getPointer(options.e).x + 2,
                                scope.canvas.getPointer(options.e).y + 2],{
        transparentCorners: scope.canvas.options.transparentCorners
        padding: scope.canvas.options.padding
        borderColor: scope.canvas.options.borderColor
        cornerColor: scope.canvas.options.cornerColor
        stroke: '#' + scope.tools.line.color
        strokeWidth: scope.tools.line.size
        strokeDashArray: @convertStrokeStyleToArray(scope.tools.line.style)
        selectable: false
        uid: scope.uid++
        slideIndex: scope.slides.slideIndex
      })
      object
    createCircle: (scope, options) ->
      object = new fabric.Circle({
        radius: 1
        left: scope.canvas.getPointer(options.e).x - 1
        top: scope.canvas.getPointer(options.e).y - 1
        transparentCorners: scope.canvas.options.transparentCorners
        padding: scope.canvas.options.padding
        borderColor: scope.canvas.options.borderColor
        cornerColor: scope.canvas.options.cornerColor
        stroke: '#' + scope.tools.forms.borderColor
        strokeWidth: scope.tools.forms.borderSize
        strokeDashArray: @convertStrokeStyleToArray(scope.tools.forms.borderStyle)
        fill: '#' + scope.tools.forms.color
        lockUniScaling: true
        selectable: false
        uid: scope.uid++
        slideIndex: scope.slides.slideIndex
      })
      object
    createEllipse: (scope, options) ->
      object = new fabric.Ellipse({
        rx: 1
        ry: 1
        left: scope.canvas.getPointer(options.e).x - 1
        top: scope.canvas.getPointer(options.e).y - 1
        padding: scope.canvas.options.padding
        transparentCorners: scope.canvas.options.transparentCorners
        borderColor: scope.canvas.options.borderColor
        cornerColor: scope.canvas.options.cornerColor
        stroke: '#' + scope.tools.forms.borderColor
        strokeWidth: scope.tools.forms.borderSize
        strokeDashArray: @convertStrokeStyleToArray(scope.tools.forms.borderStyle)
        fill: '#' + scope.tools.forms.color
        selectable: false
        originLeft: scope.canvas.getPointer(options.e).x - 1
        originTop: scope.canvas.getPointer(options.e).y - 1
        uid: scope.uid++
        slideIndex: scope.slides.slideIndex
      })
      object
    createTriangle: (scope, options) ->
      object = new fabric.Triangle({
        width: 4
        height: 4
        left: scope.canvas.getPointer(options.e).x - 2
        top: scope.canvas.getPointer(options.e).y - 2
        padding: scope.canvas.options.padding
        transparentCorners: scope.canvas.options.transparentCorners
        borderColor: scope.canvas.options.borderColor
        cornerColor: scope.canvas.options.cornerColor
        stroke: '#' + scope.tools.forms.borderColor
        strokeWidth: scope.tools.forms.borderSize
        strokeDashArray: @convertStrokeStyleToArray(scope.tools.forms.borderStyle)
        fill: '#' + scope.tools.forms.color
        selectable: false
        originLeft: scope.canvas.getPointer(options.e).x - 1
        originTop: scope.canvas.getPointer(options.e).y - 1
        uid: scope.uid++
        slideIndex: scope.slides.slideIndex
      })
      object
    createSquare: (scope, options) ->
      object = new fabric.Rect({
        width: 2
        height: 2
        left: scope.canvas.getPointer(options.e).x - 1
        top: scope.canvas.getPointer(options.e).y - 1
        padding: scope.canvas.options.padding
        transparentCorners: scope.canvas.options.transparentCorners
        borderColor: scope.canvas.options.borderColor
        cornerColor: scope.canvas.options.cornerColor
        stroke: '#' + scope.tools.forms.borderColor
        strokeWidth: scope.tools.forms.borderSize
        strokeDashArray: @convertStrokeStyleToArray(scope.tools.forms.borderStyle)
        fill: '#' + scope.tools.forms.color
        selectable: false
        lockUniScaling: true
        originLeft: scope.canvas.getPointer(options.e).x - 1
        originTop: scope.canvas.getPointer(options.e).y - 1
        uid: scope.uid++
        slideIndex: scope.slides.slideIndex
      })
      object
    createRectangle: (scope, options) ->
      object = new fabric.Rect({
        width: 4
        height: 2
        left: scope.canvas.getPointer(options.e).x - 1
        top: scope.canvas.getPointer(options.e).y - 2
        padding: scope.canvas.options.padding
        transparentCorners: scope.canvas.options.transparentCorners
        borderColor: scope.canvas.options.borderColor
        cornerColor: scope.canvas.options.cornerColor
        stroke: '#' + scope.tools.forms.borderColor
        strokeWidth: scope.tools.forms.borderSize
        strokeDashArray: @convertStrokeStyleToArray(scope.tools.forms.borderStyle)
        fill: '#' + scope.tools.forms.color
        selectable: false
        originLeft: scope.canvas.getPointer(options.e).x - 1
        originTop: scope.canvas.getPointer(options.e).y - 1
        uid: scope.uid++
        slideIndex: scope.slides.slideIndex
      })
      object
    createText: (scope, options) ->
      textAlign = "left"
      textAlign = "center" if scope.tools.text.centerAlign
      textAlign = "right" if scope.tools.text.rightAlign
      fontWeight = "normal"
      fontWeight = "bold" if scope.tools.text.bold
      fontStyle = "normal"
      fontStyle = "italic" if scope.tools.text.italic
      textDecoration = "normal"
      textDecoration = "underline" if scope.tools.text.underline
      object = new fabric.IText("", {
        left: scope.canvas.getPointer(options.e).x - 1
        top: scope.canvas.getPointer(options.e).y - 2
        padding: scope.canvas.options.padding
        transparentCorners: scope.canvas.options.transparentCorners
        borderColor: scope.canvas.options.borderColor
        cornerColor: scope.canvas.options.cornerColor
        selectable: false
        fontSize: scope.tools.text.fontSize
        fill: '#' + scope.tools.text.color
        fontFamily: @convertFontFamilyKeyToName(scope.tools.text.fontFamily)
        textDecoration: textDecoration
        fontStyle: fontStyle
        fontWeight: fontWeight
        textAlign: textAlign
        uid: scope.uid++
        slideIndex: scope.slides.slideIndex
        originStyles: {}
      })
      object = @createTextEventListener(scope, object)
      object
    createTextEventListener: (scope, object) ->
      object.on "editing:entered", () ->
        for obj, idx in scope.canvas.getObjects()
          obj.set("selectable", false) if obj? and obj.type is "i-text" and not obj.isEditing

        object.set("borderColor", scope.canvas.options.borderColor)

        if not scope.canvas.options.isEditingMode
          scope.tools.delete = false
          scope.tools.layerDown = false
          scope.tools.layerUp = false
          scope.$apply()

      object.on "editing:exited", (fromDeleteTool) ->
        fromDeleteTool = false if not fromDeleteTool?

        for obj, idx in scope.canvas.getObjects()
          if scope.canvas.options.isEditingMode
            obj.set("selectable", true) if obj? and obj.type is "i-text" and not obj.isEditing
          else
            obj.set("selectable", false) if obj? and obj.type is "i-text"

        originStyles = JSON.stringify($.extend(true,{},object.get("originStyles")))
        styles = JSON.stringify($.extend(true,{},object.get("styles")))

        if object.getText().trim() is ""
          #beachte object:remove muss i-text abblocken
          if not scope.canvas.options.isEditingMode
            scope.tools.text.blockCreating = true
            scope.$apply()
            scope.canvas.remove(object)
          else
            scope.commandAction = true
            scope.$apply()
            originalState =
              originalState: $.extend(true,{},object.originalState)
              originStyles: JSON.parse(originStyles)
              uid: object.get("uid")
            tmpObject = $.extend(true,{},object)
            scope.canvas.remove(object)
            scope.canvas.trigger("whiteboard:text:removed", {target:tmpObject, origin:originalState})
          return

        if fromDeleteTool
          originalState =
            originalState: $.extend(true,{},object.originalState)
            originStyles: JSON.parse(originStyles)
            uid: object.get("uid")
          scope.canvas.trigger("whiteboard:text:removed", {target:$.extend(true,{},object), origin:originalState})

        if object.getText() isnt object.originalState.text or styles isnt originStyles
          if not scope.canvas.options.isEditingMode
            scope.tools.text.blockCreating = true
            scope.$apply()
            object.set('originStyles', JSON.parse(styles)) if styles isnt originStyles
            scope.canvas.trigger("whiteboard:text:added", {target:object})
          else
            object.set('originStyles', JSON.parse(styles)) if styles isnt originStyles
            scope.$apply()
            originalState =
              originalState: $.extend(true,{},object.originalState)
              originStyles: JSON.parse(originStyles)
            scope.canvas.trigger("whiteboard:text:modified", {target:object, origin: originalState})
      object
    createImage: (scope, options) ->
      fabric.Image.fromURL(options.src, (oImg) ->
        oImg.set(
          top: options.top
          left: options.left
          padding: scope.canvas.options.padding
          transparentCorners: scope.canvas.options.transparentCorners
          borderColor: scope.canvas.options.borderColor
          cornerColor: scope.canvas.options.cornerColor
          selectable: true
          uid: scope.uid++
          slideIndex: scope.slides.slideIndex
        )
        scope.commandAction = true
        scope.canvas.add(oImg)
        scope.canvas.setActiveObject(oImg)
        scope.imageRepositoryOpen()
        scope.$apply()
      )
    drawLine: (object, scope, options) ->
      object.set({
        x2: scope.canvas.getPointer(options.e).x
        y2: scope.canvas.getPointer(options.e).y
      })
    drawCircle: (object, scope, options) ->
      oldLeft = object.get('left')
      oldTop = object.get('top')
      oldRadius = object.get('radius')
      radius = parseInt( Math.sqrt( Math.pow( scope.canvas.getPointer(options.e).x - oldLeft - oldRadius, 2 ) +
                                    Math.pow( scope.canvas.getPointer(options.e).y - oldTop - oldRadius, 2 ) ), 10)
      object.set({
        radius: radius
        left: oldLeft+oldRadius-radius
        top: oldTop+oldRadius-radius
      })
    drawEllipse: (object, scope, options) ->
      originLeft = object.get('originLeft')
      originTop = object.get('originTop')

      rx = scope.canvas.getPointer(options.e).x - originLeft;
      ry = scope.canvas.getPointer(options.e).y - originTop;

      object.set({
        rx: Math.abs(parseInt(rx/2,10))
        ry: Math.abs(parseInt(ry/2,10))
      })
      object.set({
        width: 2*object.get('rx')
        height: 2*object.get('ry')
      })

      if rx > 0
        object.set('left', originLeft)
      if rx <= 0
        object.set('left', originLeft-object.get('width'))
      if ry > 0
        object.set('top', originTop)
      if ry <= 0
        object.set('top', originTop-object.get('height'))
    drawTriangle: (object, scope, options) ->
      originLeft = object.get('originLeft')
      originTop = object.get('originTop')

      x = scope.canvas.getPointer(options.e).x - originLeft;
      y = scope.canvas.getPointer(options.e).y - originTop;

      object.set({
        x: Math.abs(parseInt(x/2,10))
        y: Math.abs(parseInt(y/2,10))
      })
      object.set({
        width: 2*object.get('x')
        height: 2*object.get('y')
      })

      if x > 0
        object.set('left', originLeft)
      if x <= 0
        object.set('left', originLeft-object.get('width'))
      if y > 0
        object.set('top', originTop)
        object.set('flipY', false)
      if y <= 0
        object.set('top', originTop-object.get('height'))
        object.set('flipY', true)
    drawSquare: (object, scope, options) ->
      originLeft = object.get('originLeft')
      originTop = object.get('originTop')

      x = scope.canvas.getPointer(options.e).x - originLeft;

      if scope.canvas.getPointer(options.e).y - originTop < 0
        y = -1 * Math.abs(x)
      else
        y = Math.abs(x)

      object.set({
        x: Math.abs(parseInt(x/2,10))
        y: Math.abs(parseInt(y/2,10))
      })
      object.set({
        width: 2*object.get('x'),
        height: 2*object.get('y')
      })

      if x > 0
        object.set('left', originLeft)
      if x <= 0
        object.set('left', originLeft-object.get('width'))
      if y > 0
        object.set('top', originTop)
      if y <= 0
        object.set('top', originTop-object.get('height'))
    drawRectangle: (object, scope, options) ->
      originLeft = object.get('originLeft')
      originTop = object.get('originTop')

      x = scope.canvas.getPointer(options.e).x - originLeft;
      y = scope.canvas.getPointer(options.e).y - originTop;

      object.set({
        'x': Math.abs(parseInt(x/2,10))
        'y': Math.abs(parseInt(y/2,10))
      })
      object.set({
        'width': 2*object.get('x'),
        'height': 2*object.get('y')
      })

      if x > 0
        object.set('left', originLeft)
      if x <= 0
        object.set('left', originLeft-object.get('width'))
      if y > 0
        object.set('top', originTop)
      if y <= 0
        object.set('top', originTop-object.get('height'))
    editObject: (scope) ->
      obj = scope.canvas.getActiveObject()
      origin = obj.saveState()
      switch obj.type
        when "path"
          obj.set({
            stroke: "#"+scope.tools.pen.color
            strokeWidth: scope.tools.pen.size
          })
        when "line"
          obj.set({
            stroke: "#"+scope.tools.line.color
            strokeWidth: scope.tools.line.size
            strokeDashArray: @convertStrokeStyleToArray(scope.tools.line.style)
          })
        when "rect", "triangle", "circle", "ellipse"
          obj.set({
            stroke: "#"+scope.tools.forms.borderColor
            strokeWidth: scope.tools.forms.borderSize
            strokeDashArray: @convertStrokeStyleToArray(scope.tools.forms.borderStyle)
            fill: "#"+scope.tools.forms.color
          })
      scope.canvas.renderAll()
      scope.canvas.trigger("whiteboard:object:modified", {target: obj, origin: origin})
    editGroupObject: (scope) ->
      group = scope.canvas.getActiveGroup()
      origin = group.saveState()
      targetObjects = []
      originObjects = []
      for obj, idx in group.getObjects()
        targetObjects[idx] = obj
        originObjects[idx] = obj.saveState()
        switch obj.type
          when "path"
            obj.set({
              stroke: "#"+scope.tools.group.line.color
              strokeWidth: scope.tools.group.line.size
            })
          when "line"
            obj.set({
              stroke: "#"+scope.tools.group.line.color
              strokeWidth: scope.tools.group.line.size
              strokeDashArray: @convertStrokeStyleToArray(scope.tools.group.line.style)
            })
          when "rect", "triangle", "circle", "ellipse"
            obj.set({
              stroke: "#"+scope.tools.group.line.color
              strokeWidth: scope.tools.group.line.size
              strokeDashArray: @convertStrokeStyleToArray(scope.tools.group.line.style)
              fill: "#"+scope.tools.group.forms.color
            })
      scope.canvas.renderAll()
      scope.canvas.trigger("whiteboard:object:modified", {target: group, origin: origin, targetObjects: targetObjects, originObjects: originObjects})
    editTextObject: (scope, type) ->
      obj = scope.canvas.getActiveObject()
      if obj?
        @setTextStyle(obj, scope, type)
        if not obj.isEditing
          originalState =
            originalState: $.extend(true,{},obj.originalState)
            originStyles: $.extend(true,{},obj.get("originStyles"))
          #otherwise it would call event on "edit:exiting"
          scope.canvas.trigger("whiteboard:text:modified", {target: obj, origin: originalState})
        scope.canvas.renderAll()
      else
        group = scope.canvas.getActiveGroup()
        if group?
          origin = group.saveState()
          targetObjects = []
          originObjects = []
          for object, idx in group.getObjects()
            targetObjects[idx] = object
            originObjects[idx] = object.saveState()
            if object.type is "i-text"
              @setTextStyle(object, scope, type)
          if targetObjects.length > 0
            scope.canvas.trigger("whiteboard:object:modified", {target: group, origin: origin, targetObjects: targetObjects, originObjects: originObjects})
            scope.canvas.renderAll()
    setTextStyle: (obj, scope, type) ->
      if obj.isEditing is false
        styles = obj.get('styles')
        switch type
          when "bold"
            fontWeight = "normal"
            fontWeight = "bold" if scope.tools.text.bold
            for keyI, line of styles
              for keyJ, styleObj of line
                if styleObj.hasOwnProperty('fontWeight')
                  styleObj.fontWeight = fontWeight
            obj.set('fontWeight', fontWeight)
          when "italic"
            fontStyle = "normal"
            fontStyle = "italic" if scope.tools.text.italic
            for keyI, line of styles
              for keyJ, styleObj of line
                if styleObj.hasOwnProperty('fontStyle')
                  styleObj.fontStyle = fontStyle
            obj.set('fontStyle', fontStyle)
          when "underline"
            textDecoration = "normal"
            textDecoration = "underline" if scope.tools.text.underline
            for keyI, line of styles
              for keyJ, styleObj of line
                if styleObj.hasOwnProperty('textDecoration')
                  styleObj.textDecoration = textDecoration
            obj.set('textDecoration', textDecoration)
          when "leftAlign"
            textAlign = "left"
            for keyI, line of styles
              for keyJ, styleObj of line
                if styleObj.hasOwnProperty('textAlign')
                  styleObj.textAlign = textAlign
            obj.set('textAlign', textAlign)
          when "rightAlign"
            textAlign = "left"
            textAlign = "right" if scope.tools.text.rightAlign
            for keyI, line of styles
              for keyJ, styleObj of line
                if styleObj.hasOwnProperty('textAlign')
                  styleObj.textAlign = textAlign
            obj.set('textAlign', textAlign)
          when "centerAlign"
            textAlign = "left"
            textAlign = "center" if scope.tools.text.centerAlign
            for keyI, line of styles
              for keyJ, styleObj of line
                if styleObj.hasOwnProperty('textAlign')
                  styleObj.textAlign = textAlign
            obj.set('textAlign', textAlign)
          when "fontSize"
            fontSize = scope.tools.text.fontSize
            for keyI, line of styles
              for keyJ, styleObj of line
                if styleObj.hasOwnProperty('fontSize')
                  styleObj.fontSize = fontSize
            obj.set('fontSize', fontSize)
          when "fontFamily"
            fontFamily = @convertFontFamilyKeyToName(scope.tools.text.fontFamily)
            for keyI, line of styles
              for keyJ, styleObj of line
                if styleObj.hasOwnProperty('fontFamily')
                  styleObj.fontFamily = fontFamily
            obj.set('fontFamily', fontFamily)
          when "color"
            fill = "#" + scope.tools.text.color
            for keyI, line of styles
              for keyJ, styleObj of line
                if styleObj.hasOwnProperty('fill')
                  styleObj.fill = fill
            obj.set('fill', fill)

        obj.set('styles', styles)
      else
        if obj.setSelectionStyles and obj.isEditing
          style = {}
          switch type
            when "bold"
              fontWeight = "normal"
              fontWeight = "bold" if scope.tools.text.bold
              style.fontWeight = fontWeight
            when "italic"
              fontStyle = "normal"
              fontStyle = "italic" if scope.tools.text.italic
              style.fontStyle = fontStyle
            when "underline"
              textDecoration = "normal"
              textDecoration = "underline" if scope.tools.text.underline
              style.textDecoration = textDecoration
            when "leftAlign"
              textAlign = "left"
              style.textAlign = textAlign
            when "centerAlign"
              textAlign = "left"
              textAlign = "center" if scope.tools.text.centerAlign
              style.textAlign = textAlign
            when "rightAlign"
              textAlign = "left"
              textAlign = "right" if scope.tools.text.rightAlign
              style.textAlign = textAlign
            when "fontSize"
              fontSize = scope.tools.text.fontSize
              style.fontSize = fontSize
            when "fontFamily"
              fontFamily = @convertFontFamilyKeyToName(scope.tools.text.fontFamily)
              style.fontFamily = fontFamily
            when "color"
              fill = "#" + scope.tools.text.color
              style.fill = fill
          obj.setSelectionStyles(style)
    getObjectFromUID: (scope, uid) ->
      object = {}
      scope.canvas.forEachObject( (o, i) ->
        object = o if o.get("uid") is uid
      )
      return object
    deleteObject: (scope, object, trigger) ->
      trigger = false if not trigger?
      # hack due to fabric js bug
      if object.type is "i-text"
        if trigger
          if object.isEditing
            object.trigger("editing:exited")
          else
            scope.commandAction = true
            scope.$apply()
            tmpObject = $.extend(true,{},object)
            originalState =
              originalState: tmpObject.originalState
              originStyles: tmpObject.get("originStyles")
              uid: tmpObject.get("uid")
            object.remove()
            object.setCoords()
            scope.canvas.remove(object)
            scope.canvas.trigger("whiteboard:text:removed", {target: tmpObject, origin: originalState})
        else
          object.remove()
          object.setCoords()
          scope.canvas.remove(object)
      else
        if trigger
          scope.commandAction = true
        object.remove()
    deleteActiveObject: (scope) ->
      group = scope.canvas.getActiveGroup()
      if group?
        targetObjects = []
        scope.tools.group.removeIsActive = true
        for object, idx in group.getObjects()
          targetObjects[idx] = object
          scope.canvas.discardActiveObject(object)
          scope.commandAction = true
          scope.canvas.remove(object)
        scope.canvas.discardActiveGroup().renderAll()
        scope.commandAction = true
        scope.canvas.trigger("whiteboard:group:removed", {target: group, targetObjects: targetObjects})
      if scope.canvas.getActiveObject()?
        object = scope.canvas.getActiveObject()
        scope.canvas.discardActiveObject(object)
        @deleteObject(scope, object, true)
    layerUpObject: (scope) ->
      obj = scope.canvas.getActiveObject()
      obj.bringForward()
      scope.canvas.trigger("whiteboard:layer:up", {target: obj})
    layerDownObject: (scope) ->
      obj = scope.canvas.getActiveObject()
      obj.sendBackwards()
      scope.canvas.trigger("whiteboard:layer:down", {target: obj})
    convertStrokeStyleToArray: (style) ->
      switch style
        when "dashed"
          strokeStyle = [5, 2]
        when "dotted"
          strokeStyle = [1, 2]
        else
          strokeStyle = null
      strokeStyle
    convertFontFamilyKeyToName: (fontFamilyKey) ->
      name = "Times New Roman"
      switch fontFamilyKey
        when "arial"
          name = "Arial"
        when "verdana"
          name = "Verdana"
        when "comicsans"
          name = "Comic Sans MS"
        else
          name = "Times New Roman"
      name
    createFontStyleFromTextModel: (scope) ->
      textAlign = "left"
      textAlign = "center" if scope.tools.text.centerAlign
      textAlign = "right" if scope.tools.text.rightAlign
      fontWeight = "normal"
      fontWeight = "bold" if scope.tools.text.bold
      fontStyle = "normal"
      fontStyle = "italic" if scope.tools.text.italic
      textDecoration = "normal"
      textDecoration = "underline" if scope.tools.text.underline
      style =
        textAlign: textAlign
        fontWeight: fontWeight
        fontStyle: fontStyle
        textDecoration: textDecoration
        fontFamily: @convertFontFamilyKeyToName(scope.tools.text.fontFamily)
        fontSize: scope.tools.text.fontSize
        fill: "#"+scope.tools.text.color
      style
  }
)