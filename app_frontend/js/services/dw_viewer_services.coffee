'use strict'

angular.module('digitalWhiteboardApp.viewer_services', []).factory("ViewerService", ["WhiteboardService", "SlidesService", (WhiteboardService, SlidesService) ->
  return {
    setMessageList: (scope, messages) ->
      convertedMessages = []
      for message in messages
        convertedMessage = message.split("rh6Ee0tTh5")
        msgObj =
          state: convertedMessage[0].trim()
          text: decodeURI(convertedMessage[1].trim())
        convertedMessages.push(msgObj)
      scope.messages = convertedMessages
      scope.$apply()
      $('#messages-list').animate({
        scrollTop: $('#messages-list').get(0).scrollHeight
      }, 0);

    addMessageToList: (scope, message) ->
      convertedMessage = message.split("rh6Ee0tTh5")
      msgObj =
        state: convertedMessage[0].trim()
        text: decodeURI(convertedMessage[1].trim())
      if scope.messages.length > 50
        scope.messages.splice(0, 1)
      scope.messages.push(msgObj)
      scope.$apply()
      $('#messages-list').animate({
        scrollTop: $('#messages-list').get(0).scrollHeight
      }, 500);

    setUserList: (scope, usersCount, users) ->
      scope.countConnectedUsers = usersCount
      scope.connectedUsers = users
      scope.$apply()

    addUserToList: (scope, data) ->
      scope.countConnectedUsers = data.usersCount
      user =
        userID: data.userID
        firstName: data.firstName
        lastName: data.lastName
        presenter: data.presenter

      check = false
      for connUser in scope.connectedUsers
        if connUser.userID is user.userID
          check = true

      if not check
        scope.connectedUsers.push(user)
        scope.$apply()

    removeUserFromList: (scope, data) ->
        scope.countConnectedUsers = data.usersCount
        index = -1
        for connUser, idx in scope.connectedUsers
          if connUser.userID is data.userID
            index = idx

        if index > -1
          scope.connectedUsers.splice(index, 1)
          scope.$apply()

    setSlides: (scope, content) ->
      SlidesService.setSlidesForViewer(scope, content)

    showSlide: (scope, slideIndex) ->
      SlidesService.showSlideForViewer(scope, slideIndex)

    doCommand: (scope, command) ->
      command = JSON.parse(command)
      switch command.command
        when "add-slide"
          scope.canvas.clear()
          content = {json: JSON.stringify(scope.canvas.toDatalessJSON(scope.canvas.options.propertiesToInclude))}
          scope.slides.content.splice(command.properties.slideIndex, 0, content)
        when "re-add-slide"
          scope.canvas.clear()
          scope.canvas.loadFromJSON(command.properties.content, () ->
            content =
              json: JSON.stringify(scope.canvas.toDatalessJSON(scope.canvas.options.propertiesToInclude))
            scope.slides.content.splice(command.properties.slideIndex, 0, content)
          )
        when "remove-slide"
          scope.canvas.clear()
          scope.slides.content.splice(command.properties.slideIndex, 1)
          if scope.slides.slideIndex is 0 and command.properties.slideIndex is 0
            @showSlide(scope, scope.slides.slideIndex)
          else if scope.slides.slideIndex isnt (command.properties.slideIndex - 1)
            scope.slides.slideIndex = command.properties.slideIndex - 1
            if scope.slides.slideIndex < 0
              scope.slides.slideIndex = 0
            @showSlide(scope, scope.slides.slideIndex)
        when "move-slide"
          tmpItem = scope.slides.content[command.properties.from]
          scope.slides.content.splice(command.properties.from, 1)
          scope.slides.content.splice(command.properties.to, 0, tmpItem)
          @showSlide(scope, command.properties.to)
        else
          if scope.slides.slideIndex is command.properties.slideIndex
            @exec(scope, command)
            scope.slides.content[scope.slides.slideIndex].json = JSON.stringify(scope.canvas.toDatalessJSON(scope.canvas.options.propertiesToInclude))
          else
            scope.slides.slideIndex = command.properties.slideIndex
            scope.canvas.clear()
            that = this
            scope.canvas.loadFromJSON(JSON.parse(scope.slides.content[command.properties.slideIndex].json), () ->
              that.exec(scope,command)
              scope.slides.content[scope.slides.slideIndex].json = JSON.stringify(scope.canvas.toDatalessJSON(scope.canvas.options.propertiesToInclude))
              scope.$apply()
            )
      return

    exec: (scope, command) ->
      scope.commandAction = false
      properties = command.properties

      if properties.uid?
        object = WhiteboardService.getObjectFromUID(scope, properties.uid)

      switch command.command
        when "add"
          @add(scope, command)
        when "modify"
          if not _.isEmpty(object)
            scope.canvas.discardActiveGroup() if scope.canvas.getActiveGroup()?.getObjects().indexOf(object) > -1
            if object.type is "circle"
              object.setRadius(properties.new.width/2)
            if object.type is "ellipse"
              object.set("rx", properties.new.width/2)
              object.set("ry", properties.new.height/2)
            object.set(properties.new)
            object.setCoords()
        when "group-modify"
          @groupModify(scope, properties.groupObjectUIDs, properties)
        when "group-wb-modify"
          @groupModify(scope, properties.groupObjectUIDs, properties, true)
        when "group-remove"
          @groupRemove(scope, properties.groupObjectUIDs)
        when "group-add"
          @groupAdd(scope, command)
        when "layer-down"
          if not _.isEmpty(object)
            scope.canvas.discardActiveGroup() if scope.canvas.getActiveGroup()?.getObjects().indexOf(object) > -1
            object.sendBackwards()
        when "layer-up"
          if not _.isEmpty(object)
            scope.canvas.discardActiveGroup() if scope.canvas.getActiveGroup()?.getObjects().indexOf(object) > -1
            object.bringForward()
        when "remove"
          if not _.isEmpty(object)
            WhiteboardService.deleteObject(scope, object)

      scope.canvas.renderAll()

    add: (scope, command) ->
      object = {}
      properties = command.properties
      switch command.type
        when "path"
          object = new fabric.Path(properties.path)
        when "line"
          object = new fabric.Line()
        when "circle"
          object = new fabric.Circle()
        when "ellipse"
          object = new fabric.Ellipse()
        when "rect"
          object = new fabric.Rect()
          object.setHeight(properties.height)
          object.setWidth(properties.width)
        when "triangle"
          object = new fabric.Triangle()
        when "image"
          fabric.Image.fromURL(properties.src, (oImg) ->
            oImg.set(properties)
            oImg.setCoords()
            scope.canvas.add(oImg).renderAll()
            scope.slides.content[scope.slides.slideIndex].json = JSON.stringify(scope.canvas.toDatalessJSON(scope.canvas.options.propertiesToInclude))
            scope.$apply()
          )
        when "i-text"
          object = new fabric.IText(properties.text)
          object.set("originStyles", properties.originStyles)
      if not _.isEmpty(object)
        object.set(properties)
        #hack because line cannot handle coordinates...
        object.setLeft(properties.left)
        object.setTop(properties.top)
        object.setCoords()
        scope.canvas.add(object).renderAll()

    groupAdd: (scope, command) ->
      for objectToAdd in command.properties.groupObjectProperties
        @add(scope, objectToAdd)

    groupModify: (scope, groupObjectUIDs, properties, modify) ->
      objectsToGroup = []
      for objectUID in groupObjectUIDs
        object = WhiteboardService.getObjectFromUID(scope, objectUID)
        objectsToGroup.push(object) if not _.isEmpty(object)

      if modify?
        for objectToModify,idx in objectsToGroup
          objectToModify.set(properties.groupObjectProperties[idx].new)
          objectToModify.setCoords()

      top = properties.new.top
      left = properties.new.left
      angle = properties.new.angle

      groupTemp = new fabric.Group(objectsToGroup,{
        originX: properties.originX
        originY: properties.originY
        top: top
        left: left
        angle: angle
      })

      groupTemp.setCoords()
      groupTemp._restoreObjectsState()
      scope.canvas.remove(groupTemp)
      group = new fabric.Group(objectsToGroup,{
        originX: properties.originX
        originY: properties.originY
      })

      group.setCoords()
      group._restoreObjectsState()
      scope.canvas.remove(group)

    groupRemove: (scope, groupObjectUIDs) ->
      objectsToRemove = []
      for objectUID in groupObjectUIDs
        object = WhiteboardService.getObjectFromUID(scope, objectUID)
        objectsToRemove.push(object) if not _.isEmpty(object)

      for objectToRemove in objectsToRemove
        WhiteboardService.deleteObject(scope, objectToRemove)
  }
])