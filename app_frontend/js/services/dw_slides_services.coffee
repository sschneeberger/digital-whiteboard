'use strict'

angular.module('digitalWhiteboardApp.slides_services', []).factory("SlidesService", ["WhiteboardService", (WhiteboardService) ->
  return {
    getSlides: (scope) ->
      result = []
      scope.canvas.clear()
      for slide, idx in scope.slides.content
        scope.canvas.loadFromJSON(JSON.parse(slide.json), () ->
          data = scope.canvas.getElement().toDataURL('image/png', 0.3);
          base64 = data.replace("data:image/png;base64,","");
          slideContent =
            content: slide.json
            base64: base64
          result[idx] = slideContent
        )
      result

    renderSlide: (scope) ->
      scope.canvas.renderAll()
      for obj, idx in scope.canvas.getObjects()
        if obj.type is "i-text"
          obj = WhiteboardService.createTextEventListener(scope, obj)
      if not scope.canvas.options.isEditingMode
        WhiteboardService.deactivateObjects(scope)
        if not ( scope.tools.pen.selected or scope.tools.forms.selected or scope.tools.text.selected or scope.tools.line.selected)
          scope.canvas.defaultCursor = 'default'
        WhiteboardService.activateDrawingMode(scope) if scope.tools.pen.selected
      if scope.tools.cursor
        WhiteboardService.activateObjects(scope)

    setSlides: (scope, content) ->
      slides = JSON.parse(content)
      scope.slides.content = []
      if scope.connectionEstablished is 1
        scope.slides.slideIndex = 0
      for slide, idx in slides.contents
        newImg = document.createElement("img")
        newImg.src = "data:image/jpeg;base64," + slides.images[idx]
        newImg.width = 160
        newImg.height = 100
        content =
          json: slide
          img: $(newImg).prop('outerHTML')
        scope.slides.content[idx] = content

      if scope.slides.content.length > 1
        scope.tools.removeSlide = true

      @showSlide(scope, scope.slides.slideIndex)

    setSlidesForViewer: (scope, content) ->
      slides = JSON.parse(content)
      scope.slides.content = []
      for slide, idx in slides.contents
        content = json: slide
        scope.slides.content[idx] = content

      scope.canvas.loadFromJSON(JSON.parse(scope.slides.content[scope.slides.slideIndex].json))

    showSlide: (scope, slideIndex) ->
      @setBackAndNext(scope)
      scope.canvas.clear()
      that = this
      scope.canvas.loadFromJSON(JSON.parse(scope.slides.content[slideIndex].json), () ->
        that.renderSlide(scope)
        scope.$apply()
      )

    showSlideForViewer: (scope, slideIndex) ->
      scope.canvas.clear()
      scope.canvas.loadFromJSON(JSON.parse(scope.slides.content[slideIndex].json))

    setBackAndNext: (scope) ->
      if scope.slides.slideIndex is 0
        scope.tools.back = false
      else
        scope.tools.back = true
      if scope.slides.slideIndex is scope.slides.content.length - 1
        scope.tools.next = false
      else
        scope.tools.next = true

    getBase64ImageOfActiveSlide: (scope) ->
      groupObjects = []
      object = null
      originX = ""
      originY = ""

      if scope.canvas.getActiveObject()?
        object = scope.canvas.getActiveObject()
      if scope.canvas.getActiveGroup()?
        originX = scope.canvas.getActiveGroup().get("originX")
        originY = scope.canvas.getActiveGroup().get("originY")
        for object in scope.canvas.getActiveGroup().getObjects()
          groupObjects.push(object)
      scope.canvas.deactivateAll().renderAll()
      data = scope.canvas.getElement().toDataURL('image/png', 0.3);
      base64 = data.replace("data:image/png;base64,","");
      if object?
        scope.canvas.setActiveObject(object)
        scope.canvas.renderAll()
      if groupObjects.length > 0
        group = new fabric.Group(groupObjects,{
          originX: originX
          originY: originY
        })
        for groupObject in groupObjects
          groupObject.set('active', true)
        scope.canvas._activeObject = null
        scope.canvas.setActiveGroup(group.setCoords()).renderAll()
      base64
  }
])