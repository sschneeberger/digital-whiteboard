'use strict';

# App Module
angular.module 'digitalWhiteboardApp', ['ngSanitize',
                                        'ngAnimate',
                                        'ui.sortable',
                                        'digitalWhiteboardApp.presenter_controller',
                                        'digitalWhiteboardApp.viewer_controller',
                                        'digitalWhiteboardApp.whiteboard_directives',
                                        'digitalWhiteboardApp.toolbar_directives',
                                        'digitalWhiteboardApp.dashboard_directives',
                                        'digitalWhiteboardApp.toolbar_services',
                                        'digitalWhiteboardApp.whiteboard_services',
                                        'digitalWhiteboardApp.command_services',
                                        'digitalWhiteboardApp.slide_services',
                                        'digitalWhiteboardApp.slides_services',
                                        'digitalWhiteboardApp.socket_services',
                                        'digitalWhiteboardApp.status_services',
                                        'digitalWhiteboardApp.viewer_services',
                                        'digitalWhiteboardApp.webrtc_services',
                                        'digitalWhiteboardApp.properties_services']

angular.module('digitalWhiteboardApp').config ($interpolateProvider) ->
  $interpolateProvider.startSymbol("{[").endSymbol "]}"

angular.module('digitalWhiteboardApp').filter('to_trusted', ['$sce', ($sce) ->
  return (text) ->
    return $sce.trustAsHtml(text)

])




