# customise fabric.Object with a method to resize rather than just scale after tranformation
fabric.Object::resizeToScale = ->

  # resizes an object that has been scaled (e.g. by manipulating the handles), setting scale to 1 and recalculating bounding box where necessary
  switch @type
    when "circle"
      @setRadius(@radius * @scaleX)
      @scaleX = 1
      @scaleY = 1
    when "ellipse"
      @rx *= @scaleX
      @ry *= @scaleY
      @width = @rx * 2
      @height = @ry * 2
      @scaleX = 1
      @scaleY = 1
    when "polygon", "polyline"
      points = @get("points")
      i = 0
      while i < points.length
        p = points[i]
      p.x *= @scaleX
      p.y *= @scaleY
      i++
      @scaleX = 1
      @scaleY = 1
      @width = @getBoundingBox().width
      @height = @getBoundingBox().height
    when "triangle", "line", "rect"
      @width = parseInt(@width * @scaleX,10)
      @height = parseInt(@height * @scaleY,10)
      @scaleX = 1
      @scaleY = 1
    else
      return


# helper function to return the boundaries of a polygon/polyline
# something similar may be built in but it seemed easier to write my own than dig through the fabric.js code.  This may make me a bad person.
fabric.Object::getBoundingBox = ->
  minX = null
  minY = null
  maxX = null
  maxY = null
  switch @type
    when "polygon", "polyline"
      points = @get("points")
      i = 0

      while i < points.length
        if typeof (minX) is `undefined`
          minX = points[i].x
        else minX = points[i].x  if points[i].x < minX
        if typeof (minY) is `undefined`
          minY = points[i].y
        else minY = points[i].y  if points[i].y < minY
        if typeof (maxX) is `undefined`
          maxX = points[i].x
        else maxX = points[i].x  if points[i].x > maxX
        if typeof (maxY) is `undefined`
          maxY = points[i].y
        else maxY = points[i].y  if points[i].y > maxY
        i++
    else
      minX = @left
      minY = @top
      maxX = @left + @width
      maxY = @top + @height
  topLeft: new fabric.Point(minX, minY)
  bottomRight: new fabric.Point(maxX, maxY)
  width: maxX - minX
  height: maxY - minY


fabric.Object::mouseEvent = (type, sx, sy, cx, cy) ->
  evt = undefined
  e =
    bubbles: true
    cancelable: (type isnt "mousemove")
    view: window
    detail: 0
    screenX: sx
    screenY: sy
    clientX: cx
    clientY: cy
    ctrlKey: false
    altKey: false
    shiftKey: false
    metaKey: false
    button: 0
    relatedTarget: `undefined`

  if typeof (document.createEvent) is "function"
    evt = document.createEvent("MouseEvents")
    evt.initMouseEvent type, e.bubbles, e.cancelable, e.view, e.detail, e.screenX, e.screenY, e.clientX, e.clientY, e.ctrlKey, e.altKey, e.shiftKey, e.metaKey, e.button, document.body.parentNode
  else if document.createEventObject
    evt = document.createEventObject()
    for prop of e
      evt[prop] = e[prop]
    evt.button = { 0:1, 1:4, 2:2 }[evt.button] || evt.button
  evt

fabric.Object::dispatchEvent = (el, evt) ->
  if el.dispatchEvent
    el.dispatchEvent evt
  else el.fireEvent "on" + type, evt  if el.fireEvent
  evt