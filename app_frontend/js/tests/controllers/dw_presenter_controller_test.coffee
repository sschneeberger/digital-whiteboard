"use strict"
describe "digitalWhiteboardControllerPresenter", ->
  scope = undefined #we'll use this scope in our tests

  window.AppConfig =
    webSocketUrl: "ws://192.168.50.100"
    webSocketImageRepositoryPort: 8081
    webSocketPresentationPort: 8082
    webSocketWebRTCPort: 8083
    iceServer: "192.168.50.100:3478"
    resiprocatePassword: 1234
    resiprocateUsername: "test"
    authenticationRealm: "return-digital-whiteboard"
    enableWebRTCLogs: true
  window.AppGlobals =
    username: "admin"
    userID: 1
    isPresentator: true

  #mock Application to allow us to inject our own dependencies
  beforeEach angular.mock.module("digitalWhiteboardApp.presenter_controller")
  beforeEach angular.mock.module("digitalWhiteboardApp.command_services")
  beforeEach angular.mock.module("digitalWhiteboardApp.toolbar_services")
  beforeEach angular.mock.module("digitalWhiteboardApp.whiteboard_services")
  beforeEach angular.mock.module("digitalWhiteboardApp.slide_services")
  beforeEach angular.mock.module("digitalWhiteboardApp.socket_services")
  beforeEach angular.mock.module("digitalWhiteboardApp.slides_services")
  beforeEach angular.mock.module("digitalWhiteboardApp.webrtc_services")
  beforeEach angular.mock.module("digitalWhiteboardApp.viewer_services")
  beforeEach angular.mock.module("digitalWhiteboardApp.status_services")

  #mock the controller for the same reason and include $rootScope and $controller
  beforeEach angular.mock.inject(($rootScope, $controller) ->

    #create an empty scope
    scope = $rootScope.$new()

    #declare the controller and inject our empty scope
    $controller "digitalWhiteboardControllerPresenter",
      $scope: scope

    return
  )

  it "should have scope variables set correctly", ->
    expect(scope.imageRepository).toBe false
    expect(scope.canvas.isDrawingMode).toBe false
    expect(scope.canvas.freeDrawingBrush).toBe null
    expect(scope.tmpImageRepository).toBe false
    expect(scope.tools.back).toBe false
    expect(scope.tools.next).toBe false
    expect(scope.recordTime).toBe 0
    return

  return