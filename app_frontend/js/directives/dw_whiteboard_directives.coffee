'use strict'

angular.module('digitalWhiteboardApp.whiteboard_directives', [])

  .directive("dwCanvas", [ 'ToolbarService', 'WhiteboardService', 'CommandService', 'PropertiesService', (ToolbarService, WhiteboardService, CommandService, PropertiesService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        object = false

        scope.canvas.on "path:created", (e) ->
          console.log("path:created")
          scope.commandAction = true
          e.path.set({
            padding: scope.canvas.options.padding
            transparentCorners: scope.canvas.options.transparentCorners
            borderColor: scope.canvas.options.borderColor
            cornerColor: scope.canvas.options.cornerColor
            uid: scope.uid++
            slideIndex: scope.slides.slideIndex
          })
          scope.canvas.trigger("object:added", {target: e.path})
          return

        scope.canvas.on "object:added", (e) ->
          if scope.commandAction
            console.log("object:added")
            scope.commandAction = false
            object = e.target
            properties = PropertiesService.getSharedProperties(scope, object)
            properties = PropertiesService.getPropertiesForObjectAdded(object, properties)
            CommandService.createCommand(scope, object, "add", properties)
            scope.$apply() if not scope.$$phase
          return

        scope.canvas.on "object:modified", (e) ->
          if scope.commandAction
            console.log("object:modified")
            e.target.resizeToScale()
            properties = PropertiesService.getPropertiesForObjectModified(scope, e.target)
            if e.target.type is "group"
              properties.groupObjectUIDs = []
              scope.canvas.getActiveGroup().forEachObject( (object) ->
                properties.groupObjectUIDs.push(object.get("uid"))
                properties.slideIndex = scope.slides.slideIndex
              )
              CommandService.createCommand(scope, e.target, "group-modify", properties)
            else
              CommandService.createCommand(scope, e.target, "modify", properties)
            scope.$apply() if not scope.$$phase
          return

        scope.canvas.on "whiteboard:text:added", (e) ->
          console.log("whiteboard:text:added")
          properties = PropertiesService.getSharedProperties(scope, e.target)
          properties = PropertiesService.getPropertiesForTextAdded(e.target, properties)
          CommandService.createCommand(scope, e.target, "add", properties)
          scope.$apply() if not scope.$$phase
          return

        scope.canvas.on "whiteboard:object:modified", (e) ->
          if scope.commandAction
            console.log("whiteboard:object:modified")
            if e.target.type is "group"
              properties = PropertiesService.getPropertiesForObjectModified(scope, e.target)
              properties.groupObjectUIDs = []
              properties.groupObjectProperties = []
              for object, idx in e.target.getObjects()
                properties.groupObjectUIDs.push(object.get("uid"))
                properties.slideIndex = scope.slides.slideIndex
                if object.type is "i-text"
                  properties.groupObjectProperties.push(PropertiesService.getPropertiesForTextModified(scope, e.targetObjects[idx], e.originObjects[idx], true))
                else
                  properties.groupObjectProperties.push(PropertiesService.getPropertiesForWhiteboardObjectModified(scope, e.targetObjects[idx], e.originObjects[idx], true))
              CommandService.createCommand(scope, e.target, "group-wb-modify", properties)
            else
              properties = PropertiesService.getPropertiesForWhiteboardObjectModified(scope, e.target, e.origin, false)
              CommandService.createCommand(scope, e.target, "modify", properties)
            scope.$apply() if not scope.$$phase
          return

        scope.canvas.on "whiteboard:text:modified", (e) ->
          console.log("whiteboard:text:modified")
          properties = PropertiesService.getPropertiesForTextModified(scope, e.target, e.origin)
          CommandService.createCommand(scope, e.target, "modify", properties)
          scope.$apply() if not scope.$$phase
          return

        scope.canvas.on "whiteboard:text:removed", (e) ->
          if scope.commandAction
            #scope.commandAction = false
            console.log("whiteboard:text:removed")
            properties = PropertiesService.getSharedProperties(scope, e.target)
            properties = PropertiesService.getPropertiesForTextRemoved(e.origin, properties)
            CommandService.createCommand(scope, e.target, "remove", properties)
            scope.$apply() if not scope.$$phase
          return

        scope.canvas.on "object:removed", (e) ->
          if e.target.type is "i-text"
            return
          if scope.commandAction and not scope.tools.group.removeIsActive
            console.log("object:removed")
            properties = PropertiesService.getSharedProperties(scope, e.target)
            properties = PropertiesService.getPropertiesForObjectAdded(e.target, properties)
            CommandService.createCommand(scope, e.target, "remove", properties)
            scope.$apply() if not scope.$$phase
          return

        scope.canvas.on "whiteboard:group:removed", (e) ->
          if scope.commandAction
            console.log("whiteboard:group:removed")
            properties =
              groupObjectUIDs: []
              groupObjectProperties: []
            for object, idx in e.target.getObjects()
              properties.groupObjectUIDs.push(object.get("uid"))
              properties.slideIndex = scope.slides.slideIndex
              tmpProperties = PropertiesService.getSharedProperties(scope, e.targetObjects[idx])
              if object.type is "i-text"
                tmpProperties = PropertiesService.getPropertiesForTextAdded(e.targetObjects[idx], tmpProperties)
                properties.groupObjectProperties.push({type: object.type, properties: tmpProperties})
              else
                tmpProperties = PropertiesService.getPropertiesForObjectAdded(e.targetObjects[idx], tmpProperties)
                properties.groupObjectProperties.push({type: object.type, properties: tmpProperties})
            CommandService.createCommand(scope, e.target, "group-remove", properties)
            scope.tools.group.removeIsActive = false
            scope.$apply() if not scope.$$phase
          return

        scope.canvas.on "whiteboard:layer:up", (e) ->
          if scope.commandAction
            console.log("whiteboard:layer:up")
            properties =
              uid: e.target.get("uid")
              slideIndex: scope.slides.slideIndex
            CommandService.createCommand(scope, e.target, "layer-up", properties)
            scope.$apply() if not scope.$$phase
          return

        scope.canvas.on "whiteboard:layer:down", (e) ->
          if scope.commandAction
            console.log("whiteboard:layer:down")
            properties =
              uid: e.target.get("uid")
              slideIndex: scope.slides.slideIndex
            CommandService.createCommand(scope, e.target, "layer-down", properties)
            scope.$apply() if not scope.$$phase
          return

        scope.canvas.on "object:selected", (e) ->
          if e.target.type is "i-text" and scope.tools.text.isCreating
            return
          console.log("obj:selected")
          ToolbarService.deselectEditingTools(scope)
          ToolbarService.deselectSidebar(scope)
          ToolbarService.selectActionTools(scope)
          scope.commandAction = true
          switch e.target.type
            when "path"
              ToolbarService.openPenEditingMode(e.target, scope)
            when "line"
              ToolbarService.openLineEditingMode(e.target, scope)
            when "rect", "triangle", "circle", "ellipse"
              ToolbarService.openFormsEditingMode(e.target, scope)
            when "i-text"
              ToolbarService.openTextEditingMode(e.target, scope)
            when "image"
              ToolbarService.openImageEditingMode(e.target, scope)
            when "group"
              console.log("select group")
              ToolbarService.deselectLayerTools(scope)
              ToolbarService.openGroupEditingMode(e.target, scope)
          scope.$apply() if not scope.$$phase
          return

        scope.canvas.on 'mouse:down', (e) ->
          object = false
          ToolbarService.deselectSidebar(scope)
          if not scope.canvas.options.isEditingMode
            if scope.tools.line.selected
              object = WhiteboardService.createLine(scope, e)
              scope.canvas.add(object)
            if scope.tools.forms.selected
              switch scope.tools.forms.form.form
                when "circle"
                  object = WhiteboardService.createCircle(scope, e)
                  scope.canvas.add(object)
                when "ellipse"
                  object = WhiteboardService.createEllipse(scope, e)
                  scope.canvas.add(object)
                when "triangle"
                  object = WhiteboardService.createTriangle(scope, e)
                  scope.canvas.add(object)
                when "square"
                  object = WhiteboardService.createSquare(scope, e)
                  scope.canvas.add(object)
                else
                  object = WhiteboardService.createRectangle(scope, e)
                  scope.canvas.add(object)
            if scope.tools.text.selected
              if not scope.tools.text.isCreating and not scope.tools.text.blockCreating
                scope.tools.text.isCreating = true
                scope.tools.commandAction = false
                text = WhiteboardService.createText(scope, e)
                scope.canvas.add(text).setActiveObject(text)
                text.enterEditing()
              if scope.tools.text.blockCreating
                scope.tools.text.isCreating = false
                scope.tools.text.blockCreating = false
            scope.$apply() if not scope.$$phase
          else
            setTimeout( () ->
              if scope.tools.text.selected and scope.canvas.getActiveObject()? and scope.canvas.getActiveObject().isEditing
                ToolbarService.openTextEditingMode(scope.canvas.getActiveObject(), scope)
                scope.$apply()
            , 50)
            if scope.canvas.getActiveObject() is null and scope.canvas.getActiveGroup() is null
              ToolbarService.deselectEditingTools(scope)
              scope.$apply()
          return

        scope.canvas.on 'mouse:move', (e) ->
          if not scope.canvas.options.isEditingMode and object
            if scope.tools.line.selected
              WhiteboardService.drawLine(object, scope, e)
            if scope.tools.forms.selected
              switch scope.tools.forms.form.form
                when "circle"
                  WhiteboardService.drawCircle(object, scope, e)
                when "ellipse"
                  WhiteboardService.drawEllipse(object, scope, e)
                when "triangle"
                  WhiteboardService.drawTriangle(object, scope, e)
                when "square"
                  WhiteboardService.drawSquare(object, scope, e)
                else
                  WhiteboardService.drawRectangle(object, scope, e)
            if scope.tools.text.selected
              return
            object.setCoords()
            scope.canvas.renderAll()
          return

        scope.canvas.on 'mouse:up', (e) ->
          if not scope.canvas.options.isEditingMode and object
            if scope.tools.line.selected or scope.tools.forms.selected
              scope.commandAction = true
              scope.$apply() if not scope.$$phase
              scope.canvas.trigger("object:added", {target:object})
          object = false
          return

        scope.canvas.on 'selection:cleared', (e) ->
          if scope.canvas.options.isEditingMode
            console.log("selection:cleared")
            ToolbarService.deselectEditingTools(scope)
            scope.$apply() if not scope.$$phase

        scope.canvas.on 'slide:added', (e) ->
          console.log("slide:added")
          properties = {slideIndex: e.slideIndex}
          CommandService.createCommand(scope, {type: "slide"}, "add-slide", properties)
          scope.$apply() if not scope.$$phase
          return

        scope.canvas.on 'slide:removed', (e) ->
          console.log("slide:removed")
          properties =
            slideIndex: e.slideIndex
            content: scope.slides.content[scope.slides.slideIndex].json
          CommandService.createCommand(scope, {type: "slide"}, "remove-slide", properties)
          scope.$apply() if not scope.$$phase
          return

        scope.canvas.on 'slide:moved', (e) ->
          console.log("slide:moved")
          CommandService.createCommand(scope, {type: "slide"}, "move-slide", e)
          scope.$apply() if not scope.$$phase
          return
    }
  ])
