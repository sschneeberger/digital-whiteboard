'use strict'

angular.module('digitalWhiteboardApp.dashboard_directives', [])

  .directive("dwDeletePresentation", () ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( (e) ->
          e.stopPropagation();
          text = attributes.dwDataQuestion
          r = confirm(text);
          if (r == true)
            document.location.href = attributes.dwDataUrl
        )
    }
  )