'use strict'

angular.module('digitalWhiteboardApp.toolbar_directives', [])

  .directive('integer', () ->
    return {
      require: 'ngModel',
      link: (scope, ele, attr, ctrl) ->
        ctrl.$parsers.unshift( (viewValue) ->
          parseInt(viewValue, 10)
        )
    }
  )

  .directive("dwZoomIn", () ->
    return {
    restrict: "A"
    link: (scope, element, attributes) ->
      $(element).click( () ->
        zoom = scope.canvas.getZoom()
        scope.canvas.setZoom(zoom+0.1)
        scope.canvas.calcOffset()
        scope.canvas.renderAll()
      )
    }
  )

  .directive("dwZoomOut", () ->
    return {
    restrict: "A"
    link: (scope, element, attributes) ->
      $(element).click( () ->
        zoom = scope.canvas.getZoom()
        if zoom > 0.2
          zoom = zoom - 0.1
        scope.canvas.setZoom(zoom)
        scope.canvas.calcOffset()
        scope.canvas.renderAll()
      )
    }
  )

  .directive("dwNext", ['SlidesService', (SlidesService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if scope.tools.next and not scope.tools.desktop
            if scope.canvas.getActiveObject()?.isEditing
              scope.canvas.getActiveObject().exitEditing()
              scope.tools.text.isCreating = false
              scope.tools.text.blockCreating = false
            scope.slides.slideIndex++
            SlidesService.showSlide(scope, scope.slides.slideIndex)
            scope.$apply()
        )
    }
  ])

  .directive("dwBack", ['SlidesService', (SlidesService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if scope.tools.back and not scope.tools.desktop
            if scope.canvas.getActiveObject()?.isEditing
              scope.canvas.getActiveObject().exitEditing()
              scope.tools.text.isCreating = false
              scope.tools.text.blockCreating = false
            scope.slides.slideIndex--
            SlidesService.showSlide(scope, scope.slides.slideIndex)
            scope.$apply()
        )
    }
  ])

  .directive("dwShowSlide", ['SlidesService', (SlidesService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          slideIndex = parseInt(attributes.dwSlideIndex,10)
          if scope.canvas.getActiveObject()?.isEditing
            scope.canvas.getActiveObject().exitEditing()
            scope.tools.text.isCreating = false
            scope.tools.text.blockCreating = false
          if scope.slides.slideIndex isnt slideIndex
            scope.slides.slideIndex = slideIndex
            SlidesService.showSlide(scope, slideIndex)
          scope.$apply()
        )
    }
  ])

  .directive("dwAddSlide", ['SlideService', (SlideService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if not scope.tools.desktop
            if scope.canvas.getActiveObject()?.isEditing
              scope.canvas.getActiveObject().exitEditing()
              scope.tools.text.isCreating = false
              scope.tools.text.blockCreating = false
            scope.canvas.trigger("slide:added", {slideIndex: scope.slides.slideIndex+1})
            SlideService.addSlide(scope, scope.slides.slideIndex+1)
            scope.$apply()
        )
    }
  ])

  .directive("dwRemoveSlide", ['SlideService', (SlideService) ->
    return {
    restrict: "A"
    link: (scope, element, attributes) ->
      $(element).click( () ->
        if scope.tools.removeSlide and not scope.tools.desktop
          if scope.canvas.getActiveObject()?.isEditing
            scope.canvas.getActiveObject().exitEditing()
            scope.tools.text.isCreating = false
            scope.tools.text.blockCreating = false
          scope.canvas.trigger("slide:removed", {slideIndex: scope.slides.slideIndex})
          SlideService.removeSlide(scope, scope.slides.slideIndex)
          scope.$apply()
      )
    }
  ])

  .directive("dwUndo", ['ToolbarService', (ToolbarService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if scope.tools.undo and not scope.tools.desktop
            if scope.canvas.getActiveObject()?.isEditing
              scope.canvas.getActiveObject().exitEditing()
              scope.tools.text.isCreating = false
              scope.tools.text.blockCreating = false
            scope.stackIndex--
            scope.undoMode = true
            ToolbarService.deselectSidebar(scope)
            ToolbarService.undo(scope)
            scope.$apply()
        )
    }
  ])

  .directive("dwRedo", ['ToolbarService', (ToolbarService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if scope.tools.redo and not scope.tools.desktop
            if scope.canvas.getActiveObject()?.isEditing
              scope.canvas.getActiveObject().exitEditing()
              scope.tools.text.isCreating = false
              scope.tools.text.blockCreating = false
            scope.stackIndex++
            ToolbarService.deselectSidebar(scope)
            ToolbarService.redo(scope)
            scope.$apply()
        )
    }
  ])

  .directive('dwImageDroppable', ['ToolbarService', 'WhiteboardService', (ToolbarService, WhiteboardService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).droppable({
          drop: (event,ui) ->
            if $(ui.helper).hasClass("dragging-image")
              top = 142
              left = 64
              width = parseInt($('#wb').css("width"),10)
              height = parseInt($('#wb').css("height"),10)
              widthOfImageSidebar = parseInt($("#sidebar-image-repository").css("width"),10)
              eventLeft = event.pageX
              eventTop = event.pageY
              if eventLeft > left + widthOfImageSidebar and eventLeft < left + width and eventTop > top and eventTop < top + height
                options =
                  src: $(ui.helper).attr("src")
                  top: parseInt($(ui.helper).css("top"), 10) - 42
                  left: parseInt($(ui.helper).css("left"),10) - 42
                $(ui.helper).remove()
                ToolbarService.deselectTools(scope)
                WhiteboardService.activateObjects(scope)
                WhiteboardService.createImage(scope, options)
                scope.$apply()
        })
    }
  ])

  .directive("dwDesktop", ['ToolbarService', 'WhiteboardService', 'WebRTCService', (ToolbarService, WhiteboardService, WebRTCService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if scope.tools.desktopAvailable
            if scope.tools.desktop
              $('.tool').removeClass("unselectable-desktop")
              $('.sidebar-tool').removeClass("unselectable-desktop")
              scope.tools.desktop = false
              if scope.sidebarFixed
                scope.imageRepository = scope.tmpImageRepository
                scope.slides.selected = scope.slides.tmpSelected
              WebRTCService.stopDesktop(scope)
            else
              WebRTCService.startDesktop(scope)

          scope.$apply()
        )
    }
  ])

  .directive("dwWebcam", ['WebRTCService', (WebRTCService) ->
    return {
    restrict: "A"
    link: (scope, element, attributes) ->
      $(element).click( () ->
        if scope.tools.webcamAvailable
          if scope.tools.webcam
            WebRTCService.stopWebcam(scope)
          else
            WebRTCService.startWebcam(scope)
          scope.$apply()
      )
    }
  ])

  .directive("dwScreencast", ['SocketService', (SocketService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if scope.tools.screencastAvailable
            if scope.tools.screencast
              SocketService.stopScreencast(scope)
              scope.tools.screencast = false
            else
              SocketService.startScreencast(scope)
              scope.tools.screencast = true
            scope.$apply()
        )
    }
  ])

  .directive("dwRecord", ["WebRTCService", "SocketService", "$interval", (WebRTCService, SocketService, $interval) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if scope.tools.recordAvailable
            scope.tools.record = false
            scope.tools.recordStop = true
            date = new Date()
            scope.timer = true
            setTimeout scope.updateRecordTime, 0
            scope.$apply()
            SocketService.startRecording(scope)
            WebRTCService.startRecording(scope)
        )
    }
  ])

  .directive("dwRecordStop", ["WebRTCService", "SocketService", "$interval", (WebRTCService, SocketService, $interval) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if scope.tools.recordStop
            scope.tools.record = true
            scope.tools.recordStop = false
            scope.timer = null
            scope.timerStart = undefined
            scope.$apply()
            WebRTCService.stopRecording(scope)
            SocketService.sendRecordTime(scope)

        )
    }
  ])

  .directive("dwShowCursorTools", ['ToolbarService', 'WhiteboardService', (ToolbarService, WhiteboardService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if not scope.tools.desktop
            ToolbarService.deselectTools(scope)
            ToolbarService.deselectSidebar(scope)
            WhiteboardService.activateObjects(scope)
            scope.tools.cursor = true
            scope.$apply()
        )
    }
  ])

  .directive("dwDeleteElement", ['ToolbarService', 'WhiteboardService', (ToolbarService, WhiteboardService) ->
      return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if not scope.tools.desktop
            WhiteboardService.deleteActiveObject(scope)
            scope.$apply()
        )
      }
    ])

  .directive("dwLayerDownElement", ['ToolbarService', 'WhiteboardService', (ToolbarService, WhiteboardService) ->
      return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if scope.tools.layerDown and not scope.tools.desktop
            WhiteboardService.layerDownObject(scope)
            ToolbarService.selectLayerTools(scope)
            scope.$apply()
        )
      }
    ])

  .directive("dwLayerUpElement", ['ToolbarService', 'WhiteboardService', (ToolbarService, WhiteboardService) ->
      return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if scope.tools.layerUp and not scope.tools.desktop
            WhiteboardService.layerUpObject(scope)
            ToolbarService.selectLayerTools(scope)
            scope.$apply()
        )
      }
    ])

  .directive("dwShowPenTools", ['ToolbarService', 'WhiteboardService', (ToolbarService, WhiteboardService) ->
    return {
    restrict: "A"
    link: (scope, element, attributes) ->
      $(element).click( () ->
        if not scope.tools.desktop
          ToolbarService.deselectTools(scope)
          ToolbarService.deselectSidebar(scope)
          WhiteboardService.deactivateObjects(scope)
          scope.tools.pen.selected = true
          scope.tools.pen.size = scope.tools.pen.pen.size
          scope.tools.pen.color = scope.tools.pen.pen.color
          WhiteboardService.activateDrawingMode(scope)
          document.getElementById("tool-pen-color-field").color.fromString(scope.tools.pen.color);
          scope.$apply()
      )
    }
  ])

  .directive("dwShowPenDefault", ['ToolbarService', 'WhiteboardService', (ToolbarService, WhiteboardService) ->
    return {
    restrict: "A"
    link: (scope, element, attributes) ->
      $(element).click( () ->
        if not scope.tools.desktop
          ToolbarService.deselectPenDefaults(scope)
          scope.$eval(attributes.dwDefault).selected = true
          scope.tools.pen.pen = scope.$eval(attributes.dwDefault)
          scope.tools.pen.size = scope.$eval(attributes.dwDefault).size
          scope.tools.pen.color = scope.$eval(attributes.dwDefault).color
          document.getElementById("tool-pen-color-field").color.fromString(scope.tools.pen.color);
          scope.$apply()
          scope.tools.pen.modify()
      )
    }
  ])

  .directive("dwShowLineTools", ['ToolbarService', 'WhiteboardService', (ToolbarService, WhiteboardService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if not scope.tools.desktop
            ToolbarService.deselectTools(scope)
            ToolbarService.deselectSidebar(scope)
            WhiteboardService.deactivateObjects(scope)
            scope.tools.line.selected = true
            scope.tools.line.size = scope.tools.line.line.size
            scope.tools.line.color = scope.tools.line.line.color
            scope.tools.line.style = scope.tools.line.line.style
            document.getElementById("tool-line-color-field").color.fromString(scope.tools.line.color);
            scope.$apply()
        )
    }
  ])

  .directive("dwShowLineDefault", ['ToolbarService', (ToolbarService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if not scope.tools.desktop
            ToolbarService.deselectLineDefaults(scope)
            scope.$eval(attributes.dwDefault).selected = true
            scope.tools.line.line = scope.$eval(attributes.dwDefault)
            scope.tools.line.size = scope.$eval(attributes.dwDefault).size
            scope.tools.line.color = scope.$eval(attributes.dwDefault).color
            scope.tools.line.style = scope.$eval(attributes.dwDefault).style
            document.getElementById("tool-line-color-field").color.fromString(scope.tools.line.color);
            scope.$apply()
        )
    }
  ])

  .directive("dwShowFormsTools", ['ToolbarService', 'WhiteboardService', (ToolbarService, WhiteboardService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if not scope.tools.desktop
            ToolbarService.deselectTools(scope)
            ToolbarService.deselectSidebar(scope)
            WhiteboardService.deactivateObjects(scope)
            scope.tools.forms.selected = true
            scope.tools.forms.borderStyle = scope.tools.forms.form.borderStyle
            scope.tools.forms.borderSize = scope.tools.forms.form.borderSize
            scope.tools.forms.borderColor = scope.tools.forms.form.borderColor
            scope.tools.forms.color = scope.tools.forms.form.color
            document.getElementById("tool-forms-border-color-field").color.fromString(scope.tools.forms.borderColor);
            document.getElementById("tool-forms-fill-color-field").color.fromString(scope.tools.forms.color);
            scope.$apply()
        )
    }
  ])

  .directive("dwShowFormsDefault", ['ToolbarService', (ToolbarService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if not scope.tools.desktop
            ToolbarService.deselectFormsDefaults(scope)
            scope.$eval(attributes.dwDefault).selected = true
            scope.tools.forms.form = scope.$eval(attributes.dwDefault)
            scope.tools.forms.borderStyle = scope.$eval(attributes.dwDefault).borderStyle
            scope.tools.forms.borderSize = scope.$eval(attributes.dwDefault).borderSize
            scope.tools.forms.borderColor = scope.$eval(attributes.dwDefault).borderColor
            scope.tools.forms.color = scope.$eval(attributes.dwDefault).color
            document.getElementById("tool-forms-border-color-field").color.fromString(scope.tools.forms.borderColor)
            document.getElementById("tool-forms-fill-color-field").color.fromString(scope.tools.forms.color)
            scope.$apply()
        )
    }
  ])

  .directive("dwShowTextTools", ['ToolbarService', 'WhiteboardService', (ToolbarService, WhiteboardService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if not scope.tools.desktop
            ToolbarService.deselectTools(scope)
            ToolbarService.deselectSidebar(scope)
            WhiteboardService.deactivateObjects(scope)
            scope.tools.text.selected = true
            scope.tools.text.fontFamily = scope.tools.text.text.fontFamily
            scope.tools.text.fontSize = scope.tools.text.text.fontSize
            scope.tools.text.color = scope.tools.text.text.color
            scope.tools.text.leftAlign = scope.tools.text.text.leftAlign
            scope.tools.text.centerAlign = scope.tools.text.text.centerAlign
            scope.tools.text.rightAlign = scope.tools.text.text.rightAlign
            scope.tools.text.underline = scope.tools.text.text.underline
            scope.tools.text.bold = scope.tools.text.text.bold
            scope.tools.text.italic = scope.tools.text.text.italic
            document.getElementById("tool-text-color-field").color.fromString(scope.tools.text.color)
            scope.$apply()
        )
    }
  ])

  .directive("dwShowTextDefault", ['ToolbarService', (ToolbarService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if not scope.tools.desktop
            ToolbarService.deselectTextDefaults(scope)
            scope.$eval(attributes.dwDefault).selected = true
            scope.tools.text.text = scope.$eval(attributes.dwDefault)
            scope.tools.text.fontFamily = scope.$eval(attributes.dwDefault).fontFamily
            scope.tools.text.fontSize = scope.$eval(attributes.dwDefault).fontSize
            scope.tools.text.color = scope.$eval(attributes.dwDefault).color
            scope.tools.text.leftAlign = scope.$eval(attributes.dwDefault).leftAlign
            scope.tools.text.centerAlign = scope.$eval(attributes.dwDefault).centerAlign
            scope.tools.text.rightAlign = scope.$eval(attributes.dwDefault).rightAlign
            scope.tools.text.underline = scope.$eval(attributes.dwDefault).underline
            scope.tools.text.bold = scope.$eval(attributes.dwDefault).bold
            scope.tools.text.italic = scope.$eval(attributes.dwDefault).italic
            document.getElementById("tool-text-color-field").color.fromString(scope.tools.text.color)
            scope.$apply()
        )
    }
  ])

  .directive("dwSendMessage", ['SocketService', (SocketService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          msg = $("#text-message").val().trim()
          if msg.length > 500
            msg = msg.substring(0,500) + '...'
          safeMsg = encodeURI(msg)
          SocketService.sendMessage(scope, safeMsg)
          $("#text-message").val("")
        )
    }
  ])

  .directive("dwPowerOffPresenter", ['SocketService', 'WebRTCService', (SocketService, WebRTCService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if confirm($("#power-off").text())
            formData = new FormData()
            formData.append "presentationID", window.AppGlobals.presentationID
            request = new XMLHttpRequest()
            request.open "POST", "disconnect", true
            request.send formData
            $("#tool-record-stop").click() if scope.tools.recordStop
            $("#tool-screencast-stop").click() if scope.tools.screencast and scope.tools.screencastAvailable
            $("#tool-webcam-stop").click() if scope.tools.webcam and scope.tools.webcamAvailable
            setTimeout( () ->
              SocketService.close(scope)
              WebRTCService.close()
            , 1000)
            setTimeout( () ->
              window.location = "../dashboard"
            , 2000)
        )
    }
  ])

  .directive("dwPowerOffViewer", ['SocketService', 'WebRTCService', (SocketService, WebRTCService) ->
    return {
      restrict: "A"
      link: (scope, element, attributes) ->
        $(element).click( () ->
          if confirm($("#power-off").text())
            setTimeout( () ->
              SocketService.close(scope)
              WebRTCService.close()
            , 1000)
            setTimeout( () ->
              window.location = "../dashboard"
            , 2000)
        )
    }
  ])