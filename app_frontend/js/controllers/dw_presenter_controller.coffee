'use strict'

angular.module('digitalWhiteboardApp.presenter_controller', []).controller("digitalWhiteboardControllerPresenter", [ '$scope', 'CommandService', 'ToolbarService', 'WhiteboardService', 'SlideService', 'SocketService', 'SlidesService', 'WebRTCService', (scope, CommandService, ToolbarService, WhiteboardService, SlideService, SocketService, SlidesService, WebRTCService) ->

  scope.canvas = new fabric.Canvas("wb", {
    allowTouchScrolling: true
    selection: true
    backgroundColor: 'rgb(255,255,255)'
  })

  scope.canvas.options =
    padding: 8
    borderColor: "#26AAE2"
    cornerColor: "#26AAE2"
    transparentCorners: false
    isEditingMode: false
    originCanvasWidth: parseInt($('#whiteboard').css('width'), 10)
    originCanvasHeight: parseInt(parseInt($('#whiteboard').css('width'), 10) * 10/16, 10)
    propertiesToInclude: ['lockUniScaling','uid','selectable','transparentCorners','padding','borderColor','cornerColor', 'slideIndex', 'originStyles', 'styles']

  scope.canvas.setDimensions({
    width: scope.canvas.options.originCanvasWidth
    height: scope.canvas.options.originCanvasHeight
  })

  $( window ).resize( () ->
    canvasWidth = parseInt($('#whiteboard').css('width'), 10)
    canvasHeight = parseInt(canvasWidth * 10/16, 10)
    scope.canvas.setDimensions({
      width: canvasWidth
      height: canvasHeight
    })
    scope.canvas.calcOffset()
    #WhiteboardService.resizeAllObjects(scope, scope.canvas.options.originCanvasWidth)
    scope.canvas.options.originCanvasWidth = canvasWidth
    scope.canvas.options.originCanvasHeight = canvasHeight
    scope.canvas.renderAll()
  )

  scope.canvas.isDrawingMode = false
  scope.canvas.freeDrawingBrush = null
  scope.slides =
    content: []
    slideIndex: 0
    selected: true
    tmpSelected: false
  scope.imageRepository = false
  scope.tmpImageRepository = false
  scope.tools =
    back: false
    next: false
    addSlide: true
    removeSlide: false
    undo: false
    redo: false
    webcam: false
    webcamAvailable: false
    desktop: false
    desktopAvailable: false
    screencast: false
    screencastAvailable: false
    record: true
    recordAvailable: false
    recordStop: false
    delete: false
    layerDown: false
    layerUp: false
    cursor: true
    pen:
      selected: false
      size: 2
      color: "000000"
      default1:
        selected: true
        size: 2
        color: "000000"
      default2:
        selected: false
        size: 2
        color: "00C200"
      default3:
        selected: false
        size: 2
        color: "FF0000"
      default4:
        selected: false
        size: 2
        color: "0000FF"
      default5:
        selected: false
        size: 2
        color: "FFFF00"
    line:
      selected: false
      size: 2
      color: "000000"
      style: "stroke"
      default1:
        selected: true
        size: 2
        color: "000000"
        style: "stroke"
      default2:
        selected: false
        size: 3
        color: "FF0000"
        style: "stroke"
      default3:
        selected: false
        size: 2
        color: "000000"
        style: "dashed"
      default4:
        selected: false
        size: 2
        color: "000000"
        style: "dotted"
      default5:
        selected: false
        size: 2
        color: "000000"
        style: "stroke"
      default6:
        selected: false
        size: 2
        color: "0000FF"
        style: "dashed"
    forms:
      selected: false
      borderStyle: "stroke"
      borderSize: 1
      borderColor: "000000"
      color: "FFFFFF"
      circle:
        form: "circle"
        selected: true
        borderStyle: "stroke"
        borderSize: 1
        borderColor: "000000"
        color: "FFFFFF"
      ellipse:
        form: "ellipse"
        selected: false
        borderStyle: "stroke"
        borderSize: 1
        borderColor: "000000"
        color: "FFFFFF"
      rectangle:
        form: "rectangle"
        selected: false
        borderStyle: "stroke"
        borderSize: 1
        borderColor: "000000"
        color: "FFFFFF"
      square:
        form: "square"
        selected: false
        borderStyle: "stroke"
        borderSize: 1
        borderColor: "000000"
        color: "FFFFFF"
      triangle:
        form: "triangle"
        selected: false
        borderStyle: "stroke"
        borderSize: 1
        borderColor: "000000"
        color: "FFFFFF"
    text:
      selected: false
      blockCreating: false
      isCreating: false
      fontFamily: "arial"
      fontSize: "14"
      color: "000000"
      leftAlign: true
      centerAlign: false
      rightAlign: false
      underline: false
      bold: false
      italic: false
      big:
        selected: true
        fontFamily: "arial"
        fontSize: "20"
        color: "000000"
        leftAlign: true
        centerAlign: false
        rightAlign: false
        underline: false
        bold: false
        italic: false
      small:
        selected: false
        fontFamily: "arial"
        fontSize: "14"
        color: "000000"
        leftAlign: true
        centerAlign: false
        rightAlign: false
        underline: false
        bold: false
        italic: false
    group:
      removeIsActive: false
      selected: false
      isEditingMode: false
      text:
        selected: false
      pen:
        selected: false
      line:
        selected: false
        size: 2
        color: "000000"
        style: "stroke"
      forms:
        selected: false
        color: "000000"

  scope.tools.line.line = scope.tools.line.default1;
  scope.tools.pen.pen = scope.tools.pen.default1;
  scope.tools.forms.form = scope.tools.forms.circle;
  scope.tools.text.text = scope.tools.text.big

  scope.status =
    saving: false
    saved: false
    noConnection: false
    connected: false

  scope.recordTime = 0
  scope.timer = null
  scope.timerStart = undefined
  scope.timeStartRecording = 0
  scope.uid = new Date().getTime()
  scope.commands = []
  scope.stackIndex = 0
  scope.stackIndexLimit = 50
  scope.undoMode = false
  scope.commandAction = false
  scope.sidebarFixed = true
  scope.connectionEstablished = 0
  scope.countConnectedUsers = 1
  scope.connectedUsers = []
  scope.messages = []

  SlideService.saveInitialSlide(scope, scope.slides.slideIndex)

  scope.sortableOptions =
    axis: 'y'
    revert: true
    revertDuration: 100
    containment: "#slides-container"
    cancel: ".notDraggable"
    update: (e, ui) ->
      SocketService.moveSlide(scope, ui.item['sortable'].index, ui.item['sortable'].dropindex)
      ui.item.scope().canvas.trigger("slide:moved", {from: ui.item['sortable'].index, to: ui.item['sortable'].dropindex})
      ui.item.scope().slides.slideIndex = ui.item['sortable'].dropindex
      SlidesService.setBackAndNext(ui.item.scope())
      ui.item.scope().$apply()

  scope.tools.text.changeBold = () ->
    scope.tools.text.bold = (if scope.tools.text.bold is true then false else true)
    if scope.canvas.options.isEditingMode or scope.tools.text.isCreating
      WhiteboardService.editTextObject(scope, 'bold')

  scope.tools.text.changeItalic = () ->
    scope.tools.text.italic = (if scope.tools.text.italic is true then false else true)
    if scope.canvas.options.isEditingMode or scope.tools.text.isCreating
      WhiteboardService.editTextObject(scope, "italic")

  scope.tools.text.changeUnderline = () ->
    scope.tools.text.underline = (if scope.tools.text.underline is true then false else true)
    if scope.canvas.options.isEditingMode or scope.tools.text.isCreating
      WhiteboardService.editTextObject(scope, "underline")

  scope.tools.text.changeAlignment = (align) ->
    scope.tools.text.leftAlign = false
    scope.tools.text.centerAlign = false
    scope.tools.text.rightAlign = false
    if align is 'leftAlign'
      scope.tools.text.leftAlign = true
    if align is 'centerAlign'
      scope.tools.text.centerAlign = true
    if align is 'rightAlign'
      scope.tools.text.rightAlign = true
    if scope.canvas.options.isEditingMode or scope.tools.text.isCreating
      WhiteboardService.editTextObject(scope, align)

  scope.slidesOpen = () ->
    if not scope.tools.desktop
      scope.imageRepository = false
      scope.slides.selected = true

  scope.imageRepositoryOpen = () ->
    if not scope.tools.desktop
      scope.imageRepository = true
      scope.slides.selected = false

  scope.tools.pen.modify = () ->
    if scope.canvas.options.isEditingMode
      WhiteboardService.editObject(scope)
    if scope.canvas.isDrawingMode
      WhiteboardService.activateDrawingMode(scope)

  scope.tools.line.modify = () ->
    if scope.canvas.options.isEditingMode
      WhiteboardService.editObject(scope)

  scope.tools.forms.modify = () ->
    if scope.canvas.options.isEditingMode
      WhiteboardService.editObject(scope)

  scope.tools.group.modify = () ->
    if scope.canvas.options.isEditingMode
      WhiteboardService.editGroupObject(scope)

  scope.tools.text.modify = (type) ->
    if scope.canvas.options.isEditingMode or scope.tools.text.isCreating
      if type is "fontFamily"
        $('#tool-text-style-select').blur()
        $('#wb').focus()
      WhiteboardService.editTextObject(scope, type)

  scope.$watchCollection("[stackIndex]", () ->
    if scope.stackIndex is 0
      scope.tools.undo = false
    else
      scope.tools.undo = true
    if scope.commands.length - scope.stackIndex > 0
      scope.tools.redo = true
    else
      scope.tools.redo = false
  )

  scope.$watch("slides.slideIndex", () ->
    if scope.sess? and scope.tools.screencast
      SocketService.setActiveSlide(scope)
  )

  scope.updateRecordTime = () ->
    if scope.timer?
      scope.timerStart = new Date().getTime() - scope.recordTime * 1000 unless scope.timerStart
      now = new Date().getTime()
      if now < scope.timerStart + scope.recordTime * 1000
        setTimeout scope.updateRecordTime, 0
      else
        scope.recordTime = Math.round(( now - scope.timerStart ) / 1000)
        SocketService.sendRecordTime(scope)
        scope.formatRecordTime()
        setTimeout scope.updateRecordTime, 990
    return

  scope.formatRecordTime = () ->
    temp = scope.recordTime
    hours = Math.floor((temp %= 86400) / 3600)
    minutes = Math.floor((temp %= 3600) / 60)
    seconds = temp % 60
    hours = "0" + hours if hours < 10
    minutes = "0" + minutes if minutes < 10
    seconds = "0" + seconds if seconds < 10
    $(".record-time").html(hours + ":" + minutes + ":" + seconds)

  scope.establishConnection = () ->
    scope.topic = window.AppGlobals.presentationName + ' - ' + window.AppGlobals.presentationID;
    SocketService.establishConnection(scope)
    WebRTCService.establishConnection(scope, true)
])