'use strict'

angular.module('digitalWhiteboardApp.viewer_controller', []).controller("digitalWhiteboardControllerViewer", [ '$scope', "SocketService", "WebRTCService", (scope, SocketService, WebRTCService) ->

  scope.canvas = new fabric.StaticCanvas("wb", {
    allowTouchScrolling: true
    selection: true
    backgroundColor: 'rgb(255,255,255)'
  })

  scope.canvas.options =
    padding: 8
    borderColor: "#26AAE2"
    cornerColor: "#26AAE2"
    transparentCorners: false
    isEditingMode: false
    originCanvasWidth: parseInt($('#whiteboard').css('width'), 10)
    originCanvasHeight: parseInt(parseInt($('#whiteboard').css('width'), 10) * 10/16, 10)
    propertiesToInclude: ['lockUniScaling','uid','selectable','transparentCorners','padding','borderColor','cornerColor', 'slideIndex', 'originStyles', 'styles']

  scope.canvas.setDimensions({
    width: scope.canvas.options.originCanvasWidth
    height: scope.canvas.options.originCanvasHeight
  })

  $( window ).resize( () ->
    canvasWidth = parseInt($('#whiteboard').css('width'), 10)
    canvasHeight = parseInt(canvasWidth * 10/16, 10)
    scope.canvas.setDimensions({
      width: canvasWidth
      height: canvasHeight
    })
    scope.canvas.calcOffset()
    #WhiteboardService.resizeAllObjects(scope, scope.canvas.options.originCanvasWidth)
    scope.canvas.options.originCanvasWidth = canvasWidth
    scope.canvas.options.originCanvasHeight = canvasHeight
    scope.canvas.renderAll()
  )

  scope.slides =
    content: []
    slideIndex: 0

  scope.tools =
    screencast: false
    webcam: false
    webcamStarted: false

  scope.status =
    saving: false
    saved: false
    noConnection: false
    connected: false

  scope.connectionEstablished = 0
  scope.countConnectedUsers = 1
  scope.connectedUsers = []
  scope.messages = []

  scope.establishConnection = () ->
    scope.topic = window.AppGlobals.presentationName + ' - ' + window.AppGlobals.presentationID;
    SocketService.establishConnection(scope)
    WebRTCService.establishConnection(scope, false)
])