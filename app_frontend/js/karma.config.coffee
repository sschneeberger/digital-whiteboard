# Karma configuration
# Generated on Thu Oct 23 2014 02:23:15 GMT+0200 (CEST)
module.exports = (config) ->
  config.set

    # base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: "../.."

    # frameworks to use
    # available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ["jasmine"]

    # list of files / patterns to load in the browser
    #  "../../../bower_components/angular/angular.js"
    #  "../../../bower_components/angular-mocks/angular-mocks.js"
    files: [
        "../bower_components/angular/angular.js"
        "../bower_components/angular-mocks/angular-mocks.js"
        "../bower_components/fabric/dist/fabric.js"
        "../bower_components/jquery/dist/jquery.js"
        "../bower_components/blueimp-canvas-to-blob/js/canvas-to-blob.js"
        "**/controllers/*.js"
        "**/directives/*.js"
        "**/services/*.js"
        "**/tests/**/*.js"
        "**/app.js"
        "**/fabric_extensions.js"
        "**/image_repository.js"
        "**/web_socket.js"
        "**/whiteboard.js"
    ]

    # list of files to exclude
    exclude: []

    # preprocess matching files before serving them to the browser
    # available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      "**/!(*test).js": ['coverage']
    },

    # test results reporter to use
    # possible values: 'dots', 'progress'
    # available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ["progress", 'coverage']

    # optionally, configure the reporter
    coverageReporter: {
      type: 'html',
      dir: '../../docs/angularjs-coverage',
      subdir: (browser) ->
        return browser.toLowerCase().split(/[ /-]/)[0]
    },

    # web server port
    port: 9876

    # enable / disable colors in the output (reporters and logs)
    colors: true

    # level of logging
    # possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_DEBUG

    # enable / disable watching file and executing tests whenever any file changes
    autoWatch: false

    # start these browsers
    # available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ["PhantomJS"]

    # Continuous Integration mode
    # if true, Karma captures browsers, runs the tests and exits
    singleRun: true

  return