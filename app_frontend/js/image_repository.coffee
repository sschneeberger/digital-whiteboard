###
This script contains the image repository and uploader.

@author sischnee <sischnee@gmail.com>
@since 2012/14/11
###
window.ImageRepository = ->
  @fileList = []
  @uploadList = []
  @uploadIndex = 0
  @userName = ""
  @userID = 0
  @$el = {}
  @maxSize = 0
  @creating = false

  ###
  Scope duplicator / parent this.

  @var
  @access private
  @type object
  ###
  that = this
  progressBar = $("#progress-bar")
  progress = document.querySelector(".percent")

  ###
  Init method sets up the image repository for the dashboard with some event handlers.

  @access public
  @return void
  ###
  @init = ->

    @userName = window.AppGlobals.username
    #for the create event allowing these types!
    jQuery("#browser").jstree(
      plugins: [
        "themes"
        "html_data"
        "types"
        "ui"
        "contextmenu"
        "crrm"
        "sort"
        "dnd"
      ]
      themes:
        theme: "classic"

      ui:
        select_limit: 1
        select_multiple_modifier: "alt"
        selected_parent_close: "select_parent"
        initially_select: ["root"]

      types:
        valid_children: [
          "file"
          "folder"
          "root"
        ]
        types:
          file:
            valid_children: []
            icon:
              image: "/images/file.png"

          folder:
            valid_children: [
              "folder"
              "file"
            ]

          root:
            valid_children: [
              "default"
              "folder"
              "file"
            ]
            icon:
              image: "/images/root.png"

            move_node: false
            remove: false

      contextmenu:
        items: that.customMenu
        select_node: true

      crrm:
        move:
          default_position: "first"
          check_move: (m) ->
            (if m.np[0].getAttribute("id") is "browser" then false else true)

      core:
        animation: 0
        initially_open: ["root"]
        strings:
          new_node    : "new_folder"

      sort: (a, b) ->
        return (if a.getAttribute("title") > b.getAttribute("title") then 1 else -1)  if a.getAttribute("rel") is b.getAttribute("rel")
        (if a.getAttribute("rel") > b.getAttribute("rel") then -1 else 1)
    ).bind("loaded.jstree", (event, data) ->
      that.renderImages $("#root")
      $("#browser").append('<div class="loader-container" style="display: none;"><div class="loader"></div></div>')
      $("body").click(() -> $("#sidebar .alert").hide())
      return
    ).bind("open_node.jstree", (event, data) ->
      that.renderImages data.rslt.obj  if data.rslt.obj.attr("rel") is "folder"
      if not $(".loader-container")?
        $("#browser").append('<div class="loader-container" style="display: none;"><div class="loader"></div></div>')
      return
    ).bind("select_node.jstree", (event, data) ->
      data.rslt.obj.children("ins").click()  if data.rslt.obj.attr("rel") isnt "root" and data.rslt.obj.hasClass("jstree-closed")
      return
    ).bind("create_node.jstree", (event, data) ->
      data.rslt.obj.attr("rel", "folder")
      data.rslt.obj.attr("title", "new_folder")
      data.rslt.obj.sort()
      that.creating = true
      return
    ).bind("create.jstree", (event, data) ->
      node = data.rslt.obj
      if node.attr("rel") is "folder"
        data.rslt.obj.attr("data-rel", that.getParentRelativePath(data.rslt.obj) + "/" + data.rslt.name)
        regex = /^[a-zA-Z0-9_-]*$/
        indexOfLastSlash = node.attr("data-rel").lastIndexOf("/")
        unless regex.test(data.rslt.name)
          $("#characters_not_allowed").show()
          $.jstree.rollback data.rlbk
        if that.checkDuplication(node, node.attr("data-rel").substring(0, indexOfLastSlash) + "/" + data.rslt.name)
          $.jstree.rollback data.rlbk
          return
        xhrData = []
        xhrData["file"] = node.attr("data-rel")
        xhrData["action"] = "createFolder"
        xhrData["callbackValue"] = "create_success"
        xhrData["userID"] = that.userID
        that.sendXHRMessage xhrData
      that.creating = false
      return
    ).bind("rename_node.jstree", (event, data) ->
      if that.creating isnt true
        node = data.rslt.obj
        if node.attr("rel") is "file"
          regex = /^[a-zA-Z0-9._-]*.(jpg|jpeg|gif|bmp|png)$/i
        else regex = /^[a-zA-Z0-9_-]*$/  if node.attr("rel") is "folder"
        unless regex.test(data.rslt.name)
          $("#characters_not_allowed").show()
          $.jstree.rollback data.rlbk
          return
        indexOfLastSlash = node.attr("data-rel").lastIndexOf("/")
        newFileName = node.attr("data-rel").substring(0, indexOfLastSlash) + "/" + data.rslt.name
        if newFileName isnt node.attr("data-rel")
          if that.checkDuplication(node, newFileName)
            $.jstree.rollback data.rlbk
            return
          that.changeRelativePaths node, newFileName
          xhrData = []
          xhrData["file"] = newFileName
          xhrData["oldFile"] = node.attr("data-rel")
          xhrData["action"] = "renameFile"
          xhrData["callbackValue"] = "rename_success"
          xhrData["userID"] = that.userID
          that.sendXHRMessage xhrData
          node.attr "data-rel", newFileName
          node.attr "title", data.rslt.name
      return
    ).bind("delete_node.jstree", (event, data) ->
      xhrData = []
      xhrData["file"] = data.rslt.obj.attr("data-rel")
      xhrData["action"] = "removeFile"
      xhrData["callbackValue"] = "remove_success"
      xhrData["userID"] = that.userID
      that.sendXHRMessage xhrData
      return
    ).bind("move_node.jstree", (event, data) ->
      node = $(data.rslt.o[0])
      parent = $(data.rslt.np[0])
      newFileName = parent.attr("data-rel") + "/" + node.attr("title")
      if that.checkDuplication(node, newFileName)
        $.jstree.rollback data.rlbk
        return
      data.inst.open_node node
      that.changeRelativePaths node, newFileName
      xhrData = []
      xhrData["file"] = newFileName
      xhrData["oldFile"] = node.attr("data-rel")
      xhrData["action"] = "moveFile"
      xhrData["callbackValue"] = "move_success"
      xhrData["userID"] = that.userID
      that.sendXHRMessage xhrData
      node.attr "data-rel", newFileName
      node.sort()
      return
    ).click ->
      $(".info").hide()
      return

    return


  ###
  Create a custom context menu.

  @access public
  @return object
  ###
  @customMenu = (node) ->
    items =
      uploadItem:
        label: "Upload directory"
        action: (obj) ->
          fileInput = document.querySelector("input[webkitdirectory]")
          $(fileInput).change (e) ->
            if e.target.files.length > 0
              indexOfFirstSlash = e.target.files[0].webkitRelativePath.indexOf("/")
              folderName = e.target.files[0].webkitRelativePath.substring(0,indexOfFirstSlash)
              if that.checkDuplicationSubDir(obj, folderName)
                return
              that.uploadInit e
            else
              $("#folder_already_exists").show()
            return

          that.$el = obj
          fileInput.click()
          return

      uploadImage:
        label: "Upload image"
        action: (obj) ->
          fileInput = document.querySelector("#image-select")
          $(fileInput).change (e) ->
            if e.target.files.length > 0
              fileName = e.target.files[0].name
              if that.checkDuplicationSubFile(obj, fileName)
                return
              that.uploadInit e
            else
              $("#folder_already_exists").show()
            return

          that.$el = obj
          fileInput.click()
          return

      createItem:
        label: "Create directory"
        separator_before: true
        action: (obj) ->
          that.creating = true
          $("#browser").jstree("create", null, "inside", { "attr" : { rel : "folder" } } )
          return

      renameItem:
        label: "Rename"
        action: (obj) ->
          $("#browser").jstree "rename"
          return

      deleteItem:
        label: "Delete"
        action: (obj) ->
          $("#browser").jstree "remove"  if confirm("Are you sure you want to delete \"" + obj.context.text.trim() + "\"?")
          return

    if $(node).attr("rel") is "file"
      delete items.uploadImage
      delete items.uploadItem
      delete items.createItem

    if $(node).attr("rel") is "root"
      delete items.renameItem
      delete items.deleteItem
    items


  ###
  Check if file oder folder exists already in the same dir level.

  @access public
  @return bool
  ###
  @checkDuplication = (node, fileName) ->
    countDuplicateObjectsFiles = node.siblings().filter(->
      $el = $(this)
      $el.attr("data-rel") is fileName and $el.attr("rel") is "file"
    ).length
    countDuplicateObjectsFolders = node.siblings().filter(->
      $el = $(this)
      $el.attr("data-rel") is fileName and $el.attr("rel") is "folder"
    ).length
    if countDuplicateObjectsFiles > 0
      $("#file_already_exists").show()
      return true
    if countDuplicateObjectsFolders > 0
      $("#folder_already_exists").show()
      return true
    false

  ###
  Check if folder exists already in the sub dir level

  @access public
  @return bool
  ###
  @checkDuplicationSubDir = (node, folderName) ->
    countDuplicateObjectsFolders = node.children("ul").children("li").filter(->
      $el = $(this)
      $el.attr("title") is folderName and $el.attr("rel") is "folder"
    ).length
    if countDuplicateObjectsFolders > 0
      $("#folder_already_exists").show()
      return true
    false

  ###
  Check if file exists already in the sub dir level

  @access public
  @return bool
  ###
  @checkDuplicationSubFile = (node, fileName) ->
    countDuplicateObjectsFiles = node.children("ul").children("li").filter(->
      $el = $(this)
      $el.attr("title") is fileName and $el.attr("rel") is "file"
    ).length
    if countDuplicateObjectsFiles > 0
      $("#file_already_exists").show()
      return true
    false

  ###
  Change relative paths for each children if parent folder is renamed.

  @access public
  @return void
  ###
  @getParentRelativePath = (node) ->
    return node.parents("li").eq(0).attr("data-rel")


  ###
  Change relative paths for each children if parent folder is renamed.

  @access public
  @return void
  ###
  @changeRelativePaths = (node, newFileName) ->
    if node.attr("rel") is "folder"
      node.find("li").each (i, elem) ->
        postFix = $(elem).attr("data-rel").substring(node.attr("data-rel").length)
        $(elem).attr "data-rel", newFileName + postFix
        return

    return


  ###
  Send formdata via XMLHttpRequest to the webserver.

  @access public
  @return void
  ###
  @sendXHRMessage = (data) ->
    $(".loader-container").show()
    formData = new FormData()
    for key of data
      formData.append key, data[key]
    xhr = new XMLHttpRequest()
    xhr.open "POST", "send", true
    xhr.send formData
    return


  ###
  Render small preview images.

  @access public
  @return void
  ###
  @renderImages = (elem) ->
    elem.children("ul").children("li[rel=\"file\"]").each ->
      if $(this).children("img").length is 0
        try
          fullPath = "/storage/" + that.userName + "/" + $(this).attr("data-rel")
          imgElement = $("<img src='" + fullPath + "' />")
          $(this).prepend imgElement
          if $(".widget")?
            imgElement.mouseover( () ->
              imgElement.css("cursor", "pointer")
              imgElement.css("opacity", "0.5")
              imgElement.css("filter", "alpha(opacity='50')")
            )
            imgElement.mouseout( () ->
              imgElement.css("cursor", "default")
              imgElement.css("opacity", "1")
              imgElement.css("filter", "alpha(opacity='100')")
            )
            imgElement.draggable({
              revert: true
              revertDuration: 300
              appendTo: '#toolbar'
              helper: 'clone'
              zIndex: 10
              start: (event, ui) ->
                $(ui.helper).addClass("dragging-image")
                $(ui.helper).css("opacity", "0.5")
                $(ui.helper).css("filter", "alpha(opacity='50')")
                $(ui.helper).css("cursor", "no-drop")
                $(ui.helper).css("list-style-type", "none")

              drag: (event, ui) ->
                top = 117
                left = 64
                width = parseInt($('#wb').css("width"),10)
                height = parseInt($('#wb').css("height"),10)
                widthOfImageSidebar = parseInt($("#sidebar-image-repository").css("width"),10)
                eventLeft = event.pageX
                eventTop = event.pageY

                if eventLeft > left + widthOfImageSidebar and eventLeft < left + width and eventTop > top and eventTop < top + height
                  $(ui.helper).css("cursor", "copy")
                else
                  $(ui.helper).css("cursor", "no-drop")
            })
      return
    return


  ###
  Init upload process (uploadlist, progress bar, ..)

  @access public
  @return void
  ###
  @uploadInit = (e) ->
    progress.style.width = "0%"
    progress.textContent = "0%"
    @fileList = e.target.files
    @uploadList = []
    @uploadIndex = 0
    i = 0
    file = undefined

    while file = @fileList[i]
      if file.type.match("image.*")
        @uploadList.push file
        @maxSize = that.maxSize + file.size
      ++i
    $(".info").hide()
    progressBar.show()
    progressBar.addClass "loading"
    @upload()
    return


  ###
  Upload an image.

  @access public
  @return void
  ###
  @upload = ->
    if that.uploadList.length is 0
      $(".error").show()
      that.uploadIndex = 0
      return
    loadedSize = 0
    i = 0

    while i < that.uploadIndex
      loadedFile = that.uploadList[i]
      loadedSize = loadedSize + loadedFile.size
      ++i
    @updateProgress loadedSize
    file = that.uploadList[that.uploadIndex]
    if file instanceof Object
      xhrData = []
      xhrData["file"] = file
      if file.webkitRelativePath is ""
        xhrData["path"] = file.name
        xhrData["base_dir"] = that.$el.attr("data-rel")
      else
        xhrData["path"] = file.webkitRelativePath
        xhrData["base_dir"] = that.$el.attr("data-rel")
      xhrData["action"] = "uploadImage"
      xhrData["userID"] = that.userID
      that.sendXHRMessage xhrData
    return


  ###
  Manages upload process, call a new upload if there are images left.

  @access public
  @return void
  ###
  uploadHandler = (result) ->
    if result
      that.uploadIndex++
      unless that.uploadList.length is that.uploadIndex
        that.upload()
      else
        progress.style.width = "100%"
        progress.textContent = "100%"
        xhrData = []
        xhrData["action"] = "getHTMLSnippet"
        xhrData["userID"] = that.userID
        that.sendXHRMessage xhrData
    else
      resultHandler("error")
    return

  uploadSuccessHandler = (result) ->
    progressBar.hide()
    $("#browser").jstree("destroy")
    $("#browser ul").remove()
    resultHandler("upload_success")
    $("#browser").prepend(result)
    that.init()

  resultHandler = (result) ->
    $("#sidebar .alert").hide()
    $("#" + result).show()
    setTimeout (->
      $("#" + result).fadeOut()
      return
    ), 3000

  @dispatch = (event) ->
    $(".loader-container").hide()
    data = JSON.parse(event)
    callback = data.func
    eval(callback) data.value
    return

  setUserID = (data) ->
    that.userID = JSON.parse(data)
    return


  ###
  Upload progress bar

  @access public
  @return void
  ###
  @updateProgress = (loadedSize) ->

    # evt is an ProgressEvent.
    if @maxSize > 0
      percentLoaded = Math.round((loadedSize / @maxSize) * 100)

      # Increase the progress bar length.
      if percentLoaded < 100
        progress.style.width = percentLoaded + "%"
        progress.textContent = percentLoaded + "%"
    return

  @init()
  return