###
This script serves as the webSocket handler for image repository.

@author sischnee <sischnee@gmail.com>
@since 2012/11/12
###
window.WhiteboardWebSocket = (url) ->

  callbacks = {}
  ws_url = url
  @conn = null

  ###
  Bind method for callbacks.

  @access public
  @return object
  ###
  @bind = (event_name, callback) ->
    callbacks[event_name] = callbacks[event_name] or []
    callbacks[event_name].push callback
    this # chainable


  ###
  Send data over websocket connection.

  @access public
  @return this
  ###
  @send = (event_name, event_data) ->
    @conn.send event_data
    this


  ###
  Connect websocket.

  @access public
  @return void
  ###
  @connect = ->
    if typeof (MozWebSocket) is "function"
      @conn = new MozWebSocket(url)
    else
      @conn = new WebSocket(url)

    # dispatch to the right handlers
    @conn.onmessage = (evt) ->
      dispatch "message", evt.data
      return

    @conn.onclose = ->
      dispatch "close", null
      return

    @conn.onopen = ->
      dispatch "open", null
      return

    return

  @disconnect = ->
    @conn.close()
    return


  ###
  Dispatcher for callbacks.

  @access private
  @return void
  ###
  dispatch = (event_name, message) ->
    chain = callbacks[event_name]
    return  if typeof chain is "undefined" # no callbacks for this event
    i = 0

    while i < chain.length
      chain[i] message
      i++
    return

  @log = (msg) ->
    console.log msg
    return

  return