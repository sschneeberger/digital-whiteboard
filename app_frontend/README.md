# Digital Whiteboard Frontend

This is the [Angular.js][] ??? application that renders the data it fetches from the backend.

 [Angular.js]: http://angularjs.com

## Build assets

TLDR:

Assets are build with [Grunt][]. To build all assets for developement mode, just run `grunt`. To have Grunt watch for file changes and build automatically, use `grunt watch`. If there is no `grunt-cli` NPM package installed globally on your machine, you can use the local one, found in `node_modules/.bin/grunt`.

## Why is this not in a bundle?

This is outside of any Symfony bundles for several reasons:

 * Easier to build with Grunt which is more powerful than Assetic
 * Not having to dig into bundles to find JS/CSS stuff

## Stuff used

 * [CoffeeScript][]: Nicer way to write JS code
 * [Angular.js][]: JavaScript MVC framework
 * [Less][]: CSS preprocessor
    * [Bootstrap][] CSS Framework
 * [Grunt][]: Asset build tool
    * [grunt-browserify][]: Grunt task for [browserify][], makes it possible to use `require()` in Browser code
    * [grunt-contrib-watch][]: Build assets automatically on change
 * [Bower][]: Package management for frontend code running in the browser

 [CoffeeScript]: http://coffeescript.org
 [Less]: http://lesscss.org
 [Bootstrap]: http://getbootstrap.com
 [Grunt]: http://gruntjs.com
 [grunt-browserify]: https://github.com/jmreidy/grunt-browserify
 [browserify]: http://browserify.org
 [grunt-contrib-watch]: https://github.com/gruntjs/grunt-contrib-watch
 [Bower]: http://bower.io

## Project structure

```
digital-whiteboard
├── app_frontend                        # Frontend project root
│   ├── bower_components                # Installed Bower components
│   ├── build                           # Build output
│   ├── css                             # LESS files
│   ├── images                          # Images
│   └── js                              # JavaScript/CoffeeScript files
│       └── app.coffee                    # Main AngularJS entry point
├── bower.json                          # Bower dependencies
├── package.json                        # NPM dependencies
└── Gruntfile.coffee                    # Grunt build script
```

## Conventions

There are some conventions that should be followed when adding new code, as the Grunt build process makes some assumptions about them.

### CoffeeScript

All JavaScript code should be written in CoffeeScript unless there is a good reason not to.

### CommonJS modules

Thanks to `browserify`, we have a [CommonJS Modules][cjs-modules] system we can use in the browser. To expose something to the outside, attach them to `module.exports`, e.g.

```coffee
# modules/foo.coffee
module.exports =
  foo = -> console.log("Hello World")
  bar = (a) -> a * 2

# other module
baz = require('foo').bar(2)
```

`module.exports` can be any JavaScript value, so you can also export just a function:

```coffee
# modules/hello.coffee
module.exports = (name = "World") -> console.log("Hello #{name}!")

# other module
hello = require('hello')
hello("Universe")
```

There are to module roots defined:

 * `js/modules`: General things that can be reused. The modules here are automatically aliased without prefix, e.g. the module in `js/modules/foo/bar.coffee` can be used with `bar = require("foo/bar")`.

 [cjs-modules]: http://wiki.commonjs.org/wiki/Modules/1.1

## Adding Bower packages

Packages added via Bower should be registered in the `Gruntfile.coffee` so `browserify` knows about them. For CommonJS compliant things (like Underscore.js), adding it to `BOWER_MODULES` is sufficient:

```coffee
BOWER_MODULES =
  # ...
  underscore: "app_frontend/bower_components/underscore/underscore.js"
```

`BOWER_MODULES` expects the keys to be the module name it will be available under and the path to the JS file as the value.

## Translations

Symfony translations are available in JavaScript via [BazingaJsTranslationBundle][]. It is integrated into the Grunt workflow, via the `translations` task. To expose a translation domain to JavaScript, it needs to be added to the `JS_TRANSLATION_DOMAINS` array in `Gruntfile.coffee`.

 [BazingaJsTranslationBundle]: https://github.com/willdurand/BazingaJsTranslationBundle